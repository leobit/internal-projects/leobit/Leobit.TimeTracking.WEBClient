export const environment = {
  production: true,
  identityServerUrl: 'https://identity.leobit.co',
  apiUrl: 'https://timetracking.leobit.co/v1',
  postLogoutRedirectUri: 'https://timetracking.leobit.co',
};
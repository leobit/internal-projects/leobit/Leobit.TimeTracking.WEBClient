export const environment = {
  production: false,
  identityServerUrl: 'https://test.identity.leobit.co',
  apiUrl: 'https://test.timetracking.leobit.co/v1',
  postLogoutRedirectUri: 'https://test.timetracking.leobit.co',
};
export const environment = {
  production: true,
  identityServerUrl: 'https://staging.identity.leobit.co',
  apiUrl: 'https://staging.timetracking.leobit.co/v1',
  postLogoutRedirectUri: 'https://staging.timetracking.leobit.co',
};
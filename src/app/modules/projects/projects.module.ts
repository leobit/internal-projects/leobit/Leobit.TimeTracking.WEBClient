import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectsResolver } from './pages/projects/projects.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import { ApproveReopenProjectReportingComponent } from './pages/approve-reopen-project-reporting/approve-reopen-project-reporting.component';
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { InputNumberModule } from 'primeng/inputnumber';
import { SharedModule } from 'src/app/shared/shared.module';
import { TimeToDatePipe } from 'src/app/shared/pipes/time-to-date.pipe';

const projectsRoutes: Routes = [
  {
    path: '',
    component: ProjectsComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: ProjectsResolver }
  },
  {
    path: 'approve-reopen-reporting/:pendingRequestId',
    component: ApproveReopenProjectReportingComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(projectsRoutes),
    InputTextModule,
    InputNumberModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    FieldsetModule,
    CheckboxModule,
    SliderModule,
    DialogModule,
    CalendarModule,
    ReactiveFormsModule,
    ProgressSpinnerModule,
    SharedModule,
    TimeToDatePipe
  ],
  declarations: [ApproveReopenProjectReportingComponent, ProjectsComponent],
  providers: [ProjectsResolver]
})
export class ProjectsModule { }

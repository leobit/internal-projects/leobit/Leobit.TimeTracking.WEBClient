import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Slider } from 'primeng/slider';
import { SelectItem, MessageService, SortEvent, FilterMetadata } from 'primeng/api';
import { Project, ProjectPriority, ClosingType, CategoryShort } from 'src/app/core/api/models';
import { ProjectService } from 'src/app/core/api/services';
import { ProjectsPreloaded } from './projects.resolver';
import { Globals } from 'src/app/core/globals';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { ClosingTypeService } from 'src/app/core/services/closing-type-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProjectType } from 'src/app/shared/models/project-type.enum';
import { Table } from 'primeng/table';
import { ProjectConfigurationBackup } from 'src/app/shared/models/project-configuration-backup';
import { UserProfileService } from 'src/app/core/services/user-profile.service';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import { noWhiteSpacesValidator } from 'src/app/shared/validators/no-whitespaces-validator';
import { DateRangeValidator } from 'src/app/shared/validators/date-range-validator';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, AfterViewInit {
  @ViewChild("internalSlider", { static: true })
  internalSlider: Slider;

  @ViewChild("externalSlider", { static: true })
  externalSlider: Slider;

  @ViewChild('dt') dataTable!: Table;

  dialogVisible: boolean = false;
  reopenRequestsDialogVisible: boolean = false;
  showClosedProjects: boolean = false;

  closingTypes: SelectItem[];

  closingTypesEnum = ClosingType;

  projectPriority = ProjectPriority;

  priorityMapping: Record<ProjectType, ProjectPriority> = {
    [ProjectType.External]: ProjectPriority.High,
    [ProjectType.Internal]: ProjectPriority.Medium,
    [ProjectType.Vacation]: ProjectPriority.High,
    [ProjectType.SickLeaveWithoutCertificate]: ProjectPriority.High,
    [ProjectType.SickLeaveWithCertificate]: ProjectPriority.High,
    [ProjectType.VacationUnpaid]: ProjectPriority.High,
    [ProjectType.Reserve]: ProjectPriority.VeryLow,
    [ProjectType.Onboarding]: ProjectPriority.Low
  };

  overrideProjectType: boolean = false;
  categories: SelectItem[];
  accounts: SelectItem[];
  projectTypes: SelectItem[];
  projectPriorities: SelectItem[];

  projects: Project[];

  minDate = new Date('01-01-2014');

  editProject: Project = {} as Project;
  currentSort: 'none' | 'ascending' | 'descending' = 'none'
  form: FormGroup;

  projectConfigurationBackup?: ProjectConfigurationBackup;
  projectConfugurationBackupExpiration?: Date;

  get f() {
    return this.form.controls;
  }
  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private messageService: MessageService,
    private userProfileService: UserProfileService,
    public globals: Globals,
    public closingTypeService: ClosingTypeService,
    private tabPermissionsService: TabPermissionsService) { }

  ngOnInit() {
    const preloaded = <ProjectsPreloaded>this.route.snapshot.data.preloaded;

    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(100), noWhiteSpacesValidator()]),
      projectPriority: new FormControl(),
      startDate: new FormControl(null),
      endDate: new FormControl(null),
      },
      { validators: DateRangeValidator }
    );

    this.accounts = preloaded.accounts.map<SelectItem>(account => {
      return { value: account, label: account.name };
    });
    this.categories = preloaded.categories.map<SelectItem>(category => {
      return { value: category as CategoryShort, label: `${category.organizationUnit.division.name} - ${category.organizationUnit.name} - ${category.name}` };
    });
    this.projectTypes = Object.values(ProjectType).filter(x =>
      !isNaN(Number(x))).map<SelectItem>(projectType => ({
        value: projectType,
        label: String(ProjectType[projectType]).replace(/([A-Z]+)/g, ' $1').trim() //adding spaces between capital letters
      }));
    this.projectPriorities = Object.values(ProjectPriority)
      .map<SelectItem>(projectPriority => ({
        value: projectPriority,
        label: String(ProjectPriority[projectPriority]).replace(/([A-Z]+)/g, ' $1').trim()
      }));

    this.projects = preloaded.projects;
  }

  updateExpirationTimes() {
    if (this.editProject?.projectReopenRequests) {
      const currentTime = new Date();
      this.editProject.projectReopenRequests = this.editProject.projectReopenRequests.filter(reopenRequest => {
        const expirationTime = new Date(reopenRequest.expirationTime);
        if (expirationTime > currentTime) {
          expirationTime.setSeconds(expirationTime.getSeconds() - 1); // Subtract 1 second to simulate change
          reopenRequest.expirationTime = expirationTime.toISOString();
          return true;
        }
        return false;
      });
      if (this.editProject.projectReopenRequests.length === 0) {
        this.reopenRequestsDialogVisible = false;
      }
    }
  }

  ngAfterViewInit() {
    this.dataTable.filter('false', 'isClosed', 'equals');
  }

  get hasWritePermission() {
    return this.tabPermissionsService.hasWriteClaim("Project");
  }

  onAddProject() {
    this.form.markAsUntouched();
    this.editProject = {
      id: 0,
      account: this.accounts[0].value,
      category: this.categories[0].value,
      name: 'Project name',
      overrideHoursSettings: false,
      minInternal: 0,
      maxInternal: 12,
      minExternal: 0,
      maxExternal: 12,
      overrideClosingSettings: false,
      closingType: ClosingType.Monday,
      beforeClosing: 0,
      afterClosing: 0,
      isClosed: false,
      autoAssignNewUsers: false,
      overrideTimeReportPriorities: false,
      projectType: ProjectType.External,
      projectPriority: ProjectPriority.High,
      startDate: null,
      endDate: null
    };
    this.form.controls['startDate'].setValue(null);
    this.form.controls['endDate'].setValue(null);
    this.projectConfigurationBackup = null;

    this.loadClosingTypes();

    this.showDialog();
  }

  onEditProject(project: Project) {
    this.form.markAsUntouched();
    this.editProject = Object.assign({}, project);

    if (this.editProject.startDate) {
      this.form.controls['startDate'].setValue(new Date(this.editProject.startDate));
    } else {
      this.form.controls['startDate'].setValue(null);
    }
    if (this.editProject.endDate) {
      this.form.controls['endDate'].setValue(new Date(this.editProject.endDate));
    } else {
      this.form.controls['endDate'].setValue(null);
    }
    if (this.editProject.category) {
      this.editProject.category = this.categories.find(c => c.value.id === project.category.id).value
    }

    if (project.projectSettingsBackup) {
      this.projectConfigurationBackup = JSON.parse(project.projectSettingsBackup.configurationSnapshot);
      this.projectConfugurationBackupExpiration = new Date(project.projectSettingsBackup.expirationTime);
    } else {
      this.projectConfigurationBackup = null;
    }
    if (this.editProject.projectReopenRequests) {
      this.updateExpirationTimes();
    }

    this.loadClosingTypes();

    this.showDialog();
  }

  loadClosingTypes() {
    this.closingTypes = this.closingTypeService.getEnabledTypes(this.editProject.closingType);
  }

  supportsClosingType(closingType) { return this.closingTypeService.supportsWindowExtension(closingType); }

  onSaveProject() {
    let startDate = this.form.controls['startDate']?.value;
    let endDate = this.form.controls['endDate']?.value;

    if (startDate) {
      this.editProject.startDate = Date.CreateUTCDate(startDate).toISOString()
    } else {
      this.editProject.startDate = null;
    }
    
    if (endDate) {
      this.editProject.endDate = Date.CreateUTCDate(endDate).toISOString();
    } else {
      this.editProject.endDate = null;
    }
    if (!this.editProject.category) {
      this.editProject.category = this.categories[0].value;
    }

    if (this.editProject.id == 0) {
      this.projectService.createProject({ body: this.editProject }).subscribe(project => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Project '${project.name}' created` });
        this.projects = [project].concat(this.projects.slice());
        this.filterGlobaly();
      });
    } else {
      this.projectService.editProject({ body: this.editProject }).subscribe(project => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Project '${project.name}' updated` });
        const i = this.projects.findIndex(x => x.id === this.editProject.id);
        if (this.currentSort === "none") {
          this.projects[i] = project;
        } else {
          if (i !== -1) {
            this.projects.splice(i, 1);
          }
          this.projects = [project].concat(this.projects.slice());
        }
        this.filterGlobaly();
      });
    }


    this.hideDialog();
  }

  filterGlobaly() {
    const filterGlobal = this.dataTable.filters['global'] as FilterMetadata;
    this.dataTable.filterGlobal(filterGlobal?.value, 'contains');
  }

  onInternalHoursChange(values: number[]) {
    this.editProject.minInternal = values[0] / 2;
    this.editProject.maxInternal = values[1] / 2;
  }

  onExternalHoursChange(values: number[]) {
    this.editProject.minExternal = values[0] / 2;
    this.editProject.maxExternal = values[1] / 2;
  }

  onProjectTypeChange(value: number) {
    if (!this.form.controls.projectPriority.touched) {
      this.editProject.projectPriority = this.priorityMapping[value];
    }
  }

  customSort(event: SortEvent) {
    if (event.order === 1) {
      this.currentSort = "ascending";
      this.projects.sort((a, b) => a.name.localeCompare(b.name));
    }
    if (event.order === -1) {
      this.currentSort = "descending";
      this.projects.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? -1 : 1));
    }
  }

  resetSort() {
    this.currentSort = "none";
    this.projects.sort((a, b) => b.id - a.id);
  }

  approveReopenRequest(id: number): void {
    this.projectService.approveRequestReopenProjectReporting({
      id: id
    }).subscribe({
      next: (res) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: `${res}`
        });
        dayjs.extend(utc);
        var oneHourLater = dayjs().add(1, 'hour').utc().format("YYYY-MM-DDTHH:mm:ss[Z]");
        this.editProject.projectReopenRequests.find(x => x.id === id).projectManagerApprovedFullName = this.userProfileService.userProfile.name;
        this.editProject.projectReopenRequests.find(x => x.id === id).expirationTime = oneHourLater;
      },
      error: () => { }
    });
  }

  declineReopenRequest(id: number): void {
    this.projectService.declineRequestReopenProjectReporting({
      id: id
    }).subscribe({
      next: (res) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: `${res}`
        });
        this.editProject.projectReopenRequests = this.editProject.projectReopenRequests.filter(x => x.id !== id);
        if (this.editProject.projectReopenRequests.length === 0) {
          this.reopenRequestsDialogVisible = false;
        }
      },
      error: () => { }
    });
  }

  private showDialog() {
    this.overrideProjectType = this.editProject.projectType != ProjectType.External ? true : false;

    this.dialogVisible = true;
    // handle sliders range mode
    if (this.editProject.minInternal == 0 && this.editProject.maxInternal == 0) {
      this.internalSlider.handleIndex = 1;
    }
    if (this.editProject.minExternal == 0 && this.editProject.maxExternal == 0) {
      this.externalSlider.handleIndex = 1;
    }
  }

  private hideDialog() {
    this.dialogVisible = false;
  }
  getEventValue($event: any): string {
    return $event.target.value;
  }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectService } from 'src/app/core/api/services';
import { Project, AccountShort, Category } from 'src/app/core/api/models';

export interface ProjectsPreloaded {
    categories: Category[];
    accounts: AccountShort[];
    projects: Project[];
}

@Injectable()
export class ProjectsResolver  {
    constructor(private projectService: ProjectService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProjectsPreloaded> {
        return forkJoin([
            this.projectService.getShortCategories(),
            this.projectService.getShortAccounts(),
            this.projectService.getProjects({})
        ]).pipe(map(([categories, accounts, projects]) => {
            return {
                categories: categories,
                accounts: accounts,
                projects: projects
            };
        }));
    }
}

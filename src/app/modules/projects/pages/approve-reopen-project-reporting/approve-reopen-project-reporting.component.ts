import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MessageService } from 'primeng/api';
import { ProjectService } from "src/app/core/api/services";

@Component({
  selector: "app-approve-reopen-project-reporting",
  templateUrl: "./approve-reopen-project-reporting.component.html",
  styleUrls: ["./approve-reopen-project-reporting.component.css"],
})
export class ApproveReopenProjectReportingComponent implements OnInit
{
  userId: string;
  projectId: number;
  projectName: string;
  startDate: string;
  endDate: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectService: ProjectService,
    private messageService: MessageService
  ) {}

  public isLoading: boolean = false;

  ngOnInit() {
    this.isLoading = true;

    this.projectService.approveRequestReopenProjectReporting({
      id: this.route.snapshot.params['pendingRequestId']
    })
    .subscribe({
      next: (res) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: `${res}`
        });

        this.redirectToMainPage();
      },
      error: _ => this.redirectToMainPage()
    });
  }

  redirectToMainPage() {
    this.isLoading = false;
    this.router.navigate(['']);
  }  
}

import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { CustomDayOff, ReserveEmployeeInformation } from 'src/app/core/api/models';
import { ReserveService, VacationService } from 'src/app/core/api/services';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { UserProfileService } from 'src/app/core/services/user-profile.service';
import { sanitizeFormInput, showValidationErrors } from 'src/app/core/helpers/reactive-forms.helper';

@Component({
  selector: 'app-reserve',
  templateUrl: './reserve.component.html',
  styleUrls: ['./reserve.component.css']
})
export class ReserveComponent implements OnInit {
  fullNameSortField: string = 'employeeFullName';
  lastReportTimestampSortField: string = 'lastReportTimestamp';
  currentVacationBalance = 'currentVacationBalance';
  afterVacationTakenSortField = 'afterVacationTaken';

  vacationPenaltyDefaultAmount = -5;

  userName: string;
  reserveEmployeesInformation: ReserveEmployeeInformation[];
  selectedReserveEmployeeInfo: ReserveEmployeeInformation;
  vacationPenaltyForm: FormGroup;

  penaltyDialogVisible: boolean = false;
  currentDate: Date = new Date();

  get isCurrentMonthJanuary(): boolean {
    return this.currentDate.getMonth() === 0;
  }

  constructor(
    private reserveService: ReserveService,
    private vacationService: VacationService,
    private messageService: MessageService,
    private userProfileService: UserProfileService,
  ) { }

  ngOnInit(): void {
    this.vacationPenaltyForm = new FormGroup({
      amount: new FormControl('', [
        Validators.required,
        Validators.min(-30),
        Validators.max(-1),
        Validators.pattern(/\-\d*(\.?\d+)/)
      ]),
      comment: new FormControl('', [
        Validators.required,
        Validators.maxLength(100)
      ])
    });

    this.reserveService.getReserveEmployeesInformation()
      .subscribe(reserveEmployeesInformation => {
        this.reserveEmployeesInformation = reserveEmployeesInformation;
        this.userName = this.userProfileService.userProfile?.name;
      });
  }

  getEventValue($event: any): string {
    return $event.target.value;
  }

  onShowVacationPenaltyDialog(reserveEmployeeInfo: ReserveEmployeeInformation) {
    this.selectedReserveEmployeeInfo = reserveEmployeeInfo;

    this.vacationPenaltyForm.reset({
      amount: '',
      comment: reserveEmployeeInfo?.vacationPenalty?.description
        || `Penalty for not spending vacations while being in reserve. Applied by ${this.userName}`
    });

    this.showVacationPenaltyDialog();
  }

  onSubmitVacationPenalty() {
    const employeeFullName = this.selectedReserveEmployeeInfo.employeeFullName;
    if (this.selectedReserveEmployeeInfo?.vacationPenalty?.extraDayOff
      && this.selectedReserveEmployeeInfo?.vacationPenalty?.description) {
      const employeeVacationPenalty = this.selectedReserveEmployeeInfo.vacationPenalty;
      const amount = +this.vacationPenaltyForm.get('amount').value;
      this.vacationService.editCustomDayOff({
        body: {
          ...employeeVacationPenalty,
          extraDayOff: employeeVacationPenalty.extraDayOff + amount,
          description: this.vacationPenaltyForm.get('comment').value
        }
      }).subscribe(customDayOff => {
        this.messageService.add({
          severity: "success",
          summary: "Done",
          detail: `Penalty for not spending vacations was applied for ${employeeFullName}`
        });

        this.updateReserveEmployeeInfo(customDayOff, amount);
      });
    } else {
      this.vacationService.createCustomDayOff({
        body: {
          extraDayOff: this.vacationPenaltyForm.get('amount').value,
          description: this.vacationPenaltyForm.get('comment').value,
          userId: this.selectedReserveEmployeeInfo.employeeId,
          year: this.currentDate.getFullYear(),
          month: this.currentDate.getMonth(),
        }
      }).subscribe(customDayOff => {
        this.messageService.add({
          severity: "success",
          summary: "Done",
          detail: `Penalty for not spending vacations was applied for ${employeeFullName}`
        });

        this.updateReserveEmployeeInfo(customDayOff, customDayOff.extraDayOff);
      });
    }

    this.hideVacationPenaltyDialog();
  }

  private showVacationPenaltyDialog() {
    this.penaltyDialogVisible = true;
  }

  private hideVacationPenaltyDialog() {
    this.selectedReserveEmployeeInfo = null;
    this.penaltyDialogVisible = false;
  }

  private updateReserveEmployeeInfo(customDayOff: CustomDayOff, vacationPenalty: number) {
    const reserveEmployeeInfoToUpdate = this.reserveEmployeesInformation.find(x => x.employeeId === customDayOff.userId);
    if (reserveEmployeeInfoToUpdate) {
      const updatedCurrentVacationBalance = reserveEmployeeInfoToUpdate.currentVacationBalance + vacationPenalty;
      const index = this.reserveEmployeesInformation.indexOf(reserveEmployeeInfoToUpdate);

      if (updatedCurrentVacationBalance > 0) {
        this.reserveEmployeesInformation[index] = {
          ...reserveEmployeeInfoToUpdate,
          penaltyAmountForCurrentYear: reserveEmployeeInfoToUpdate.penaltyAmountForCurrentYear + vacationPenalty,
          currentVacationBalance: updatedCurrentVacationBalance,
          vacationPenalty: customDayOff
        };
      } else {
        this.reserveEmployeesInformation.splice(index, 1);
      }
    }
  }

  private negativeNumberValidate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.value >= 0 || control.value === '-' ? { negativeNumberValue: { value: control.value } } : null;
    }
  }

  sanitizeAddNumericInput(event: Event): void {
    sanitizeFormInput(event, 'amount', this.vacationPenaltyForm, /[^0-9-]|(?!^)-/g);
  }

  showVacationPenaltyFormValidationErrors(fieldName: string){
    return showValidationErrors(fieldName, this.vacationPenaltyForm);
  }
}

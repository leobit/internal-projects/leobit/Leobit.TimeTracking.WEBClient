import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReserveComponent } from "./pages/reserve.component";
import { TableModule } from "primeng/table";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "src/app/core/guards/auth.guard";
import { ButtonModule } from "primeng/button";
import { KeyFilterModule } from "primeng/keyfilter";
import { FieldsetModule } from "primeng/fieldset";
import { TooltipModule } from "primeng/tooltip";
import { CheckboxModule } from 'primeng/checkbox';
import { DialogModule } from "primeng/dialog";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { InputTextareaModule } from "primeng/inputtextarea";
import { InputTextModule } from "primeng/inputtext";
import { SignedDecimalPipe } from "src/app/shared/pipes/signed-decimal.pipe";

const reserveRoutes: Routes = [
  {
    path: '',
    component: ReserveComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TableModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    KeyFilterModule,
    FieldsetModule,
    TooltipModule,
    CheckboxModule,
    DialogModule,
    RouterModule.forChild(reserveRoutes),
    SignedDecimalPipe
  ],
  declarations: [ ReserveComponent ],
  providers: []
})
export class ReserveModule {}

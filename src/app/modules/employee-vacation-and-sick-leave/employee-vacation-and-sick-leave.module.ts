import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SharedModule } from './../../shared/shared.module';

import { EmployeeVacationAndSickLeaveComponent } from './pages/employee-vacation-and-sick-leave/employee-vacation-and-sick-leave.component';
import { EmployeeVacationAndSickLeaveResolver } from './pages/employee-vacation-and-sick-leave/employee-vacation-and-sick-leave.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { FieldsetModule } from 'primeng/fieldset';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';

const EmployeeVacationAndSickLeaveRoutes: Routes = [
  {
    path: '',
    component: EmployeeVacationAndSickLeaveComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: EmployeeVacationAndSickLeaveResolver }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(EmployeeVacationAndSickLeaveRoutes),
    SharedModule,
    FieldsetModule,
    DialogModule,
    ButtonModule,
    DropdownModule,
    SharedModule, 
    CheckboxModule,
    InputSwitchModule
  ],
  declarations: [EmployeeVacationAndSickLeaveComponent],
  providers: [EmployeeVacationAndSickLeaveResolver]
})
export class EmployeeVacationAndSickLeaveModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { filter, map, Observable, take } from 'rxjs';

import { OAuthService } from 'angular-oauth2-oidc';

import { SelectItem, MessageService } from 'primeng/api';

import { VacationService, SickLeaveService, TimeOffService,  UserService } from 'src/app/core/api/services';
import { VacationInformation, SickLeaveInformation, SickLeaveMonthData, CustomDayOff, UserShort } from 'src/app/core/api/models';
import { EmployeeVacationAndSickLeavePreloaded } from './employee-vacation-and-sick-leave.resolver';
import { UserProfileService } from 'src/app/core/services/user-profile.service';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';

@Component({
  selector: 'app-employee-vacation-and-sick-leave',
  templateUrl: './employee-vacation-and-sick-leave.component.html',
  styleUrls: ['./employee-vacation-and-sick-leave.component.css']
})
export class EmployeeVacationAndSickLeaveComponent implements OnInit {
  currentYear: number = (new Date()).getFullYear();
  selectedYear: Date = new Date();
  employmentDate: Date;
  isTMEmployee: boolean;
  terminationDate: Date | null;
  vacationInformation: VacationInformation;
  showInactiveUsers: boolean;
  inactiveUsers: UserShort[];
  extraDaysOff: number;
  compensatedVacationLeftovers: number;
  sickLeaveInformation: SickLeaveInformation;
  sickLeaveWithoutCertificate: number[];
  sickLeavewithCertificate: number[];

  userDropdownItems: SelectItem[];
  selectedUserDropdownItem: any;

  customDayOffs$: Observable<CustomDayOff[]>;
  allCustomDayOffs$: Observable<CustomDayOff[]>;


  years: SelectItem[] = [];

  public get canTransferVacation(): boolean {
    return (this.userProfileService.userProfile.TransferVacationBalance & 2) > 0;
  }

  public get hasRWPermission(): boolean {
    return this.userProfileService.userProfile.EmployeeVacationAndSickLeave === 3;
  }

  public canReadVacation(): boolean {
    return (this.userProfileService.userProfile.EmployeeVacationAndSickLeave & 1) > 0;
  }

  dialogVisible: boolean = false;

  constructor(
    private oauthService: OAuthService,
    private route: ActivatedRoute,
    private vacationService: VacationService,
    private sickLeaveService: SickLeaveService,
    private timeOffService: TimeOffService,
    private userProfileService: UserProfileService,
    private messageService: MessageService,
    private userService: UserService) { }

  ngOnInit() {
    var preloaded = <EmployeeVacationAndSickLeavePreloaded>this.route.snapshot.data.preloaded;
    this.years = [... new Array(new Date().getFullYear() - 2019)].map((_, i) => 2019 + i + 1).map<SelectItem>(y => ({ label: y.toString(), value: y }));

    this.setUserDropdownItems();
    this.selectedUserDropdownItem = this.oauthService.getIdentityClaims()['sub'];
    this.vacationInformation = preloaded.vacationInformation;
    this.employmentDate = this.vacationInformation?.employmentDate ? new Date(this.vacationInformation.employmentDate) : null;
    this.isTMEmployee = this.vacationInformation?.isTMEmployee;
    this.terminationDate = !!this.vacationInformation.terminationDate ? new Date(this.vacationInformation.terminationDate) : null;
    this.sickLeaveInformation = preloaded.sickLeaveInformation;
    this.sickLeaveWithoutCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withoutCertificate);
    this.sickLeavewithCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withCertificate);
    this.extraDaysOff = this.vacationInformation.extraDaysOff;
    this.compensatedVacationLeftovers = this.vacationInformation.compensatedVacationLeftovers;
    this.updateAllCustomDayOffs();
  }

  selectedUserChanged(userId) {
    this.selectedUserDropdownItem = userId;
    this.updateVacationAndSickLeaveInformation();
    this.updateCustomDayOffs();
  }

  previousYearClick() {
    this.selectedYear.setFullYear(this.selectedYear.getFullYear() - 1);
    this.updateVacationAndSickLeaveInformation();
    this.updateAllCustomDayOffs();
  }

  nextYearClick() {
    this.selectedYear.setFullYear(this.selectedYear.getFullYear() + 1);
    this.updateVacationAndSickLeaveInformation();
    this.updateAllCustomDayOffs();
  }

  updateInformation(event) {
    this.employmentDate = event;
    this.updateVacationAndSickLeaveInformation();
    this.updateCustomDayOffs();
  }

  setUserDropdownItems() {
    var preloaded = <EmployeeVacationAndSickLeavePreloaded>this.route.snapshot.data.preloaded;
    let users = preloaded.users;

    if (this.showInactiveUsers) {
      users = [...preloaded.users, ...this.inactiveUsers];
    }

    this.userDropdownItems = users
      .map<SelectItem>(u => ({
        label: userToStringWithMail(u),
        value: u.id
      }));
  }

  onShowInactiveChange() {
    if (this.showInactiveUsers && (!this.inactiveUsers || this.inactiveUsers.length === 0)) {
      this.userService.getInactiveUsers()
        .subscribe((data => {
          this.inactiveUsers = data;
          this.setUserDropdownItems();
        }));
    }
    else{
      this.setUserDropdownItems();
    }
  }

  private updateVacationAndSickLeaveInformation() {
    this.timeOffService.getEmployeeTimeOffs({ employeeId: this.selectedUserDropdownItem, year: this.selectedYear.getFullYear()})
      .subscribe(data => {
        this.vacationInformation = data;
        this.employmentDate = this.vacationInformation?.employmentDate ? new Date(this.vacationInformation.employmentDate) : null;
        this.terminationDate = !!this.vacationInformation.terminationDate ? new Date(this.vacationInformation.terminationDate) : null;
        this.sickLeaveInformation = {...data, yearData: data.sickLeaveYearData};
        this.sickLeaveWithoutCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withoutCertificate);
        this.sickLeavewithCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withCertificate);
        this.extraDaysOff = data.extraDaysOff;
        this.compensatedVacationLeftovers = data.compensatedVacationLeftovers;
      });
  }

  private updateCustomDayOffs() {
    this.customDayOffs$ = this.allCustomDayOffs$.pipe(
      map(allCustomDayOffs =>
        allCustomDayOffs
          .filter(cdo => cdo.userId === this.selectedUserDropdownItem)
          .map(cdo => ({ ...cdo, lastEditTimestamp: cdo.lastEditTimestamp + "Z" }))
      )
    );
  }

  private getCanWriteVacationObservable(): Observable<boolean> {
    return this.userProfileService.userProfile$.pipe(
      filter(userProfile => !!userProfile && typeof userProfile.EmployeeVacationAndSickLeave === "number"),
      take(1),
      map(userProfile => (userProfile.EmployeeVacationAndSickLeave & 2) > 0)
    );
  }

  private updateAllCustomDayOffs() {
    this.getCanWriteVacationObservable().subscribe(canWrite => {
      this.allCustomDayOffs$ = canWrite
        ? this.vacationService.getAllCustomDayOffsByYear({ year: this.selectedYear.getFullYear() })
        : this.vacationService.getEmployeeCustomDayOffs({
            employeeId: this.selectedUserDropdownItem,
            year: this.selectedYear.getFullYear()
          });
      this.updateCustomDayOffs();
    });
  }

  transferVacation(year: Date) {
    this.vacationService.transferVacationBalance({
      year: year.getFullYear()
    }).subscribe(() => {
      this.messageService.add({ severity: "success", summary: "Done", detail: "Vacation balance transfer completed" });
    });

    this.hideDialog();
  }

  private yearDataToArray(yearData: {
    january?: any
    february?: any
    march?: any
    april?: any
    may?: any
    june?: any
    july?: any
    august?: any
    september?: any
    october?: any
    november?: any
    december?: any
  }, propertyAccessor: (x?: any) => number): number[] {
    return [
      propertyAccessor(yearData.january || {}),
      propertyAccessor(yearData.february || {}),
      propertyAccessor(yearData.march || {}),
      propertyAccessor(yearData.april || {}),
      propertyAccessor(yearData.may || {}),
      propertyAccessor(yearData.june || {}),
      propertyAccessor(yearData.july || {}),
      propertyAccessor(yearData.august || {}),
      propertyAccessor(yearData.september || {}),
      propertyAccessor(yearData.october || {}),
      propertyAccessor(yearData.november || {}),
      propertyAccessor(yearData.december || {}),
    ];
  }

  showDialog() {
    this.dialogVisible = true;
  }

  hideDialog() {
    this.dialogVisible = false;
  }

  onApiChanged() {
    this.updateVacationAndSickLeaveInformation();
  }
}

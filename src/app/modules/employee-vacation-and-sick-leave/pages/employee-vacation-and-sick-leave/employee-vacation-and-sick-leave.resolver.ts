import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService, TimeOffService } from 'src/app/core/api/services';
import { VacationInformation, SickLeaveInformation, UserShort } from 'src/app/core/api/models';

export interface EmployeeVacationAndSickLeavePreloaded {
  users: UserShort[]
  vacationInformation: VacationInformation
  sickLeaveInformation: SickLeaveInformation
}

@Injectable()
export class EmployeeVacationAndSickLeaveResolver  {
  constructor(private userService: UserService, private timeOffService: TimeOffService,) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<EmployeeVacationAndSickLeavePreloaded> {
    return forkJoin([
      this.userService.getActiveUsers(),
      this.timeOffService.getTimeOffsInformation(), 
    ]).pipe(map(([users, timeOffsInformation]) => {
        return {
          users: users,
          vacationInformation: timeOffsInformation,
          sickLeaveInformation: {...timeOffsInformation, yearData: timeOffsInformation.sickLeaveYearData}
        };
      }));
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TimeReportService, UserService } from 'src/app/core/api/services';
import { MessageService, SelectItem, ConfirmationService } from 'primeng/api';
import { EmployeeReportingPreloaded } from './employee-reporting.resolver';
import { Globals } from 'src/app/core/globals';
import { UserShort, ProjectShort, TimeReport, TimeReportBatchCommitDto, TimeReportCommitDetails, EmployeeReportingFilters } from 'src/app/core/api/models';
import { Dialog } from 'primeng/dialog';
import { saveAs } from 'file-saver';
import { OAuthService } from 'angular-oauth2-oidc';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { CalendarDateRange } from 'src/app/shared/components/calendar/models/calendar-date-range';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';
import { UserProfileService } from 'src/app/core/services/user-profile.service';

@Component({
  selector: 'app-employee-reporting',
  templateUrl: './employee-reporting.component.html',
  styleUrls: ['./employee-reporting.component.css']
})
export class EmployeeReportingComponent implements OnInit {
  //#region Side filter panel
  calendarDateRange: CalendarDateRange;
  isShowInactiveUsers: boolean = false;
  isExternalOnly: boolean = false;
  users: SelectItem[];
  activeUsers: UserShort[];
  inactiveUsers: UserShort[];
  projects: SelectItem[];

  selectedTimeReportBatchCommitDetailsId: string;
  selectedTimeReportBatchCommitDetails: TimeReportCommitDetails[] = [];

  selectedUserIds: string[] = [];
  selectedProjectIds: number[] = [];
  timeReportBatchCommits: TimeReportBatchCommitDto[] = [];
  public autoResize: boolean = true;

  ngOnInit() {
    this.hasTimeReportHistoryPermission = (this.oauthService.getIdentityClaims() as any).TimeReportHistory;

    const preloaded = <EmployeeReportingPreloaded>this.route.snapshot.data.preloaded;

    this.setUserDropdownItems();
    this.calendarDateRange = preloaded.calendarDateRange;
    this.activeUsers = preloaded.activeUsers;

    this.projects = preloaded.projects.map<SelectItem>(p => ({
      value: p.id,
      label: this.projectToString(p),
      disabled: p.isInternal
    }));

    this.selectedProjectIds = preloaded.projects.map(p => p.id);

    this.timeReportBatchCommits = preloaded.timeReportHistoryCommits;

    this.userShorts = [...preloaded.activeUsers];

    this.addDropdownUsers = this.users.map<SelectItem>(u => ({
      label: u.label,
      value: u.value
    }));

    this.addDropdownProjects = this.projects.map<SelectItem>(p => ({
      label: p.label,
      value: p.value
    }));

    this.editDropdownProjects = this.projects.map<SelectItem>(p => ({
      label: p.label,
      value: p.value
    }));

    this.projectShorts = preloaded.projects;
  }

  isInternalProject(selectedProjectId: number): boolean {
    const selectedProject = this.projects.find(p => p.value === selectedProjectId);
    if (!selectedProject || selectedProject.disabled === undefined) {
      return false;
    }
    return selectedProject.disabled;
    }

  apply() {
    this.selectedTimeReports = [];

    var projectIds =
      (this.selectedProjectIds.length == 0 && this.projects.length != 0) ? [-1] : // nothing selected
        this.selectedProjectIds.length == this.projects.length ? [] : // all selected
          this.selectedProjectIds;

    var userIds =
      (this.selectedUserIds.length == 0 && this.users.length != 0) ? [""] : this.selectedUserIds;

    this.timeReportService.getTeamTimeReports({
      dateFrom: Date.CreateUTCDate(this.calendarDateRange.dateFrom).toISOString(),
      dateTo: Date.CreateUTCDate(this.calendarDateRange.dateTo || this.calendarDateRange.dateFrom).toISOString(),
      userIds: userIds,
      projectIds: projectIds,
    }).subscribe(timeReports => {
      this.timeReports = this.isExternalOnly ? timeReports.filter(r => r.externalHours > 0) : timeReports;
      this.calculateTotals(timeReports);
    });
  }

  get selectedReportingFilters(): EmployeeReportingFilters {
    return {
      dateRange: this.calendarDateRange.toDateRangeFromApi(),
      userIds: {
        allSelected: this.selectedUserIds.length == this.users.length,
        selectedItems: this.selectedUserIds.length == this.users.length ? null : this.selectedUserIds
      },
      projectIds: {
        allSelected: this.selectedProjectIds.length == this.projects.length,
        selectedItems: this.selectedProjectIds.length == this.projects.length ? null : this.selectedProjectIds
      }
    } as EmployeeReportingFilters;
  }

  get hasRWPermission(): boolean {
    return this.userProfileService.userProfile.EmployeeVacationAndSickLeave === 3;
  }

  selectedReportingFiltersChange(reportingFilters: EmployeeReportingFilters) {
    if (reportingFilters) {
      this.calendarDateRange = new CalendarDateRange(reportingFilters.dateRange);
      if (reportingFilters.userIds.allSelected) {
        this.selectedUserIds = this.users.map(u => u.value);
      }
      else {
        this.selectedUserIds = reportingFilters.userIds.selectedItems;
      }
      if (reportingFilters.projectIds.allSelected) {
        this.selectedProjectIds = this.projects.map(p => p.value);
      }
      else {
        this.selectedProjectIds = reportingFilters.projectIds.selectedItems;
      }
    }
  }

  selectedReportingFiltersReset() {
    const preloaded = <EmployeeReportingPreloaded>this.route.snapshot.data.preloaded;
    this.calendarDateRange = preloaded.calendarDateRange;

    this.selectedProjectIds = preloaded.projects.map(p => p.id);
    this.selectedUserIds = [];
  }

  onShowInactiveUsersChange(event: any) {
    if (this.isShowInactiveUsers) {
      if ((!this.inactiveUsers || this.inactiveUsers.length === 0)) {
        this.userService.getInactiveUsers()
          .subscribe((data => {
            this.inactiveUsers = data;
            this.setUserDropdownItems();
          }));
      }
      else {
        this.setUserDropdownItems();
      }
    } else {
      this.setUserDropdownItems();
      this.selectedUserIds = this.selectedUserIds.filter(id => this.activeUsers.find(u => u.id === id));
    }
  }

  //#endregion

  //#region Total table
  totals: any[];
  totalInternal: number;
  totalExternal: number;

  calculateTotals(timeReports: TimeReport[]) {
    var totals = [];
    var totalInternal = 0;
    var totalExternal = 0;
    for (let timeReport of timeReports) {
      var existingTotal = totals.find(t => t.project.id == timeReport.project.id);
      if (existingTotal) {
        existingTotal.sumInternal += timeReport.internalHours;
        existingTotal.sumExternal += timeReport.externalHours;
      } else {
        totals.push({
          account: timeReport.account,
          project: timeReport.project,
          sumInternal: timeReport.internalHours,
          sumExternal: timeReport.externalHours
        });
      }
      totalInternal += timeReport.internalHours;
      totalExternal += timeReport.externalHours;
    }
    this.totals = totals;
    this.totalInternal = totalInternal;
    this.totalExternal = totalExternal;
  }
  //#endregion

  //#region Main table
  timeReports: TimeReport[];
  selectedTimeReports: TimeReport[];
  //#endregion
  //#region Export
  onExportExcel() {
    this.timeReportService.exportTimeReportsToExcel({
      body: {
        userIds: this.selectedTimeReports.map(tr => tr.user.id),
        ids: this.selectedTimeReports.map(tr => tr.id)
      }
    }).subscribe(x => saveAs(x));
  }

  onExportWord() {
    this.timeReportService.exportTimeReportsToWord({
      body: {
        userIds: this.selectedTimeReports.map(tr => tr.user.id),
        ids: this.selectedTimeReports.map(tr => tr.id),
        dateFrom: Date.CreateUTCDate(this.calendarDateRange.dateFrom).toISOString(),
        dateTo: Date.CreateUTCDate(this.calendarDateRange.dateTo).toISOString()
      }
    }).subscribe(x => saveAs(x));
  }
  //#endregion

  //#region Add
  @ViewChild("addDialog", { static: true })
  addDialog: Dialog;
  addDialogVisible: boolean;

  hasTimeReportHistoryPermission: boolean;

  addDropdownUsers: SelectItem[];
  addDropdownProjects: SelectItem[];

  addModel = {
    userId: '',
    projectId: 0,
    internal: 8,
    external: 8,
    workDescription: 'Added by manager'
  };

  addReportingDate = new Date();

  resetAddModel() {
    this.addModel = {
      userId: '',
      projectId: 0,
      internal: 8,
      external: 8,
      workDescription: 'Added by manager'
    };

    this.isAddButtonDisabled = true;

    this.addReportingDate = new Date();
  }

  private showAddDialog() {
    this.addDialogVisible = true;
    setTimeout(() => {
      this.addDialog.center();
    }, 10);
  }

  hideAddDialog() {
    this.addDialogVisible = false;
  }

  onAddTimeReport() {
    this.resetAddModel();
    this.resetValidation();
    this.showAddDialog();
  }

  onAddTimeReportSave() {
    const internalHours = +this.addModel.internal;
    const externalHours = +this.addModel.external;

    if (internalHours > 16 || externalHours > 16) {
      this.messageService.add({
        severity: "error",
        summary: "error",
        detail: "Internal or external hours cannot exceed 16!"
      });
      return;
    }

    var timeReportToCreate: TimeReport = {
      id: 0,
      user: this.userShorts.find(u => u.id == this.addModel.userId) || this.userShorts[0],
      project: this.projectShorts.find(p => p.id == this.addModel.projectId) || this.projectShorts[0],
      reportingDate: Date.CreateUTCDate(this.addReportingDate).toISOString(),
      internalHours: internalHours,
      externalHours: externalHours,
      isOverhead: false,
      workDescription: this.addModel.workDescription,
      sendEmail: false,
      isEditable: true
    };

    this.timeReportService.createTimeReport({ body: timeReportToCreate }).subscribe((createdTimeReport) => {
      timeReportToCreate.id = createdTimeReport.id;

      this.messageService.add({ severity: "success", summary: "Done", detail: `Project name '${timeReportToCreate.project.account.name}-${timeReportToCreate.project.name}' created.` });

      this.timeReports = [...this.timeReports, timeReportToCreate];

      this.calculateTotals(this.timeReports);
    });

    this.hideAddDialog();
    this.resetAddModel();
  }

  //#endregion

  //#region Edit
  @ViewChild("editDialog", { static: true })
  editDialog: Dialog;
  editDialogVisible: boolean;

  editDropdownProjects: SelectItem[];
  isOneReportSelected = () => this.selectedTimeReports.length === 1;

  editModel = {
    setProject: false,
    selectedProjectId: 0,
    setInternal: false,
    internal: 0,
    setExternal: false,
    external: 0
  };

  private resetEditModel() {
    this.editModel = {
      setProject: false,
      selectedProjectId: null,
      setInternal: false,
      internal: 0,
      setExternal: false,
      external: 0
    };

    if(this.isOneReportSelected())
    {
      const selectedReport = this.selectedTimeReports[0];
      this.editModel.selectedProjectId = selectedReport.project.id;
      this.editModel.internal = selectedReport.internalHours;
      this.editModel.external = selectedReport.externalHours;
    }
  }

  private showEditDialog() {
    this.editDialogVisible = true;
    setTimeout(() => {
      this.editDialog.center();
    }, 10);
  }

  hideEditDialog() {
    this.editDialogVisible = false;
  }

  onEditTimeReports() {
    this.resetEditModel();
    this.resetValidation();
    this.showEditDialog();
  }

  onSetInternalChange = () => !this.isOneReportSelected() && (this.isEditButtonDisabled = false);
  onSetExternalChange = () => !this.isOneReportSelected() && (this.isEditButtonDisabled = false);

  onEditTimeReportsSave() {
    var timeReportsToUpdate = <TimeReport[]>JSON.parse(JSON.stringify(this.selectedTimeReports)); // copy
    timeReportsToUpdate.forEach(tr => {
      if (this.editModel.setProject) tr.project = this.projectShorts.find(p => p.id == this.editModel.selectedProjectId) || this.projectShorts[0];
      if (this.editModel.setInternal) tr.internalHours = +this.editModel.internal;
      if (this.editModel.setExternal) tr.externalHours = +this.editModel.external;
    });

    this.timeReportService.editTimeReports({ body: timeReportsToUpdate }).subscribe((updatedTimeReports) => {
      this.hideEditDialog();


      this.selectedTimeReports.forEach(tr => {
        if (this.editModel.setProject) tr.project = this.projectShorts.find(p => p.id == this.editModel.selectedProjectId) || this.projectShorts[0];
        if (this.editModel.setInternal) tr.internalHours = +this.editModel.internal;
        if (this.editModel.setExternal) tr.externalHours = +this.editModel.external;
      });

      if (this.hasTimeReportHistoryPermission) {
        this.timeReportService.getTimeReportBatchCommits().subscribe(commits => this.timeReportBatchCommits = commits);
      }

      const updatedTimeReport = (this.selectedTimeReports.length > 0) ? this.selectedTimeReports[0] : null;

      if (updatedTimeReport) {
        this.messageService.add({ severity: "success", summary: "Done", detail: `${updatedTimeReports.length > 1 ? 'Projects' : 'Project'} with ${updatedTimeReports.length > 1 ? 'names' : 'name'} '${updatedTimeReport.account.name}-${updatedTimeReport.project.name}' updated.` });
      }

      this.calculateTotals(this.timeReports);
    });
  }
  //#endregion

  //#region Delete
  onTimeReportBatchCommitDetails(id) {
    if (this.selectedTimeReportBatchCommitDetailsId === id) {
      this.selectedTimeReportBatchCommitDetailsId = ""
      setTimeout(() => {
        this.historyDialog.center();
      }, 10);
    } else {
      this.selectedTimeReportBatchCommitDetailsId = id;
      this.timeReportService.getTimeReportBatchCommitDetails({ id }).subscribe(response => {
        this.selectedTimeReportBatchCommitDetails = response;
        setTimeout(() => {
          this.historyDialog.center();
        }, 0);
      });
    }
  }

  onRevertTimeReportBatchCommit(id) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to revert this and all changes above?`,
      accept: () => {
        this.timeReportService.revertTimeReportBatchCommit({ id }).subscribe(_ => {
          this.timeReportService.getTimeReportBatchCommits().subscribe(commits => this.timeReportBatchCommits = commits);
          this.messageService.add({ severity: "success", summary: "Done" });
          this.apply();
        });
      }
    });
  }

  onDeleteTimeReports() {
    this.confirmationService.confirm({
      message: `Are you sure that you want to delete selected reports?`,
      accept: () => {
        var timeReportIds = this.selectedTimeReports.map(tr => tr.id);

        this.timeReportService.deleteTimeReports({ ids: timeReportIds }).subscribe(() => {
          this.messageService.add({ severity: "success", summary: "Done" });
          this.selectedTimeReports = [];
          this.timeReports = this.timeReports.filter(timeReport => timeReportIds.indexOf(timeReport.id) === -1);
          if (this.hasTimeReportHistoryPermission) {
            this.timeReportService.getTimeReportBatchCommits().subscribe(commits => this.timeReportBatchCommits = commits);
          }
          this.calculateTotals(this.timeReports);
        });
      }
    });
  }
  //#endregion

  //#region Numbers validation
  validateNumber(value: number): boolean {
    const regex = /^(0|[1-9]\d*)(\.5)?$/;
    return regex.test(value.toString());
  }
  isEditButtonDisabled = true;
  isAddButtonDisabled = true;
  isEditInternalValid = true;
  isEditExternalValid = true;
  isAddInternalValid = true;
  isAddExternalValid = true;
  hoursErrorMessage = 'This number must be a multiple of 0.5. Like 0.5, 1, 1.5, 2, 2.5, etc.';

  validateEdit(): void {
    this.isEditInternalValid = this.validateNumber(this.editModel.internal);
    this.isEditExternalValid = this.validateNumber(this.editModel.external);
    this.isEditButtonDisabled = !this.isEditInternalValid || !this.isEditExternalValid;
  }

  validateAdd(): void {
    this.isAddInternalValid = this.validateNumber(this.addModel.internal);
    this.isAddExternalValid = this.validateNumber(this.addModel.external);
    this.isAddButtonDisabled = this.addModel.userId === '' || this.addModel.projectId === 0 || !this.isAddInternalValid || !this.isAddExternalValid;
  }

  resetValidation(): void {
    this.isEditButtonDisabled = true;
    this.isAddButtonDisabled = true;
    this.isEditInternalValid = true;
    this.isEditExternalValid = true;
    this.isAddInternalValid = true;
    this.isAddExternalValid = true;
  }

  //#endregion

  @ViewChild("historyDialog", { static: true })
  historyDialog: Dialog;
  historyDialogVisible: boolean;
  userShorts: UserShort[];
  projectShorts: ProjectShort[];

  showHistoryDialog() {
    this.historyDialogVisible = true;
    setTimeout(() => {
      this.historyDialog.center();
    }, 10);
  }

  constructor(private route: ActivatedRoute,
    private timeReportService: TimeReportService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private oauthService: OAuthService,
    private tabPermissionsService: TabPermissionsService,
    private userProfileService: UserProfileService,
    private userService: UserService,
    public globals: Globals
  ) { }

  setUserDropdownItems() {
    var preloaded = <EmployeeReportingPreloaded>this.route.snapshot.data.preloaded;
    this.userShorts = preloaded.activeUsers;

    if (this.isShowInactiveUsers) {
      this.userShorts = [...preloaded.activeUsers, ...this.inactiveUsers];
    }

    this.users = this.userShorts
      .map<SelectItem>(u => ({
        label: userToStringWithMail(u),
        value: u.id
      }));

    this.addDropdownUsers = this.userShorts
      .map<SelectItem>(u => ({
        label: userToStringWithMail(u),
        value: u.id
      }));
  }

  private projectToString(project: ProjectShort) {
    return `${project.account.name} - ${project.name}`
  }

  get hasWritePermission() {
    return this.tabPermissionsService.hasWriteClaim("TimeReport");
  }
}

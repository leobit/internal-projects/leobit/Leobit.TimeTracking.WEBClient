import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { TimeReportService, UserService } from 'src/app/core/api/services';
import { ProjectShort, UserShort, TimeReportBatchCommitDto } from 'src/app/core/api/models';
import { OAuthService } from 'angular-oauth2-oidc';
import { CalendarDateRange } from 'src/app/shared/components/calendar/models/calendar-date-range';

export interface EmployeeReportingPreloaded {
    calendarDateRange: CalendarDateRange;
    activeUsers: UserShort[];
    projects: ProjectShort[];
    timeReportHistoryCommits: TimeReportBatchCommitDto[];
}

@Injectable()
export class EmployeeReportingResolver  {
    constructor(
        private userService: UserService,
        private timeReportService: TimeReportService,
        private oauthService: OAuthService
    ) { }

    resolve(
        _route: ActivatedRouteSnapshot,
        _state: RouterStateSnapshot
    ): Observable<EmployeeReportingPreloaded> {
        const date = new Date(), year = date.getFullYear(), month = date.getMonth();
        const calendarDateRange = new CalendarDateRange({
            dateFrom: new Date(year, month, 1).toISOString(), // first day in month
            dateTo: new Date(year, month + 1, 0).toISOString(), // last day in month
        });

        return forkJoin([
            this.userService.getActiveUsers(),
            this.timeReportService.getTimeReportProjects(),
            (this.oauthService.getIdentityClaims() as any).TimeReportHistory
                ? this.timeReportService.getTimeReportBatchCommits()
                : new Observable<TimeReportBatchCommitDto[]>((obs) => {
                    obs.next(new Array<TimeReportBatchCommitDto>());
                    obs.complete();
                })
        ]).pipe(map(([activeUsers, projects, commits]) => {
            return {
                calendarDateRange: calendarDateRange,
                activeUsers: activeUsers,
                projects: projects,
                timeReportHistoryCommits: commits
            };
        }));
    }
}

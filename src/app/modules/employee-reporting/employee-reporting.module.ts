import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeReportingComponent } from './pages/employee-reporting/employee-reporting.component';
import { EmployeeReportingResolver } from './pages/employee-reporting/employee-reporting.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { CalendarModule } from 'primeng/calendar';
import { MultiSelectModule } from 'primeng/multiselect';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { SharedModule } from 'src/app/shared/shared.module';
import { InputNumberModule } from 'primeng/inputnumber';
import { DropdownModule } from "primeng/dropdown";


const employeeReportingRoutes: Routes = [
  {
    path: '',
    component: EmployeeReportingComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: EmployeeReportingResolver }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(employeeReportingRoutes),
    CalendarModule,
    MultiSelectModule,
    TableModule,
    DialogModule,
    CheckboxModule,
    InputTextareaModule,
    InputNumberModule,
    SharedModule,
    ReactiveFormsModule,
    DropdownModule
  ],
  declarations: [EmployeeReportingComponent],
  providers: [EmployeeReportingResolver]
})
export class EmployeeReportingModule { }

import { Component, OnInit } from '@angular/core';
import { Division, OrganizationUnitShort } from 'src/app/core/api/models';
import { DivisionService } from 'src/app/core/api/services';
import { MessageService } from 'primeng/api';
import { DivisionHierarchyService } from '../../services/division-hierarchy.service';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { noWhiteSpacesValidator } from 'src/app/shared/validators/no-whitespaces-validator';

@Component({
    selector: 'app-divisions',
    templateUrl: './divisions.component.html',
    styleUrls: ['./divisions.component.css']
})
export class DivisionsComponent implements OnInit {
    dialogVisible = false;
    divisions: Division[];
    selectedDivision: Division;

    editDivision: Division = {} as Division;
    form = new FormGroup({
        name: new FormControl('', [Validators.required, Validators.maxLength(100), noWhiteSpacesValidator()]),
    });

    get f() {
        return this.form.controls;
    }
    constructor(private divisionService: DivisionService, private divisionHierarchyService: DivisionHierarchyService,
        private messageService: MessageService, private tabPermissionsService: TabPermissionsService) { }

    ngOnInit() {
        this.loadDivisions();
        this.divisionHierarchyService.getSelectedDivision.subscribe(division => {
            this.selectedDivision = division;
            this.sendToCategories(null);
        });
    }

    sendToCategories(organizationUnit: OrganizationUnitShort) {
        this.divisionHierarchyService.getSelectedOrganizationUnit.next(organizationUnit);
    }

    sendToOrganizationUnits(division: Division) {
        this.divisionHierarchyService.getSelectedDivision.next(division);
    }

    get hasWritePermission() {
        return this.tabPermissionsService.hasWriteClaim("Division");
    }

    loadDivisions() {
        this.divisionService.getDivisions().subscribe(divisions => {
            this.divisions = divisions.sortBy(d => d.name);
            if (this.divisions.length) {
                this.selectedDivision = this.divisions[0];
                this.setSelectedOrganizationUnit(this.selectedDivision);
            }
        });
    }

    setSelectedOrganizationUnit(division: Division) {
        this.divisionHierarchyService.getSelectedDivision.next(division);
    }

    onAddDivision() {
        this.editDivision = {
            id: 0,
            name: 'Division name'
        };
        this.showDialog();
    }

    onEditDivision(division: Division) {
        this.editDivision.id = division.id;
        this.editDivision.name = division.name;
        this.divisions[this.divisions.findIndex(x => x.id == division.id)] = this.editDivision;
        this.showDialog();
    }

    onSaveDivision() {
        if (this.editDivision.id === 0) {
            this.createDivision();
        } else {
            this.updateDivision();
        }
        this.hideDialog();
    }

    private createDivision() {
        this.divisionService.createDivision({ body: this.editDivision }).subscribe(division => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Division '${division.name}' created` });
            this.divisions = [...this.divisions, division];
            this.divisionHierarchyService.updateDivisions.next(division);
        });
    }

    private updateDivision() {
        this.divisionService.editDivision({ body: this.editDivision }).subscribe(division => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Division '${division.name}' updated` });
            this.divisionHierarchyService.updateDivisions.next(division);
        });
    }

    private showDialog() {
        this.dialogVisible = true;
    }

    private hideDialog() {
        this.dialogVisible = false;
    }
}

import { Component, OnInit } from '@angular/core';
import { OrganizationUnit, OrganizationUnitShort, Division } from 'src/app/core/api/models';
import { OrganizationUnitService } from 'src/app/core/api/services';
import { SelectItem, MessageService } from 'primeng/api';
import { DivisionHierarchyService } from '../../services/division-hierarchy.service';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { noWhiteSpacesValidator } from 'src/app/shared/validators/no-whitespaces-validator';

@Component({
    selector: 'app-organization-units',
    templateUrl: './organization-units.component.html',
    styleUrls: ['./organization-units.component.css']
})
export class OrganizationUnitsComponent implements OnInit {
    dialogVisible = false;

    divisions: SelectItem[];
    selectedDivision: Division;

    organizationUnits: OrganizationUnitShort[];
    selectedOrganizationUnit: OrganizationUnitShort;

    editOrganizationUnit: OrganizationUnit = {} as OrganizationUnit;
    
    unitForm = new FormGroup({
        unitName: new FormControl('', [Validators.required, Validators.maxLength(100), noWhiteSpacesValidator()]),
    });

    get f() {
        return this.unitForm.controls;
    }
    constructor(
        private organizationUnitService: OrganizationUnitService,
        private divisionHierarchyService: DivisionHierarchyService,
        private messageService: MessageService,
        private tabPermissionsService: TabPermissionsService) { }

    ngOnInit() {
        this.editOrganizationUnit.division = {} as Division;
        this.loadDivisions();
        
        this.divisionHierarchyService.getSelectedOrganizationUnit.subscribe(organizationUnit => {
            this.selectedOrganizationUnit = organizationUnit;
        });

        this.divisionHierarchyService.getSelectedDivision.subscribe(division => {
            this.selectedDivision = division;

            if (division) {
                this.loadOrganizationUnitsByDivisionId(division.id);
            } else {
                this.organizationUnits = null;
            }

        });

        this.updateDivisions();
    }

    get hasWritePermission() { 
        return this.tabPermissionsService.hasWriteClaim("OrganizationUnit");
    }

    loadOrganizationUnitsByDivisionId(divisionId: number) {
        this.organizationUnitService.getOrganizationUnits({ divisionId: divisionId }).subscribe(organizationUnits => {
            this.organizationUnits = organizationUnits.sortBy(ou => ou.name);
            if (this.organizationUnits.length) {
                if (this.selectedOrganizationUnit == null) {
                    this.selectedOrganizationUnit = this.organizationUnits[0];
                }
                this.sendToCategories(this.selectedOrganizationUnit);
            }
        });
    }

    loadDivisions() {
        this.organizationUnitService.getShortDivisions().subscribe(divisions => {
            this.divisions = divisions.map<SelectItem>(division => {
                return { value: division, label: division.name };
            }).sortBy(ou => ou.label);
        });
    }

    updateDivisions() {
        this.divisionHierarchyService.updateDivisions.subscribe(division => {
            const divisionToUpdate = { value: division, label: division.name } as SelectItem;
            const divisionIndex = this.divisions.findIndex(d => d.value.id === division.id);
            if (divisionIndex >= 0) {
                this.divisions[divisionIndex] = divisionToUpdate;
            } else {
                this.divisions.push(divisionToUpdate);
            }
        });
    }

    sendToCategories(organizationUnit: OrganizationUnitShort) {
        this.divisionHierarchyService.getSelectedOrganizationUnit.next(organizationUnit);
    }

    onAddOrganizationUnit() {
        this.editOrganizationUnit = {
            id: 0,
            name: 'Organization unit name',
            division: this.selectedDivision
        };
        this.showDialog();
    }

    onEditOrganizationUnit(organizationUnit: OrganizationUnitShort) {
        this.editOrganizationUnit = {
            ...organizationUnit, ... { division: this.selectedDivision }
        };
        this.showDialog();
    }

    onSaveOrganizationUnit() {
        if (this.editOrganizationUnit.id === 0) {
            this.createOrganizationUnit();
        } else {
            this.updateOrganizationUnit();
        }
        this.hideDialog();
    }

    private createOrganizationUnit() {
        this.organizationUnitService.createOrganizationUnit({ body: this.editOrganizationUnit }).subscribe(organizationUnit => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Organization unit '${organizationUnit.name}' created` });
            if (this.selectedDivision && organizationUnit.division.id === this.selectedDivision.id) {
                this.organizationUnits = [...this.organizationUnits, organizationUnit];
            }
            this.divisionHierarchyService.updateOrganizationUnits.next(organizationUnit);
            this.divisionHierarchyService.getSelectedOrganizationUnit.next({
                id: organizationUnit.id,
                name: organizationUnit.name,
            });
            this.divisionHierarchyService.getSelectedDivision.next(
                organizationUnit.division
            );
        });
    }

    private updateOrganizationUnit() {
        this.organizationUnitService.editOrganizationUnit({ body: this.editOrganizationUnit }).subscribe(organizationUnit => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Organization unit '${organizationUnit.name}' updated` });
            if (organizationUnit.division.id === this.selectedDivision.id) {
                const organizationUnitToEdit = this.organizationUnits.find(o => o.id === organizationUnit.id);
                Object.assign(organizationUnitToEdit, organizationUnit);
            } else {
                this.organizationUnits = this.organizationUnits.filter(o => o.id !== organizationUnit.id);
            }
            this.divisionHierarchyService.updateOrganizationUnits.next(organizationUnit);
            this.divisionHierarchyService.getSelectedDivision.next(
                organizationUnit.division
            );
            this.divisionHierarchyService.getSelectedOrganizationUnit.next({
                id: organizationUnit.id,
                name: organizationUnit.name,
            });
        });
    }

    private showDialog() {
        this.dialogVisible = true;
    }

    private hideDialog() {
        this.dialogVisible = false;
    }
}

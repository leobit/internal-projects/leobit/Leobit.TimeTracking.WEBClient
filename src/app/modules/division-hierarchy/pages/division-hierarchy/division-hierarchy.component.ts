import { Component, OnDestroy, OnInit } from '@angular/core';
import { DivisionHierarchyService } from '../../services/division-hierarchy.service';
import { UserProfileService } from 'src/app/core/services/user-profile.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-division-hierarchy',
    templateUrl: './division-hierarchy.component.html',
    styleUrls: ['./division-hierarchy.component.css'],
    providers: [DivisionHierarchyService]
})
export class DivisionHierarchyComponent implements OnInit, OnDestroy  {
    userProfile: any;
    private subscription: Subscription = new Subscription();

    constructor(private userprofileService: UserProfileService) { }

    ngOnInit() {
      this.subscription.add(this.userprofileService.userProfile$.subscribe(profile => {
          this.userProfile = profile;
      }));
  }

  ngOnDestroy() {
      this.subscription.unsubscribe();
  }

}

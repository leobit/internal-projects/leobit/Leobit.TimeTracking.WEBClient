import { Component, OnInit, ViewChild } from '@angular/core';
import { Category, OrganizationUnitShort, CategoryShort, ProjectShort, Project, OrganizationUnit } from 'src/app/core/api/models';
import { CategoryService, ProjectService } from 'src/app/core/api/services';
import { SelectItem, MessageService } from 'primeng/api';
import { DivisionHierarchyService } from '../../services/division-hierarchy.service';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { noWhiteSpacesValidator } from 'src/app/shared/validators/no-whitespaces-validator';
export interface EditCategoryModel {
    id?: number;
    name?: null | string;
    organizationUnit?: number;
}

@Component({
    selector: 'app-categories',
    templateUrl: './categories.component.html',
    styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
    orgUnitDropdownItems: SelectItem[]
    selectedOrganizationUnit: OrganizationUnitShort;

    dialogVisible: boolean = false;
    projectsDialogVisible: boolean = false;

    allCategories: Category[];
    categories: CategoryShort[];
    selectedCategory: CategoryShort;

    selectedCategoryDropdownItem: Category;
    selectedProjectDropdownItem: ProjectShort;

    projectDropdownItems: SelectItem[];
    categoryDropdownItems: SelectItem[];

    allProjects: Project[];
    projects: SelectItem[];
    selectedProject: ProjectShort;

    isEditMode: boolean = false;

    editCategory: EditCategoryModel = {} as EditCategoryModel;

    categoryForm = new FormGroup({
        categoryName: new FormControl('', [Validators.required, Validators.maxLength(100), noWhiteSpacesValidator()]),
    });

    get f() {
        return this.categoryForm.controls;
    }

    constructor(
        private categoryService: CategoryService,
        private projectService: ProjectService,
        private divisionHierarchyService: DivisionHierarchyService,
        private messageService: MessageService,
        private tabPermissionsService: TabPermissionsService) { }

    ngOnInit() {
        this.editCategory.organizationUnit = {} as number;
        this.loadOrganizationUnits();
        this.loadAllCategoriesOptions();

        this.divisionHierarchyService.getSelectedOrganizationUnit.subscribe(organizationUnit => {
            this.selectedCategory = null;
            this.selectedOrganizationUnit = organizationUnit;

            if (organizationUnit) {
                this.loadCategoriesByOrganizationUnitId(organizationUnit.id);
            } else {
                this.categories = null;
            }
        });

        this.updateDivisions();
        this.updateOrganizationUnits();
    }

    get hasWritePermission() {
        return this.tabPermissionsService.hasWriteClaim("Category");
    }

    loadAllCategoriesOptions() {
        this.projectService.getShortCategories().subscribe((res: Category[]) => {
            this.allCategories = res.sortBy(c => c.name);
            this.setCategoriesDropdown();
        });
    }

    setCategoriesDropdown() {
        this.categoryDropdownItems = this.allCategories
            .map<SelectItem>(c => ({
                value: c,
                label: `${c.organizationUnit.division.name} - ${c.organizationUnit.name} - ${c.name}`
            }))
            .sortBy(si => si.label);
    }

    setProjectsDropdown() {
        this.projectDropdownItems = this.allProjects
            .filter(p => !p.category || p.category.id !== this.selectedCategory.id)
            .map<SelectItem>(p => ({
                value: p,
                label: `${p.account.name} - ${p.name}`
            }));
        this.selectedProjectDropdownItem = null;
        if (this.projectDropdownItems.length) {
            this.selectedProjectDropdownItem = this.projectDropdownItems[0].value;
        }
    }

    setCategoryProjects() {
        this.projects = this.allProjects
            .filter(p => p.category && p.category.id === this.selectedCategory.id)
            .map<SelectItem>(p => ({
                value: p,
                label: `${p.account.name} - ${p.name}`,
                disabled: true
            }));
        this.selectedCategoryDropdownItem = null;
    }

    loadCategoriesByOrganizationUnitId(organizationUnitId: number) {
        this.categoryService.getCategories({ organizationUnitId: organizationUnitId }).subscribe(categories => {
            this.categories = categories;
        });
    }

    loadOrganizationUnits() {
        this.categoryService.getShortOrganizationUnits().subscribe(organizationUnits => {
            this.orgUnitDropdownItems = organizationUnits.map<SelectItem>(organizationUnit => {
                return { value: organizationUnit.id, label: organizationUnit.name };
            });
        });
    }

    updateOrganizationUnits() {
        this.divisionHierarchyService.updateOrganizationUnits.subscribe(organizationUnit => {
            const organizationUnitToUpdate = { value: organizationUnit.id, label: organizationUnit.name } as SelectItem;
            const organizationUnitIndex = this.orgUnitDropdownItems.findIndex(o => o.value === organizationUnit.id);
            if (organizationUnitIndex >= 0) {
                this.orgUnitDropdownItems[organizationUnitIndex] = organizationUnitToUpdate;
            } else {
                this.orgUnitDropdownItems.push(organizationUnitToUpdate);
            }

            this.allCategories
                .filter(c => c.organizationUnit.id === organizationUnit.id)
                .forEach(c => c.organizationUnit.name = organizationUnit.name);

            this.setCategoriesDropdown();
        });
    }

    updateDivisions() {
        this.divisionHierarchyService.updateDivisions.subscribe(division => {
            this.allCategories
                .filter(c => c.organizationUnit.division.id === division.id)
                .forEach(c => c.organizationUnit.division.name = division.name);
            this.setCategoriesDropdown();
        });
    };

    onAddCategory() {
        this.editCategory = {
            id: 0,
            name: 'Category name',
            organizationUnit: this.selectedOrganizationUnit.id
        };

        this.showDialog();
    }

    onEditCategory(category: CategoryShort) {
        event.stopImmediatePropagation();
        this.editCategory = { ...category, ...{ organizationUnit: this.selectedOrganizationUnit.id } };
        this.showDialog();
    }

    onSaveCategory() {
        if (this.editCategory.id === 0) {
            this.createCategory();
        } else {
            this.updateCategory();
        }
        this.hideDialog();
    }

    private createCategory() {
        let editCategoryBody: Category = {
            id: this.editCategory.id,
            name: this.editCategory.name,
            organizationUnit: this.selectedOrganizationUnit
        }

        this.categoryService.createCategory({ body: editCategoryBody }).subscribe(category => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Category '${category.name}' created` });
            if (this.selectedOrganizationUnit && category.organizationUnit.id === this.selectedOrganizationUnit.id) {
                this.categories = [...this.categories, category];
                this.allCategories = [...this.allCategories, category];
                this.setCategoriesDropdown();
            }
            this.divisionHierarchyService.getSelectedDivision.next(
                category.organizationUnit.division
            );
            this.divisionHierarchyService.getSelectedOrganizationUnit.next({
                id: category.organizationUnit.id,
                name: category.organizationUnit.name,
            });
        });
    }

    private updateCategory() {
        let editCategoryBody: Category = {
            id: this.editCategory.id,
            name: this.editCategory.name,
            organizationUnit: this.selectedOrganizationUnit
        }

        this.categoryService.editCategory({ body: editCategoryBody }).subscribe(category => {
            this.messageService.add({ severity: 'success', summary: 'Done', detail: `Category '${category.name}' updated` });
            if (category.organizationUnit.id === this.selectedOrganizationUnit.id) {
                const categoryToEdit = this.categories.find(c => c.id === category.id);
                Object.assign(categoryToEdit, category);

                this.allCategories.find(c => c.id === category.id).name = category.name;
                this.setCategoriesDropdown();
            } else {
                this.categories = this.categories.filter(c => c.id !== category.id);
            }
            this.divisionHierarchyService.getSelectedDivision.next(
                category.organizationUnit.division
            );
            this.divisionHierarchyService.getSelectedOrganizationUnit.next({
                id: category.organizationUnit.id,
                name: category.organizationUnit.name,
            });
        });
    }

    changeEditMode(project: ProjectShort) {
        this.isEditMode = true;
        this.selectedProject = project;
        this.selectedCategoryDropdownItem = this.allCategories.find(c => c.id === this.selectedCategory.id);
    }

    cancelEditProjectCategory() {
        this.isEditMode = false;
        this.selectedProject = null;
    }

    editProjectCategory(project: ProjectShort & Project, add: boolean) {
        const category = add ? this.selectedCategory : this.selectedCategoryDropdownItem;
        this.categoryService.editCategoryProject({
            categoryId: category.id,
            projectId: project.id
        }).subscribe(() => {
            this.allProjects.find(p => p.id === project.id).category = category;
            this.setCategoryProjects();
            this.setProjectsDropdown();
        });
        this.cancelEditProjectCategory();
    }

    private showDialog() {
        this.dialogVisible = true;
    }

    private hideDialog() {
        this.dialogVisible = false;
    }

    public showProjectsDialog(category: CategoryShort) {
        this.selectedCategory = category;
        if (!this.allCategories) {
            this.loadAllCategoriesOptions();
        }
        if (!this.allProjects) {
            this.projectService.getProjects({}).subscribe(res => {
                this.allProjects = res;
                this.setCategoryProjects();
                this.setProjectsDropdown();
            });
        } else {
            this.setCategoryProjects();
            this.setProjectsDropdown();
        }
        this.projectsDialogVisible = true;
    }
}

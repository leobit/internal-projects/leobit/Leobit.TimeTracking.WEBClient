import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { DivisionHierarchyComponent } from './pages/division-hierarchy/division-hierarchy.component';
import { DivisionsComponent } from './pages/divisions/divisions.component';
import { OrganizationUnitsComponent } from './pages/organization-units/organization-units.component';
import { CategoriesComponent } from './pages/categories/categories.component';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { DialogModule } from 'primeng/dialog';
import { ListboxModule } from 'primeng/listbox';
import { SharedModule } from 'src/app/shared/shared.module';

const divisionHierarchyRoutes: Routes = [
    {
        path: '',
        component: DivisionHierarchyComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(divisionHierarchyRoutes),
        InputTextModule,
        TableModule,
        ListboxModule,
        ButtonModule,
        DropdownModule,
        DialogModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [DivisionHierarchyComponent, DivisionsComponent, OrganizationUnitsComponent, CategoriesComponent]
})
export class DivisionHierarchyModule { }

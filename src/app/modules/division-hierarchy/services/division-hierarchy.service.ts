import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Division, OrganizationUnitShort } from 'src/app/core/api/models';

@Injectable()
export class DivisionHierarchyService {
    getSelectedDivision: Subject<Division> = new Subject<Division>();
    getSelectedOrganizationUnit: Subject<OrganizationUnitShort> = new Subject<OrganizationUnitShort>();

    updateDivisions: Subject<Division> = new Subject<Division>();
    updateOrganizationUnits: Subject<OrganizationUnitShort> = new Subject<OrganizationUnitShort>();
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProjectManagerAssignmentComponent } from './pages/project-manager-assignment/project-manager-assignment.component';
import { ProjectManagerAssignmentResolver } from './pages/project-manager-assignment/project-manager-assignment.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';


import { ListboxModule } from 'primeng/listbox';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from 'src/app/shared/shared.module';

const projectManagerAssignmentRoutes: Routes = [
  {
    path: '',
    component: ProjectManagerAssignmentComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: ProjectManagerAssignmentResolver }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(projectManagerAssignmentRoutes),
    ListboxModule,
    ButtonModule,
    DropdownModule,
    SharedModule
  ],
  declarations: [ProjectManagerAssignmentComponent],
  providers: [ProjectManagerAssignmentResolver]
})
export class ProjectManagerAssignmentModule { }

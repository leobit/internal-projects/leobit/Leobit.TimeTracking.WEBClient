import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService, UserProjectService, ProjectManagerService } from 'src/app/core/api/services';
import { ProjectShort, UserShort } from 'src/app/core/api/models';

export interface ProjectManagerAssignmentPreloaded {
    users: UserShort[];
    projects: ProjectShort[];
}

@Injectable()
export class ProjectManagerAssignmentResolver  {
    constructor(private userService: UserService, private projectManagerService: ProjectManagerService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProjectManagerAssignmentPreloaded> {
        return forkJoin([
            this.userService.getActiveUsers(),
            this.projectManagerService.getProjectManagerProjects()
        ]).pipe(map(([users, projects]) => {
            return {
                users: users,
                projects: projects
            };
        }));
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SelectItem, MessageService } from 'primeng/api';
import { ProjectManagerService } from 'src/app/core/api/services';
import { UserShort, ProjectShort, UserProject } from 'src/app/core/api/models';
import { ProjectManagerAssignmentPreloaded } from './project-manager-assignment.resolver';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { Listbox } from 'primeng/listbox';
import { Dropdown } from 'primeng/dropdown';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';

type Mode = "UserProject" | "ProjectUser";

@Component({
  selector: 'app-project-manager-assignment',
  templateUrl: './project-manager-assignment.component.html',
  styleUrls: ['./project-manager-assignment.component.css']
})
export class ProjectManagerAssignmentComponent implements OnInit {
  users: UserShort[];
  projects: ProjectShort[];

  mode: Mode = "ProjectUser";

  leftItems: SelectItem[];
  rightItems: SelectItem[];
  selectedLeftItem: any;

  dropdownItems: SelectItem[];
  selectedDropdownItem: any;

  @ViewChild('listboxLeft') listboxLeft: Listbox;
  @ViewChild('listboxRight') listboxRight: Listbox;
  @ViewChild('dropdownRight') dropdownRight: Dropdown;

  constructor(
    private route: ActivatedRoute,
    private projectManagerService: ProjectManagerService,
    private messageService: MessageService,
    private tabPermissionsService: TabPermissionsService) { }

  ngOnInit() {
    var preloaded = <ProjectManagerAssignmentPreloaded>this.route.snapshot.data.preloaded;

    this.users = preloaded.users;
    this.projects = preloaded.projects;

    this.updatePanels();
  }

  get hasWritePermission() {
    return this.tabPermissionsService.hasWriteClaim("ProjectAssignment");
  }

  changeMode() {
    if (this.mode == "UserProject") {
      this.mode = "ProjectUser";
    } else {
      this.mode = "UserProject";
    }

    this.dropdownRight.writeValue(null);
    this.updatePanels();
    this.listboxLeft.resetFilter();
    this.listboxRight.resetFilter();
  }

  updatePanels() {
    this.dropdownItems = null;
    this.selectedDropdownItem = null;
    this.rightItems = null;
    this.selectedLeftItem = null;

    if (this.mode == "UserProject") {
      this.leftItems = this.users
        .map<SelectItem>(u => ({
          value: u,
          label: userToStringWithMail(u)
        }))
        .sortBy(i => i.label);
    } else {
      this.leftItems = this.projects
        .map<SelectItem>(p => ({
          value: p,
          label: this.projectToString(p)
        }))
        .sortBy(i => i.label);
    }
  }

  onLeftItemChange(value: any) {
    if (!!value && value !== this.selectedLeftItem) {
      this.selectedLeftItem = value;

      if (this.mode == "UserProject") {
        this.projectManagerService.getProjectManagers({ userIds: [ this.selectedLeftItem.id ]}).subscribe(up => {
          this.rightItems = up
            .map<SelectItem>(up => ({
              value: up,
              label: this.projectToString(up.project),
              disabled: true
            }))
            .sortBy(i => i.label);

          var upProjectIds = up.map(up => up.project.id);
          this.dropdownItems = this.projects
            .filter(p => upProjectIds.indexOf(p.id) == - 1)
            .map<SelectItem>(p => ({
              value: p,
              label: this.projectToString(p)
            }))
            .sortBy(i => i.label);
        });
      } else {
        this.projectManagerService.getProjectManagers({ projectIds: [ this.selectedLeftItem.id ]}).subscribe(up => {
          this.rightItems = up
            .map<SelectItem>(up => ({
              value: up,
              label: userToStringWithMail(up.user),
              disabled: true
            }))
            .sortBy(i => i.label);

          var upUserIds = up.map(up => up.user.id);
          this.dropdownItems = this.users
            .filter(u => upUserIds.indexOf(u.id) == - 1)
            .map<SelectItem>(u => ({
              value: u,
              label: userToStringWithMail(u)
            }))
            .sortBy(i => i.label);
        });
      }
    }
  }

  createUserProject() {
    var newUserProject = this.mode == "UserProject" ? {
      user: this.selectedLeftItem,
      project: this.selectedDropdownItem
    } : {
      user: this.selectedDropdownItem,
      project: this.selectedLeftItem
    };

    this.projectManagerService.createProjectManager({ body: newUserProject }).subscribe(userProject => {
      this.messageService.add({
        severity: "success",
        summary: "Done",
        detail: `${userToStringWithMail(userProject.user)} was assigned to ${this.projectToString(userProject.project)}`
      });

      var rigthItem =  {
        value: userProject,
        label: this.mode == "UserProject" ? this.projectToString(userProject.project) : userToStringWithMail(userProject.user)
      };

      this.rightItems = [ ...this.rightItems, rigthItem].sortBy(i => i.label);

      this.dropdownItems = this.dropdownItems
        .filter(i => i.value != this.selectedDropdownItem)
        .sortBy(i => i.label);

        this.selectedDropdownItem = this.dropdownItems.length > 0 ? this.dropdownItems[0].value : null;
    });
  }

  deleteUserProject(userProject: UserProject) {
    this.projectManagerService.deleteProjectManager({ body: userProject }).subscribe(() => {
      this.messageService.add({
        severity: "success",
        summary: "Done",
        detail: `${userToStringWithMail(userProject.user)} was unassigned from ${this.projectToString(userProject.project)}`
      });

      this.rightItems = this.rightItems.filter(i => i.value != userProject);

      var dropdownItem = this.mode == "UserProject" ? {
        value: userProject.project,
        label: this.projectToString(userProject.project)
      } : {
        value: userProject.user,
        label: userToStringWithMail(userProject.user)
      };
      this.dropdownItems = [ ...this.dropdownItems, dropdownItem].sortBy(i => i.label);

      if (!this.selectedDropdownItem) {
        this.selectedDropdownItem = this.dropdownItems[0].value;
      }
    });
  }

  private projectToString(project: ProjectShort) {
    return `${project.account.name} - ${project.name}`
  }
}

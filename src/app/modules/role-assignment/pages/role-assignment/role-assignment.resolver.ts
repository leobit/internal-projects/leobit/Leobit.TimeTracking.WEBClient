import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService, RoleService } from 'src/app/core/api/services';
import { RoleShort, UserShort } from 'src/app/core/api/models';

export interface RoleAssignmentPreloaded {
    users: UserShort[];
    roles: RoleShort[];
}

@Injectable()
export class RoleAssignmentResolver  {
    constructor(private userService: UserService, private roleService: RoleService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<RoleAssignmentPreloaded> {
        return forkJoin([
            this.userService.getActiveUsers(),
            this.roleService.getRoles()
        ]).pipe(map(([users, roles]) => {
            return {
                users: users,
                roles: roles
            };
        }));
    }
}

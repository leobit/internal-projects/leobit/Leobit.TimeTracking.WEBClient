import { Component, OnInit } from '@angular/core';
import { SelectItem, MessageService, ConfirmationService } from 'primeng/api';
import { ActivatedRoute } from '@angular/router';
import { RoleAssignmentPreloaded } from './role-assignment.resolver';
import { UserService, RoleService } from 'src/app/core/api/services';
import { SpecialRole, SpecialRolePermission } from '../../models/special-role';
import { PermissionValue } from '../../models/permission-value.enum';
import { UserShort, RoleShort, Role, RolePermission } from 'src/app/core/api/models';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';

type Mode = 'UserRole' | 'RoleUser';

type SelectRoleItem = SelectItem<RoleShort>;

@Component({
    selector: 'app-role-assignment',
    templateUrl: './role-assignment.component.html',
    styleUrls: ['./role-assignment.component.css']
})
export class RoleAssignmentComponent implements OnInit {
    editDialogVisible = false;

    users: UserShort[];
    roles: RoleShort[];

    mode: Mode = 'RoleUser';

    leftItems: SelectItem[];
    rightItems: SelectItem[];
    selectedLeftItem: any;
    selectedRightItem: any;

    // selectedRole: SelectItem;

    editRole: SpecialRole = {} as SpecialRole;
    deleteRoleModel: RoleShort = {};

    roleDropdownItems: SelectItem[];
    userDropdownItems: SelectItem[];
    selectedRoleDropdownItem: any;
    selectedUserDropdownItem: any;

    fallbackRoleDropdownItems: SelectRoleItem[];
    selectedFallbackRoleDropdownItem: RoleShort;

    isEditMode = false;
    confirmModalVisible = false;
    deleteDialogVisible = false;

    readonly RoleNameMaxLength = 50;

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        private roleService: RoleService,
        private messageService: MessageService,
        private confirmationService: ConfirmationService,
        private tabPermissionsService: TabPermissionsService) { }

    ngOnInit() {
        const preloaded = <RoleAssignmentPreloaded>this.route.snapshot.data.preloaded;
        this.users = preloaded.users;
        this.roles = preloaded.roles;

        this.updatePanels();
        this.setDropdownItems();
    }

    get hasWritePermission() {
        return this.tabPermissionsService.hasWriteClaim("Role");
    }

    prePopulate(){
        if(this.mode === "RoleUser" && this.roles.length > 0){
            let roleToSelect = this.roles[0];
            this.onRowSelectRole(roleToSelect);
            this.selectedLeftItem = this.leftItems.find(li => li.label === roleToSelect.name)?.value;
        }
    }

    changeEditMode(currentRightItem: any) {
        if (!this.selectedRightItem) {
            this.isEditMode = !this.isEditMode;
        } else if (this.selectedRightItem && this.selectedRightItem.id === currentRightItem.id) {
            this.setDefaultEditMode();
            return;
        }
        this.selectedRightItem = currentRightItem;
    }

    changeMode() {
        if (this.mode === 'UserRole') {
            this.mode = 'RoleUser';
        } else {
            this.mode = 'UserRole';
        }

        this.updatePanels();
    }

    updatePanels() {
        this.selectedLeftItem = null;
        this.rightItems = null;
        this.selectedRoleDropdownItem = null;

        if (this.mode === 'UserRole') {
            this.leftItems = this.users
                .map<SelectItem>(user => ({
                    value: user,
                    label: userToStringWithMail(user)
                }))
                .sortBy(i => i.label);
        } else {
            this.leftItems = this.roles
                .map<SelectItem>(role => ({
                    value: role,
                    label: role.name
                }))
                .sortBy(i => i.label);
        }

        this.prePopulate();
    }

    onLeftItemChange() {
        this.setDefaultEditMode();

        if (this.selectedLeftItem) {
            this.updateRightItems();
        } else {
            this.rightItems = null;
            this.selectedRoleDropdownItem = null;
        }
    }

    onRowSelectRole(selectedRole: RoleShort) {
        if (selectedRole !== this.selectedLeftItem) {
            this.selectedLeftItem = selectedRole;
            this.onLeftItemChange();
        }
    }

    cancelEditUserRole() {
        this.setDefaultEditMode();
        if (this.mode === 'UserRole') {
            this.updateRightItemRole();
        } else {
            this.selectedRoleDropdownItem = this.selectedLeftItem;
        }
    }

    onAddRole() {
        this.roleService.getPermissions().subscribe(permissions => {
            const rolePermissions = permissions.map(p => {
                return {
                    value: 0,
                    permission: p,
                    read: false,
                    write: false
                } as SpecialRolePermission;
            });

            this.editRole = {
                id: 0,
                name: 'Role name',
                isCustom: true,
                rolePermission: rolePermissions
            };

            this.showEditDialog();
        });
    }

    onEditRole(selectedRole: RoleShort) {
        this.roleService.getFullRoleById({ id: selectedRole.id })
            .subscribe((role: Role) => {
                this.editRole = {
                    ...role,
                    rolePermission: role.rolePermission.map(
                        (permission: RolePermission) => ({
                            ...permission,
                            read: (permission.value & PermissionValue.read) === PermissionValue.read,
                            write: (permission.value & PermissionValue.write) === PermissionValue.write,
                        }))
                };
                this.showEditDialog();
            });
    }

    onSaveRole() {
        this.updateRolePermissionsValues();

        if (!this.editRole.id) {
            this.roleService.createRole({ body: this.editRole }).subscribe(role => {
                this.messageService.add({ severity: 'success', summary: 'Done', detail: `Role '${role.name}' created` });
                this.roles = [...this.roles, role];
                this.leftItems = [...this.leftItems, { label: role.name, value: role }];
            });
        } else {
            this.roleService.editRole({ body: this.editRole }).subscribe(role => {
                this.messageService.add({ severity: 'success', summary: 'Done', detail: `Role '${role.name}' updated` });

                const roleToEdit = this.roles.find(r => r.id === role.id);
                Object.assign(roleToEdit, role);

                const leftItemRole = this.leftItems.find(i => i.value.id === role.id);
                Object.assign(leftItemRole, { label: role.name, value: role });
            });
        }

        this.setRoleDropdownItems();
        this.hideAllDialog();
    }

    onDeleteRole() {
        if (this.editRole.isSystemRole) {
            return;
        }
        this.deleteRoleModel = { ...this.editRole };

        if (this.editRole.isAssignedToUser) {
            this.hideEditDialog();
            this.showDeleteDialog();

            this.setFallbackRoleDropdownItems();
        } else {
            this.confirmDeleteRole();
        }
    }

    onHideEditModal() {
        this.editDialogVisible = false;
        this.editRole = <SpecialRole>{};
    }

    editUserRole(rightItem: UserShort & RoleShort, addUser: boolean) {
        if ((this.mode === 'UserRole' && rightItem.id === this.selectedRoleDropdownItem.id) ||
            (this.mode === 'RoleUser' && rightItem.roleId === this.selectedRoleDropdownItem.id)) {
            this.setDefaultEditMode();
            return;
        }

        const userShort = this.mode === 'UserRole' ? {
            id: this.selectedLeftItem.id,
            firstName: this.selectedLeftItem.firstName,
            lastName: this.selectedLeftItem.lastName,
            roleId: this.selectedRoleDropdownItem.id
        } as UserShort : {
            id: rightItem.id,
            firstName: rightItem.firstName,
            lastName: rightItem.lastName,
            roleId: this.selectedRoleDropdownItem.id
        } as UserShort;

        this.userService.editUser({ body: userShort }).subscribe(user => {
            this.messageService.add({
                severity: 'success',
                summary: 'Done',
                detail: `Role '${this.selectedRoleDropdownItem.name}' was assigned to ${userToStringWithMail(user)}`
            });

            this.users.find(u => u.id === userShort.id).roleId = this.selectedRoleDropdownItem.id;

            if (this.mode === 'UserRole') {
                this.updateRightItemRole();
            } else {
                if(addUser){
                    this.updateRightItems();
                } else {
                    this.rightItems = this.rightItems.filter(i => i.value.id !== user.id);
                    this.selectedRoleDropdownItem = this.selectedLeftItem;
                    this.setUserDropdownItems();
                }
            }
            this.setDefaultEditMode();
        });
    }

    closeConfirmModal() {
        this.confirmationService.close();
        this.confirmModalVisible = false;
    }

    confirmDeleteRole() {
        this.confirmationService.confirm({
            message: `Are you sure that you want to delete role '${this.deleteRoleModel.name}'?`,
            accept: () => {
                this.deleteRole(this.deleteRoleModel, this.selectedFallbackRoleDropdownItem);
            },
            reject: () => {
                this.confirmModalVisible = false;
            }
        });
        this.confirmModalVisible = true;
    }

    hideEditDialog() {
        this.editDialogVisible = false;
    }

    hideAllDialog() {
        this.hideEditDialog();
        this.deleteDialogVisible = false;
        this.confirmModalVisible = false;
        this.selectedFallbackRoleDropdownItem = null;
    }

    private deleteRole(role: RoleShort, fallbackRole?: RoleShort) {
        this.roleService.deleteRole({ id: role.id, fallbackRoleId: fallbackRole?.id })
            .subscribe(() => {
                this.editRole = <SpecialRole>{};
                this.deleteRoleModel = <SpecialRole>{};
                this.roles = this.roles.filter((r: RoleShort) => r.id !== role.id);

                this.updatePanels();

                this.messageService.add({ severity: 'success', summary: 'Done', detail: `Role '${role.name}' deleted` });
            });

        this.hideAllDialog();
    }

    private updateRolePermissionsValues() {
        this.editRole.rolePermission.forEach(rp => {
            // tslint:disable-next-line: no-bitwise
            rp.value = rp.read ? (rp.value | PermissionValue.read) : (rp.value & ~PermissionValue.read);
            // tslint:disable-next-line: no-bitwise
            rp.value = rp.write ? (rp.value | PermissionValue.write) : (rp.value & ~PermissionValue.write);
        });
    }

    private setDefaultEditMode() {
        this.isEditMode = false;
        this.selectedRightItem = null;
    }

    private setDropdownItems() {
       this.setRoleDropdownItems();
       this.setUserDropdownItems();
    }

    private setRoleDropdownItems(){
        this.roleDropdownItems = this.roles
            .map<SelectItem>(role => ({
                value: role,
                label: role.name
            }))
            .sortBy(i => i.label);
    }

    private setFallbackRoleDropdownItems() {
        this.fallbackRoleDropdownItems = this.roles
            .filter((role: RoleShort) => role.id !== this.deleteRoleModel.id)
            .map<SelectRoleItem>((role: RoleShort) => ({
                value: role,
                label: role.name
            }))
            .sortBy((item: SelectRoleItem) => item.label);
        this.selectedFallbackRoleDropdownItem = null;
    }

    private setUserDropdownItems(){
        this.userDropdownItems = this.users
            .filter(user => user.roleId !== this.selectedLeftItem.id)
            .map<SelectItem>(user => ({
                value: user,
                label: userToStringWithMail(user)
            }))
            .sortBy(i => i.label)
        if(this.userDropdownItems.length > 0){
            this.selectedUserDropdownItem = this.userDropdownItems[0].value;
        }
    }

    private updateRightItems() {
        if (this.mode === 'UserRole') {
            this.updateRightItemRole();
        } else {
            this.userService.getActiveUsers({ roleId: this.selectedLeftItem.id }).subscribe(usersByRole => {
                this.rightItems = usersByRole
                    .map<SelectItem>(user => ({
                        value: user,
                        label: userToStringWithMail(user),
                        disabled: true
                    }))
                    .sortBy(i => i.label);
                this.selectedRoleDropdownItem = this.selectedLeftItem;
                this.setUserDropdownItems();
            });
        }
    }

    private updateRightItemRole() {
        this.userService.getUserRole({ id: this.selectedLeftItem.id }).subscribe(role => {
            this.rightItems = [
                {
                    value: role,
                    label: role.name,
                    disabled: true
                } as SelectItem
            ];
            this.selectedRoleDropdownItem = role;
        });
    }

    private showEditDialog() {
        this.editDialogVisible = true;
    }

    private showDeleteDialog() {
        this.deleteDialogVisible = true;
    }
}

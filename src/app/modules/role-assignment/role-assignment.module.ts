import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { RoleAssignmentComponent } from './pages/role-assignment/role-assignment.component';
import { RoleAssignmentResolver } from './pages/role-assignment/role-assignment.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { InputTextModule } from 'primeng/inputtext';
import { ListboxModule } from 'primeng/listbox';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { CheckboxModule } from 'primeng/checkbox';
import { TooltipModule } from 'primeng/tooltip';
import { MessagesModule } from 'primeng/messages';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SharedModule } from 'src/app/shared/shared.module';

const roleAssignmentRoutes: Routes = [
    {
        path: '',
        component: RoleAssignmentComponent,
        canActivate: [AuthGuard],
        resolve: { preloaded: RoleAssignmentResolver }
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild(roleAssignmentRoutes),
        InputTextModule,
        ListboxModule,
        ButtonModule,
        DialogModule,
        TableModule,
        CheckboxModule,
        TooltipModule,
        ConfirmDialogModule,
        MessagesModule,
        SharedModule
    ],
    declarations: [RoleAssignmentComponent],
    providers: [RoleAssignmentResolver]
})
export class RoleAssignmentModule { }

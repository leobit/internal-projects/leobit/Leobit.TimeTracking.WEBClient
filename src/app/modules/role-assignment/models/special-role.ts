import { Role, RolePermission } from 'src/app/core/api/models';

export type SpecialRolePermission = RolePermission & { read: boolean, write: boolean };

export type SpecialRole = Pick<Role, Exclude<keyof Role, 'rolePermission'>> & {
    rolePermission: SpecialRolePermission[];
};

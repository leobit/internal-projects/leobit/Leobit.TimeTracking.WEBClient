export enum PermissionValue {
    read = 1,
    write = 2
}

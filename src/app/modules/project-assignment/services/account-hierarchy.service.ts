import { Injectable } from "@angular/core";
import { SelectItem } from "primeng/api";
import { Subject, forkJoin, map } from "rxjs";
import { Account, Project, ProjectShort, UserProject, UserShort } from "src/app/core/api/models";
import { MeService, ProjectService, UserProjectService, UserService } from "src/app/core/api/services";
import { userToString } from "src/app/core/helpers/user.helper";

@Injectable()
export class AccountHierarchyService {
  private selectedAccount = new Subject<Account>();
  public selectedAccount$ = this.selectedAccount.asObservable();

  private selectedProject = new Subject<Project>();
  public selectedProject$ = this.selectedProject.asObservable();

  public employees = new Subject<{ assigned: SelectItem<UserProject>[], available: SelectItem<UserShort>[], userProjects: UserProject[] }>();
  public employees$ = this.employees.asObservable();

  constructor(private projectService: ProjectService,private userProjectService: UserProjectService,private userService: UserService, private meService: MeService) {}

  public selectAccount(account: Account): void {
    if (account) {
      this.employees.next({ assigned: [], available: [], userProjects: [] });
      this.selectedAccount.next(account);
      this.loadProjectsForAccount(account);
    } else {
      this.employees.next({ assigned: [], available: [], userProjects: [] });
      this.selectedAccount.next(null);
    }

    this.selectedProject.next(null);
  }

  public selectProject(project: Project): void {
    if (project) {
      this.selectedProject.next(project);
    } else{
      this.selectedProject.next(null);
      this.employees.next({ assigned: [], available: [], userProjects: [] });
    }
  }

  public loadEmployeesForProject(projectId: number): void {
    const userProjects$ = this.userProjectService.getUserProjects({ projectIds: [projectId] });
    const allUsers$ = this.meService.getUsersForAssignment();

    forkJoin([userProjects$, allUsers$]).pipe(
        map(([userProjects, allUsers]) => {
            const assignedUsers = userProjects.map<SelectItem<UserProject>>(up => ({
                value: up,
                label: userToString(up.user),
                disabled: true
            })).sort((a, b) => a.label.localeCompare(b.label));

            const upUserIds = userProjects.map(up => up.user.id);

            const availableUsers = allUsers
                .filter(u => !upUserIds.includes(u.id))
                .map<SelectItem<UserShort>>(u => ({
                    value: u,
                    label: userToString(u)
                })).sort((a, b) => a.label.localeCompare(b.label));

            return { assigned: assignedUsers, available: availableUsers, userProjects: userProjects};
        })
    ).subscribe(employees => {
        this.employees.next(employees);
    });
}


  private loadProjectsForAccount(account: Account): void {
      this.projectService.getProjects({ accountIds: [account.id] }).pipe(
          map(projects => projects.length > 0 ? projects : [])
      );
  }
}


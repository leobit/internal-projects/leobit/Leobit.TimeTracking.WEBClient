export class UserWithName{
  firstName?: null | string;
  id?: null | string;
  lastName?: null | string;
  roleId?: number;
  name: string;
}

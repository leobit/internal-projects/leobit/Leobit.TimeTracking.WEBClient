import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Account } from 'src/app/core/api/models/account';
import { AccountService } from 'src/app/core/api/services';
import { AccountHierarchyService } from '../../services/account-hierarchy.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-project-assignment-accounts',
  templateUrl: './project-assignment-accounts.component.html',
  styleUrl: './project-assignment-accounts.component.css'
})
export class ProjectAssignmentAccountsComponent implements OnInit, AfterViewInit {
  @ViewChild('dt') dataTable!: Table;

  accounts: Account[];
  selectedAccount: Account;
  showClosedProjects = false;

  constructor(private accountservice: AccountService,
    private accountHierarchyService: AccountHierarchyService) { }


  ngOnInit(): void {
     this.loadAccounts();
  }

  ngAfterViewInit() {
    this.dataTable.filter('true', 'isActive', 'equals');
  }

  sendToAccounts(account: Account) {
      this.accountHierarchyService.selectAccount(account);
      this.selectedAccount = account;
  }

  getEventValue($event: any): string {
    return $event.target.value;
  }

  handleClosedAccountsVisibility() {
    this.dataTable.filter(this.showClosedProjects ? '' : 'true','isActive','equals');
    this.accountHierarchyService.selectAccount(null);
    this.selectedAccount = null;
  }

  private loadAccounts() {
      this.accountservice.getAccounts().subscribe(accounts => {
          this.accounts = accounts.sortBy(d => d.name);
      });
  }
}

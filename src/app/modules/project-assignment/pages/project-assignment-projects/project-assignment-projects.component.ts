import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Account, Project } from 'src/app/core/api/models';
import { AccountHierarchyService } from '../../services/account-hierarchy.service';
import { ProjectService } from 'src/app/core/api/services';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-project-assignment-projects',
  templateUrl: './project-assignment-projects.component.html',
  styleUrl: './project-assignment-projects.component.css'
})
export class ProjectAssignmentProjectsComponent implements OnInit, AfterViewInit {
  @ViewChild('dt') dataTable!: Table;

  projects: Project[] = [];
  selectedProject: Project | null = null;
  showClosedProjects = false;

  constructor(
    private projectService: ProjectService,
    private accountHierarchyService: AccountHierarchyService
  ) { }

  ngOnInit(): void {
    this.accountHierarchyService.selectedAccount$.subscribe(account => {
      if (account) {
        this.loadProjects(account);
      } else{
        this.projects = [];
      }
    });

    this.accountHierarchyService.selectedProject$.subscribe(project => {
      this.selectedProject = project;
    });
  }

  ngAfterViewInit() {
    this.dataTable.filter('false', 'isClosed', 'equals');
  }

  loadProjects(account: Account): void {
    this.projectService.getProjects({ accountIds: [account.id] }).subscribe(projects => {
      this.projects = projects;
      this.selectedProject = null;
    });
  }

  setSelectedProject(project: Project): void {
    this.accountHierarchyService.selectProject(project);
  }

  getEventValue($event: any): string {
    return $event.target.value;
  }

  handleClosedProjectsVisibility() {
    this.dataTable.filter(this.showClosedProjects ? '' : 'false','isClosed','equals');
    this.accountHierarchyService.selectProject(null);
  }
}

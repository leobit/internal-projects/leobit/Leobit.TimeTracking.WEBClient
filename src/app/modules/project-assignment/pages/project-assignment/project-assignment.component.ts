import { Component, OnInit } from '@angular/core';
import { AccountHierarchyService } from '../../services/account-hierarchy.service';


@Component({
  selector: 'app-project-assignment',
  templateUrl: './project-assignment.component.html',
  styleUrls: ['./project-assignment.component.css'],
  providers: [AccountHierarchyService]
})
export class ProjectAssignmentComponent implements OnInit {
  ngOnInit(): void {
  }
}

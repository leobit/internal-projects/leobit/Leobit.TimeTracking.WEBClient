import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService, UserProjectService } from 'src/app/core/api/services';
import { ProjectShort, UserShort } from 'src/app/core/api/models';

export interface ProjectAssignmentPreloaded {
    users: UserShort[];
    projects: ProjectShort[];
}

@Injectable()
export class ProjectAssignmentResolver  {
    constructor(private userService: UserService, private userProjectService: UserProjectService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProjectAssignmentPreloaded> {
        return forkJoin([
            this.userService.getActiveUsers(),
            this.userProjectService.getShortProjects()
        ]).pipe(map(([users, projects]) => {
            return {
                users: users,
                projects: projects
            };
        }));
    }
}
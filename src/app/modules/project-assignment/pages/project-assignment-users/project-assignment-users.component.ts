import { Component, OnInit } from '@angular/core';
import { ProjectShort, UserProject, UserShort } from 'src/app/core/api/models';
import { UserProjectService } from 'src/app/core/api/services';
import { AccountHierarchyService } from '../../services/account-hierarchy.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MessageService, SelectItem } from 'primeng/api';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';
import { UserWithName } from '../../models/UserWithName';
import { DateRangeValidator } from 'src/app/shared/validators/date-range-validator';
import { DatePipe } from '@angular/common';
import { sanitizeFormInput } from 'src/app/core/helpers/reactive-forms.helper';

@Component({
  selector: 'app-project-assignment-users',
  templateUrl: './project-assignment-users.component.html',
  styleUrl: './project-assignment-users.component.css',
  providers: [DatePipe]
})
export class ProjectAssignmentUsersComponent implements OnInit {
  editDialogVisible = false;
  addDialogVisible = false;
  users: UserWithName[];
  newUser: UserShort;
  currentProject: ProjectShort;
  currentEditedUser: any;
  selectedUser: UserWithName;
  addEmployeeForm: FormGroup;
  editEmployeeForm: FormGroup;

  isUnlimited = true;

  minDate = new Date('01-01-2014');

  assignedUsers: SelectItem<UserProject>[];
  availableUsers: SelectItem<UserShort>[];
  userProjects: UserProject[];

  editUserProject: UserProject;

  constructor(
    private fb: FormBuilder,
    private userProjectService: UserProjectService,
    private messageService: MessageService,
    private accountHierarchyService: AccountHierarchyService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.addEmployeeForm = new FormGroup(
      {
        newUser: new FormControl(null, [Validators.required]),
        startDate: new FormControl(null),
        endDate: new FormControl(null),
        isUnlimited: new FormControl(true),
        hours: new FormControl(null, [
          Validators.pattern("^[0-9]*$"),
          Validators.min(0),
          Validators.max(5000)]
        )
      },
      { validators: DateRangeValidator }
    );

    this.editEmployeeForm = new FormGroup(
      {
        startDate: new FormControl(null),
        endDate: new FormControl(null),
        isUnlimited: new FormControl(true),
        hours: new FormControl(null, [
          Validators.pattern("^[0-9]*$"),
          Validators.min(0),
          Validators.max(5000)])
      },
      { validators: DateRangeValidator }
    );

    this.loadData();
  }

  loadData() {
    this.accountHierarchyService.employees$.subscribe(employees => {
      this.assignedUsers = employees.assigned;
      this.availableUsers = employees.available;
      this.userProjects = employees.userProjects;
      this.users = this.assignedUsers.map(item => {
        const user = item.value.user;
        return {
          ...user,
          name: `${user.firstName} ${user.lastName}`
        };
      });
    });

    this.accountHierarchyService.selectedProject$.subscribe(project => {
      this.currentProject = project;
      if (project?.id) {
        this.accountHierarchyService.loadEmployeesForProject(project.id);
      }
    });
  }

  displayModal(user: any) {
    const userProject = this.userProjects?.find(up => up.user && up.user.id === user.id);
    if (userProject) {
      this.currentEditedUser = userProject.user;
      this.editUserProject = userProject;

      this.editEmployeeForm.patchValue({
        startDate: userProject.startDate ? this.formatDate(userProject.startDate, 'MM/dd/YYYY').toString() : null,
        endDate: userProject.endDate ? this.formatDate(userProject.endDate, 'MM/dd/YYYY').toString() : null,
        isUnlimited: userProject.hours === null || userProject.hours === undefined,
        hours: userProject.hours
      });

      this.editDialogVisible = true;
    }
  }

  onAddUser() {
    this.addEmployeeForm.reset({ isUnlimited: true });
    this.addEmployeeForm.markAsPristine();

    this.addDialogVisible = true;
  }

  addUser() {
    if (this.addEmployeeForm.invalid) {
      this.messageService.add({ severity: "error", summary: "Error", detail: "Please fill in all required fields" })
      return;
    }

    this.addDialogVisible = false;
    const formValues = this.addEmployeeForm.value;

    var newUserProject = {
      user: formValues.newUser,
      project: this.currentProject,
      hours: formValues.isUnlimited ? null : formValues.hours,
      startDate: this.formatDate(formValues.startDate, 'yyyy-MM-ddTHH:mm:ss'),
      endDate: this.formatDate(formValues.endDate, 'yyyy-MM-ddTHH:mm:ss')
    };

    this.userProjectService.createUserProject({ body: newUserProject }).subscribe(userProject => {
      this.assignedUsers.push({
        value: userProject,
        label: `${userProject.user.firstName} ${userProject.user.lastName}`,
        disabled: true
      } as SelectItem<UserProject>);
      this.messageService.add({
        severity: "success",
        summary: "Done",
        detail: `${userToStringWithMail(userProject.user)} was assigned to ${this.projectToString(userProject.project)}`
      });
    });

    this.users.push({ ...this.newUser, name: `${this.newUser.firstName} ${this.newUser.lastName}` });
    this.users.sort((a, b) => a.name.localeCompare(b.name))
    this.userProjects.push(newUserProject);
    this.availableUsers = this.availableUsers.filter(x => x.value.id != this.newUser.id);
    this.newUser = null;

    this.addEmployeeForm.reset({
      newUser: null,
      startDate: null,
      endDate: null,
      isUnlimited: false,
      hours: null
    });
  }

  onRemoveUser(user) {
    let userProject = this.assignedUsers.find(i => i.value.user.id == user.id).value;
    this.userProjectService.deleteUserProject({ body: userProject }).subscribe(() => {
      this.messageService.add({
        severity: "success",
        summary: "Done",
        detail: `${userToStringWithMail(userProject.user)} was unassigned from ${this.projectToString(userProject.project)}`
      });
      this.users = this.users.filter(u => u.id != user.id);

      if (!this.availableUsers.includes(user)) {
        this.availableUsers.push({ value: user, label: user.name })
        this.availableUsers.sort((a, b) => a.label.localeCompare(b.label));
      }
    });
  }

  onSaveEmployee() {
    const formValues = this.editEmployeeForm.value;

    const updateUserProject = {
      user: this.currentEditedUser,
      project: this.currentProject,
      hours: formValues.isUnlimited ? null : formValues.hours,
      startDate: this.formatDate(formValues.startDate, 'yyyy-MM-ddTHH:mm:ss'),
      endDate: this.formatDate(formValues.endDate, 'yyyy-MM-ddTHH:mm:ss')
    };

    this.userProjectService.updateUserProject({ body: updateUserProject }).subscribe(userProject => {
      this.messageService.add({
        severity: "success",
        summary: "Done",
        detail: `${userToStringWithMail(userProject.user)} was updated for ${this.projectToString(userProject.project)}`
      });
      this.userProjects = [
        ...this.userProjects.filter(up => up.user && up.user.id != updateUserProject.user.id),
        updateUserProject
      ];

      this.editDialogVisible = false;
    });
  }

  formatDate(dateString: string, dateFormat: 'MM/dd/YYYY' | 'yyyy-MM-ddTHH:mm:ss'): string {
    if (!dateString) {
      return null;
    }
    const date = new Date(dateString);
    return this.datePipe.transform(date, dateFormat);
  }

  projectToString(project: ProjectShort) {

    return `${project.account.name} - ${project.name}`;
  }

  getEventValue($event: any): string {
    return $event.target.value;
  }

  sanitizeEditNumericInput(event: Event): void {
    sanitizeFormInput(event, 'hours', this.editEmployeeForm, /[.,-]+/g);
  }

  sanitizeAddNumericInput(event: Event): void {
    sanitizeFormInput(event, 'hours', this.addEmployeeForm, /[.,-]+/g);
  }
}
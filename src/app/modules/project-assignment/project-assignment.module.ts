import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { ProjectAssignmentComponent } from './pages/project-assignment/project-assignment.component';
import { ProjectAssignmentResolver } from './pages/project-assignment/project-assignment.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';


import { ListboxModule } from 'primeng/listbox';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { TagModule } from 'primeng/tag';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProjectAssignmentAccountsComponent } from "./pages/project-assignment-accounts/project-assignment-accounts.component";
import { ProjectAssignmentProjectsComponent } from "./pages/project-assignment-projects/project-assignment-projects.component";
import { ProjectAssignmentUsersComponent } from "./pages/project-assignment-users/project-assignment-users.component";
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';

const projectAssignmentRoutes: Routes = [
  {
    path: '',
    component: ProjectAssignmentComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: ProjectAssignmentResolver }
  }
];

@NgModule({
    declarations: [ProjectAssignmentComponent,
      ProjectAssignmentAccountsComponent,
      ProjectAssignmentProjectsComponent,
      ProjectAssignmentUsersComponent],
    providers: [ProjectAssignmentResolver],
    imports: [
      CommonModule,
      FormsModule,
      RouterModule.forChild(projectAssignmentRoutes),
      ListboxModule,
      ButtonModule,
      DropdownModule,
      SharedModule,
      TagModule,
      TableModule,
      DialogModule,
      ReactiveFormsModule,
      InputSwitchModule,
      CalendarModule,
      CheckboxModule,
    ]
})
export class ProjectAssignmentModule { }

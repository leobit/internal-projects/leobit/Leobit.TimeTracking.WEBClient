import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './pages/settings/settings.component';
import { SettingsResolver } from './pages/settings/settings.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { SliderModule } from 'primeng/slider';
import { FieldsetModule } from 'primeng/fieldset';
import { InputNumberModule } from 'primeng/inputnumber';
import { SharedModule } from 'src/app/shared/shared.module';


const settingsRoutes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: SettingsResolver }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(settingsRoutes),
    InputNumberModule,
    ButtonModule,
    DropdownModule,
    SliderModule,
    FieldsetModule,
    SharedModule,
  ],
  declarations: [SettingsComponent],
  providers: [SettingsResolver]
})
export class SettingsModule { }

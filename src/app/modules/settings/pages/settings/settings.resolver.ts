import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { SettingsService } from 'src/app/core/api/services';
import { Settings } from 'src/app/core/api/models';

@Injectable()
export class SettingsResolver  {
    constructor(private settingsService: SettingsService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Settings> {
        return this.settingsService.getSettings();
    }
}

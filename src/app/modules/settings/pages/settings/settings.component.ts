import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Slider } from 'primeng/slider';
import { SelectItem, MessageService } from 'primeng/api';
import { Settings, ClosingType, UserShort } from 'src/app/core/api/models';
import { AccountService, SettingsService } from 'src/app/core/api/services';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { Globals } from 'src/app/core/globals';
import { ClosingTypeService } from 'src/app/core/services/closing-type-service';
import { firstValueFrom } from 'rxjs';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild("internalSlider", { static: true })
  internalSlider: Slider;

  @ViewChild("externalSlider", { static: true })
  externalSlider: Slider;

  closingTypes: SelectItem[] ;
  budgetOwnerSelectItems: SelectItem<string>[] = [];

  closingTypesEnum = ClosingType;

  settings: Settings;

  constructor(
    private route: ActivatedRoute,
    private settingsService: SettingsService,
    private messageService: MessageService,
    private globals: Globals,
    private tabPermissionsService: TabPermissionsService,
    public closingTypeService: ClosingTypeService,
    private accountService: AccountService) { }

  ngOnInit() {
    this.settings = <Settings>this.route.snapshot.data.preloaded;

    this.closingTypes = this.closingTypeService.getEnabledTypes(this.settings.closingType);

    this.loadBudgetOwners();
  }

  async loadBudgetOwners(){
    const eligibleBudgetOwnersInfo = await firstValueFrom(
      this.accountService.getEligibleBudgetOwners()
    );

    this.budgetOwnerSelectItems = eligibleBudgetOwnersInfo.eligibleBudgetOwners.map<SelectItem<string>>(
      (user: UserShort) => ({
        label: userToStringWithMail(user),
        value: user.id,
      }))
        .sortBy((item: SelectItem<string>) => item.label);
  }

  supportsClosingType(closingType) { return this.closingTypeService.supportsWindowExtension(closingType); }

  get hasWritePermission() {
    return this.tabPermissionsService.hasWriteClaim("Settings");
  }

  onSave() {
    this.settingsService.editSettings({ body: this.settings }).subscribe(settings => {
      this.messageService.add({ severity: "success", summary: "Done", detail: "Settings saved" });
      this.settings = settings;
    });
  }

  onInternalHoursChange(values: number[]) {
    this.settings.minInternal = values[0] / 2;
    this.settings.maxInternal = values[1] / 2;
  }

  onExternalHoursChange(values: number[]) {
    this.settings.minExternal = values[0] / 2;
    this.settings.maxExternal = values[1] / 2;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { SharedModule } from './../../shared/shared.module';

import { ReportingRoutingModule } from './reporting-routing.module';

import { OptionsComponent } from './components/options/options.component';
import { TimeReportComponent } from './components/time-report/time-report.component';

import { DailyComponent } from './pages/daily/daily.component';
import { DailyResolver } from './pages/daily/daily.resolver';

import { StatisticComponent } from './pages/statistic/statistic.component';
import { StatisticResolver } from './pages/statistic/statistic.resolver';

import { MonthlyComponent } from './pages/monthly/monthly.component';
import { MonthlyResolver } from './pages/monthly/monthly.resolver';

import { VacationAndSickLeaveComponent } from './pages/vacation-and-sick-leave/vacation-and-sick-leave.component';
import { VacationAndSickLeaveResolver } from './pages/vacation-and-sick-leave/vacation-and-sick-leave.resolver';

import { RadioButtonModule } from 'primeng/radiobutton';
import { InputSwitchModule } from 'primeng/inputswitch';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { TreeModule } from 'primeng/tree';
import { TableModule } from 'primeng/table';
import { SelectButtonModule } from 'primeng/selectbutton';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { InputTextModule } from 'primeng/inputtext';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { TimeReportsTableComponent } from './components/time-reports-table/time-reports-table.component';
import { TimeReportsTablePreviewComponent } from './components/time-reports-table-preview/time-reports-table-preview.component';
import { TooltipModule } from 'primeng/tooltip';
import { StringJoinPipe } from './pipes/string-join.pipe';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReportingRoutingModule,
    SharedModule,
    RadioButtonModule,
    InputSwitchModule,
    ButtonModule,
    CalendarModule,
    ButtonModule,
    DropdownModule,
    InputTextareaModule,
    TreeModule,
    TableModule,
    SelectButtonModule,
    DialogModule,
    ConfirmDialogModule,
    OverlayPanelModule,
    InputTextModule,
    TooltipModule,
    StringJoinPipe
  ],
  declarations: [
    // components
    OptionsComponent, TimeReportComponent, TimeReportsTableComponent,
    // pages
    DailyComponent, StatisticComponent, MonthlyComponent, VacationAndSickLeaveComponent, TimeReportsTablePreviewComponent
  ],
  providers: [ DailyResolver, StatisticResolver, MonthlyResolver, VacationAndSickLeaveResolver ]
})
export class ReportingModule { }

import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'join',
  standalone: true,
  pure: true
})
export class StringJoinPipe implements PipeTransform {
  transform(arr: string[], useSerialComma = false): string {
    if (!arr || arr.length === 0) {
      return '';
    }else if (arr.length === 1) {
      return arr[0];
    }
    return arr.reduce(
      (previous: string, current: string, i: number, array: string[]) => {
        let str = previous;
        if (i >= array.length - 1) {
          str += array.length > 2 && useSerialComma ? ', and' : ' and';
        }
        return `${str} ${current}`;
      },
      ''
    );
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TreeNode } from 'primeng/api';
import { MeService, OperationalService } from 'src/app/core/api/services';
import { UserTimeReport, MonthHours, Holiday, ProjectShort, UserDefaults } from 'src/app/core/api/models';
import { MonthlyPreloaded } from './monthly.resolver';
import { Dialog } from 'primeng/dialog';
import { ProjectTreeService } from 'src/app/core/services/project-tree.service';
import { OverlayPanel } from 'primeng/overlaypanel';
import { isTimeReportEditable } from 'src/app/core/helpers/UserTimeReport.helper';
import { ExtendedUserTimeReport } from '../../types/extended-user-time-report';

interface DayModel {
  date?: Date,
  internalHours?: number,
  externalHours?: number;
}

@Component({
  selector: 'app-monthly',
  templateUrl: './monthly.component.html',
  styleUrls: ['./monthly.component.css']
})
export class MonthlyComponent implements OnInit {
  @ViewChild('op', { static: true }) op: OverlayPanel;

  userDefaults: UserDefaults;
  monthBaseLines: MonthHours[] = [];
  holidays: Holiday[] = [];
  selectedTimeReports: UserTimeReport[] = [];
  startTime: string;
  reportingDate: Date = new Date();
  detailsDialog: Dialog;

  filterProjects: TreeNode[];
  selectedProjects: TreeNode | TreeNode[];
  timeReports: ExtendedUserTimeReport[];
  projects: ProjectShort[];
  activeProjects: ProjectShort[];

  currentMonth: Date;

  firstDate: Date;
  lastDate: Date;

  months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  columns: string[] = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Total'];
  readonly totalColumnIntex = 7;

  weekRows: any[];

  totalInternal: number;
  totalExternal: number;
  currentMonthBaseLine: number;

  isDetailsDialogVisible: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private operationalService: OperationalService,
    private meService: MeService,
    private projectTreeService: ProjectTreeService,
  ) { }

  ngOnInit() {
    const preloaded = <MonthlyPreloaded>this.route.snapshot.data.preloaded;

    this.userDefaults = preloaded.userDefaults;
    this.currentMonth = preloaded.currentMonth;
    this.monthBaseLines = preloaded.baseLines;
    this.setCurrentMonthBaseLine(this.currentMonth.getFullYear(), this.currentMonth.getMonth());
    this.setCurrentMonthHolidays(this.currentMonth.getFullYear(), this.currentMonth.getMonth())
    this.projects = preloaded.projects;

    this.filterProjects = [{
      label: 'All',
      expanded: true,
      children: preloaded.projects
        .groupBy(
          (p) => p.account,
          (a1, a2) => a1.id === a2.id
        )
        .map<TreeNode>((accountItem) => ({
          label: accountItem.key.name,
          expanded: true,
          children: accountItem.value.map<TreeNode>((projectItem) => ({
            label: projectItem.name,
            data: projectItem.id,
          })),
        }))
    }];
    
    this.activeProjects = this.filterActiveProject()

    this.selectedProjects = this.filterProjects.reduce(
      (acumulator, value) => {
        return acumulator.concat(value).concat(value.children);
      },
      []
    );
    this.selectedProjects = this.selectedProjects.concat(this.selectedProjects.map(
      item => item.children.filter(
        child => child.data))
      .reduce((acumulator, currentValue) => acumulator.concat(currentValue), []))

    this.timeReports = preloaded.userTimeReports.map((report: UserTimeReport) => ({
      ...report,
      isEditable: report.isEditable
    }));
    this.calculateHours(preloaded.userTimeReports);
  }

  getTimeReportDetails(date: Date, event) {
    this.reportingDate = Date.ParseISOString(Date.CreateUTCDate(date).toISOString());
    this.selectedTimeReports = this.timeReports.filter(timeReport => {
      return this.reportingDate.getDate() === Date.ParseISOString(timeReport.reportingDate).getDate();
    });

    this.isDetailsDialogVisible = true;
    this.op.toggle(event)
  }

  onEditTimeReport(report: UserTimeReport) {
    const editedReport = this.timeReports.find(x => x.id === report.id)
    if (editedReport) {
      const i = this.timeReports.indexOf(editedReport);
      this.timeReports[i] = { ...report, account: report.project.account };
    }
    this.calculateHours(this.timeReports)
  }

  onDeleteTimeReport(report: UserTimeReport) {
    this.timeReports = this.timeReports.filter(
      (userTimeReport) => userTimeReport.id != report.id);
    this.calculateHours(this.timeReports)
  }

  onAddTimeReport(report: UserTimeReport) {
    this.timeReports = this.timeReports.concat({ ...report, account: report.project.account, isEditable: isTimeReportEditable(report) })
    this.calculateHours(this.timeReports)
  }

  previousMonthClick() {
    this.currentMonth.setMonth(this.currentMonth.getMonth() - 1);
    this.setCurrentMonthBaseLine(this.currentMonth.getFullYear(), this.currentMonth.getMonth());
    this.setCurrentMonthHolidays(this.currentMonth.getFullYear(), this.currentMonth.getMonth());
    this.loadFilter();
  }

  nextMonthClick() {
    this.currentMonth.setMonth(this.currentMonth.getMonth() + 1);
    this.setCurrentMonthBaseLine(this.currentMonth.getFullYear(), this.currentMonth.getMonth());
    this.setCurrentMonthHolidays(this.currentMonth.getFullYear(), this.currentMonth.getMonth());
    this.loadFilter();
  }

  apply() {
    this.loadUserTimeReportsAndCalculateHours();
  }

  setCurrentMonthBaseLine(year: number, month: number) {
    if (this.monthBaseLines.some(item => item.year === year && item.month === month + 1)) {
      this.currentMonthBaseLine = this.monthBaseLines.find(item => item.year === year && item.month === month + 1).hours;
    }
    else {
      this.operationalService.getBaseLineByYearANdMonth({ year: year, month: month + 1 }).subscribe(monthBaseLines => {
        const monthBaseLine = monthBaseLines.find(item => item.year === year && item.month === month + 1);
        this.monthBaseLines.push(monthBaseLine);
        this.currentMonthBaseLine = monthBaseLine.hours;
      });
    }
  }

  setCurrentMonthHolidays(year: number, month: number) {
    this.operationalService.getHolidaysByYearAndMonth({ year, month: month + 1 })
      .subscribe(holidays => {
        this.holidays = holidays
      })
  }

  loadUserTimeReportsAndCalculateHours() {
    const year = this.currentMonth.getFullYear(), month = this.currentMonth.getMonth();
    this.firstDate = new Date(year, month, 1);
    this.lastDate = new Date(year, month + 1, 0);

    this.projectTreeService.getTimeReports(this.filterProjects,
      this.selectedProjects as TreeNode[],
      this.projects,
      this.firstDate,
      this.lastDate,
      null)
      .subscribe(reports => {
        this.timeReports = reports.map((report: UserTimeReport) => ({
          ...report,
          isEditable: report.isEditable
        }));
        this.calculateHours(this.timeReports);
      });
  }

  isTableCellDisabled(date: Date | undefined) {
    if (!date) {
      return false;
    }

    const weekend = [0, 6]
    const holidayDays = this.holidays.map(holiday => new Date(holiday.date).getDate())

    return weekend.includes(date.getDay()) || holidayDays.includes(date.getDate())
  }

  calculateHours(userTimeReports: UserTimeReport[]) {
    // calendar calculation
    var weeks: any[] = [{}];
    var currentWeekIndex = 0;
    var dateOfMonthToWeekMap: { [date: number]: any } = {};
    var totalInternal = 0;
    var totalExternal = 0;

    var year = this.currentMonth.getFullYear(), month = this.currentMonth.getMonth();
    var lastDateOfMonth = new Date(year, month + 1, 0);

    var lastDayOfMonth = lastDateOfMonth.getDate();
    for (var dayOfMonth = 1; dayOfMonth <= lastDayOfMonth; dayOfMonth++) {
      var date = new Date(year, month, dayOfMonth);
      var dayOfWeek = date.getDayOfWeek();

      var dayModel: DayModel = {
        date: date
      };

      var currentWeek = weeks[currentWeekIndex];
      dateOfMonthToWeekMap[dayOfMonth] = currentWeek;
      currentWeek[this.columns[dayOfWeek]] = dayModel;

      if (dayOfWeek === 6) {
        weeks.push({});
        currentWeekIndex += 1;
      }
    }

    // hours calculation
    for (let userTimeReport of userTimeReports) {
      var reportingDate = Date.ParseISOString(userTimeReport.reportingDate);
      var week = dateOfMonthToWeekMap[reportingDate.getDate()];
      dayModel = week[this.columns[reportingDate.getDayOfWeek()]];

      if (dayModel.internalHours !== undefined) {
        dayModel.internalHours += userTimeReport.internalHours;
        dayModel.externalHours += userTimeReport.externalHours;
      } else {
        dayModel.internalHours = userTimeReport.internalHours;
        dayModel.externalHours = userTimeReport.externalHours;
      }

      var totalModel = week[this.columns[this.totalColumnIntex]];
      if (totalModel) {
        totalModel.internalHours += userTimeReport.internalHours;
        totalModel.externalHours += userTimeReport.externalHours;
      } else {
        week[this.columns[this.totalColumnIntex]] = {
          internalHours: userTimeReport.internalHours,
          externalHours: userTimeReport.externalHours
        };
      }

      totalInternal += userTimeReport.internalHours;
      totalExternal += userTimeReport.externalHours;
    }

    this.totalInternal = totalInternal;
    this.totalExternal = totalExternal;
    this.weekRows = weeks;
  }

  private filterActiveProject(): ProjectShort[] {
    return this.projects.filter(
      (p: ProjectShort) => !p.isClosed
    );
  }

  private loadFilter() {
    const year = this.currentMonth.getFullYear();
    const month = this.currentMonth.getMonth()
    const firstDateOfMonth = new Date(year, month, 1);
    const lastDateOfMonth = new Date(year, month + 1, 0);
    this.meService.getFilterProjects({
      dateFrom: Date.CreateUTCDate(firstDateOfMonth).toISOString(),
      dateTo: Date.CreateUTCDate(lastDateOfMonth).toISOString()
    }).subscribe((projects) => {
      this.filterProjects = [{
        label: 'All',
        expanded: true,
        children: projects
          .groupBy(
            (p) => p.account,
            (a1, a2) => a1.id === a2.id
          )
          .map<TreeNode>((accountItem) => ({
            label: accountItem.key.name,
            expanded: true,
            children: accountItem.value.map<TreeNode>((projectItem) => ({
              label: projectItem.name,
              data: projectItem.id,
            })),
          }))
      }];

      this.selectedProjects = this.filterProjects.reduce(
        (acumulator, value) => {
          return acumulator.concat(value).concat(value.children);
        },
        []
      );

      this.selectedProjects = this.selectedProjects.concat(this.selectedProjects.map(
        item => item.children.filter(
          child => child.data))
        .reduce((acumulator, currentValue) => acumulator.concat(currentValue), []));

      this.loadUserTimeReportsAndCalculateHours();
    });
  }
}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { MeService, OperationalService } from 'src/app/core/api/services';
import { UserTimeReport, ProjectShort, MonthHours, UserDefaults } from 'src/app/core/api/models';

export interface MonthlyPreloaded {
    currentMonth: Date;
    projects: ProjectShort[];
    userTimeReports: UserTimeReport[];
    baseLines: MonthHours[];
    userDefaults: UserDefaults;
}

@Injectable()
export class MonthlyResolver  {
    constructor(
        private meService: MeService,
        private operationalService: OperationalService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<MonthlyPreloaded> {
        var date = new Date();
        var currentMonth = new Date(date.getFullYear(), date.getMonth(), 1);

        var year = currentMonth.getFullYear(), month = currentMonth.getMonth();
        var firstDateOfMonth = new Date(year, month, 1);
        var lastDateOfMonth = new Date(year, month + 1, 0);

        return forkJoin([
            this.meService.getDefault(),
            this.meService.getMyTimeReports({
                dateFrom: Date.CreateUTCDate(firstDateOfMonth).toISOString(),
                dateTo: Date.CreateUTCDate(lastDateOfMonth).toISOString()
            }),
            this.operationalService.getBaseLineByYear({ year: currentMonth.getFullYear() }),
            this.meService.getFilterProjects({
                dateFrom: Date.CreateUTCDate(firstDateOfMonth).toISOString(),
                dateTo: Date.CreateUTCDate(lastDateOfMonth).toISOString()
            })
        ]).pipe(map(([userDefaults, userTimeReports, baseLines, projects]) => {
            return {
                userDefaults: userDefaults,
                currentMonth: currentMonth,
                projects: projects,
                userTimeReports: userTimeReports.map((utr) => ({
                    ...utr,
                    project:
                    projects.find((p) => p.id === utr.project.id) || utr.project,
                  })),
                baseLines: baseLines
            };
        }));
    }
}

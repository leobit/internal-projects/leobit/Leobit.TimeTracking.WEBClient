import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, forkJoin } from 'rxjs';
import { map } from 'rxjs/operators';
import { MeService } from 'src/app/core/api/services';
import { ProjectShort, UserDefaults } from 'src/app/core/api/models';

export interface DailyPreloaded {
    userDefaults: UserDefaults;
    projects: ProjectShort[];
}

@Injectable()
export class DailyResolver  {
    constructor(private meService: MeService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<DailyPreloaded> {
        return forkJoin([
            this.meService.getDefault(),
            this.meService.getMyProjects()
        ]).pipe(map(([userDefaults, projects]) => {
            return {
                userDefaults: userDefaults,
                projects: projects
            };
        }));
    }
}

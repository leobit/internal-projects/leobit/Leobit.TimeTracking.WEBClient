import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MessageService } from "primeng/api";
import { MeService } from "src/app/core/api/services/me.service";
import {
  ProjectShort,
  UserDefault,
  CreateTimeReport,
} from "src/app/core/api/models";
import { DailyPreloaded } from "./daily.resolver";
import { calculateRange } from "src/app/core/helpers/calendar.helper";

@Component({
  selector: "app-daily",
  templateUrl: "./daily.component.html",
  styleUrls: ["./daily.component.css"],
})
export class DailyComponent implements OnInit {
  projects: ProjectShort[];
  userDefaults: UserDefault[];

  project: ProjectShort = null;
  reportingDate: Date = new Date();
  reportingRangeDates: Date[] = [new Date()];
  internalHours: number = 0;
  externalHours: number = 0;
  isOverhead: boolean = false;
  workDescription: string = "";
  sendEmail: boolean = false;
  startTime: string = "10:00";

  constructor(
    private route: ActivatedRoute,
    private meService: MeService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    const preloaded = <DailyPreloaded>this.route.snapshot.data.preloaded;

    this.projects = preloaded.projects;
    this.userDefaults = preloaded.userDefaults.defaults;

    this.project =
      this.projects.find((p) => p.id == preloaded.userDefaults.projectId) ||
      this.projects[0] ||
      null;
    this.reportingDate = Date.ParseISOString(preloaded.userDefaults.reportingDate);
    this.internalHours = preloaded.userDefaults.internalHours;
    this.externalHours = preloaded.userDefaults.externalHours;
  }

  Submit(isBatch: boolean = false) {
    if (isBatch && this.reportingRangeDates.length > 1) {
      const filteredRange = this.reportingRangeDates.filter((d) => d !== null);
      const reportingRangeDates = calculateRange(
        filteredRange[0],
        filteredRange[filteredRange.length - 1]
      );
      const timeReports = reportingRangeDates
        .map((date) => this.CreateTimeReport(date));

      this.meService
        .createMyTimeReports({
          body: timeReports,
        })
        .subscribe((userTimeReport) => {
          let message =
            userTimeReport.length > 1
              ? `${userTimeReport[0].reportingDate.parseISOStringToLocaleDateString()} - 
                 ${userTimeReport[userTimeReport.length - 1].reportingDate.parseISOStringToLocaleDateString()} reported`
              : `${userTimeReport[0].reportingDate.parseISOStringToLocaleDateString()
              } reported`;

          this.messageService.add({
            severity: "success",
            summary: "Done",
            detail: message,
          });
        });
    } else {
      this.meService
        .createMyTimeReport({
          body: this.CreateTimeReport(this.reportingDate || this.reportingRangeDates[0]),
        })
        .subscribe((userTimeReport) => {
          const detailMessage = `${userTimeReport.reportingDate.parseISOStringToLocaleDateString()} reported`;
          this.messageService.add({
            severity: "success",
            summary: "Done",
            detail: detailMessage,
          });
        });
    }
  }

  CreateTimeReport(date: Date): CreateTimeReport {
    return {
      projectId: this.project.id,
      reportingDate: Date.CreateUTCDate(date).toISOString(),
      internalHours: this.internalHours,
      externalHours: this.externalHours,
      isOverhead: this.isOverhead,
      workDescription: this.workDescription,
      sendEmail: this.sendEmail,
      startTime:
        this.internalHours !== 8
          ? Date.CreateDateTime(
            this.reportingDate || this.reportingRangeDates[0],
            new Date("1970/01/01 " + this.startTime)
          ).toISOString()
          : null,
    };
  }

  ReloadDefaultHours(selectedProject: ProjectShort) {
    const project = this.userDefaults.find(
      (ud) => ud.projectId == selectedProject.id
    );

    if (project) {
      this.internalHours = project.internalHours;
      this.externalHours = project.externalHours;
    }
  }

  onDateRangeChanged(dateRange: Date[]) {
    if (dateRange) {
      if (!dateRange[dateRange.length - 1]) {
        this.reportingDate = dateRange[0];
      }
    }
  }
}
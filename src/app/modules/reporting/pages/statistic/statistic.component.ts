import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { TreeNode, MessageService, ConfirmationService } from "primeng/api";
import { MeService } from "src/app/core/api/services";
import { UserTimeReport, ProjectShort } from "src/app/core/api/models";
import { Globals } from "src/app/core/globals";
import { StatisticPreloaded } from "./statistic.resolver";
import { Dialog } from "primeng/dialog";
import { ProjectTreeService } from "src/app/core/services/project-tree.service";
import { CalendarDateRange } from "src/app/shared/components/calendar/models/calendar-date-range";
import { ExtendedUserTimeReport } from "../../types/extended-user-time-report";


@Component({
  selector: "app-statistic",
  templateUrl: "./statistic.component.html",
  styleUrls: ["./statistic.component.css"],
})
export class StatisticComponent implements OnInit {
  @ViewChild("dialog", { static: true })
  dialog: Dialog;

  calendarDateRange: CalendarDateRange;

  overheadOptions: any[] = [
    { label: "No", value: false },
    { label: "All", value: null },
    { label: "Yes", value: true },
  ];
  selectedOverhead: any = this.overheadOptions[1];

  userTotals: any[];
  totalInternal: number;
  totalExternal: number;

  reportingDate: Date = new Date();
  startTime: string = "10:00";
  editTimeReport: UserTimeReport | null = null;

  filterProjects: TreeNode[];
  selectedProjects: TreeNode | TreeNode[];
  timeReports: ExtendedUserTimeReport[];
  projects: ProjectShort[];

  currentMonth: Date;

  firstDate: Date;
  lastDate: Date;

  dialogVisible: boolean;

  constructor(
    private route: ActivatedRoute,
    private meService: MeService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private projectTreeService: ProjectTreeService,
    public globals: Globals
  ) { }

  ngOnInit() {
    const preloaded = <StatisticPreloaded>this.route.snapshot.data.preloaded;

    this.projects = preloaded.projects;

    this.calendarDateRange = preloaded.calendarDateRange;
    this.firstDate = this.calendarDateRange.dateFrom;
    this.lastDate = this.calendarDateRange.dateTo;

    this.getFilterProjects(preloaded.reportedProjects);
    this.getSelectedProjects();

    this.timeReports = preloaded.userTimeReports.map((report: UserTimeReport) => ({
      ...report,
      isEditable: report.isEditable
    }));
    this.calculateTotals(preloaded.userTimeReports);
  }

  apply() {
    this.calendarDateRange ??= (<StatisticPreloaded>this.route.snapshot.data.preloaded).calendarDateRange;
    let dateChanged = false;

    if (this.firstDate == null || this.firstDate != this.calendarDateRange.dateFrom) {
      this.firstDate = this.calendarDateRange.dateFrom;
      dateChanged = true;
    }
    if (this.lastDate == null || this.lastDate != this.calendarDateRange.dateTo) {
      this.lastDate = this.calendarDateRange.dateTo;
      dateChanged = true;
    }

    if (dateChanged)
      this.projectFilterChange();
    else {
      this.getUserTimeReports();
    }
  }

  projectFilterChange() {
    this.meService
      .getFilterProjects({
        dateFrom: Date.CreateUTCDate(this.firstDate).toISOString(),
        dateTo: Date.CreateUTCDate(this.lastDate).toISOString(),
      })
      .subscribe((projects) => {
        this.getFilterProjects(projects);
        this.getSelectedProjects();
        this.getUserTimeReports();
      });
  }

  getFilterProjects(projects: ProjectShort[]) {
    this.filterProjects = [
      {
        label: "All",
        expanded: true,
        children: projects
          .groupBy(
            (p) => p.account,
            (a1, a2) => a1.id === a2.id
          )
          .map<TreeNode>((accountItem) => ({
            label: accountItem.key.name,
            expanded: true,
            children: accountItem.value.map<TreeNode>((projectItem) => ({
              label: projectItem.name,
              data: projectItem.id,
            })),
          })),
      },
    ];
  }

  getSelectedProjects() {
    this.selectedProjects = this.filterProjects.reduce(
      (acumulator, value) => {
        return acumulator.concat(value).concat(value.children);
      },
      []
    );

    this.selectedProjects = this.selectedProjects.concat(
      this.selectedProjects
        .map((item) => item.children.filter((child) => child.data))
        .reduce(
          (acumulator, currentValue) => acumulator.concat(currentValue),
          []
        )
    );
  }

  getUserTimeReports() {
    this.projectTreeService
      .getTimeReports(
        this.filterProjects,
        this.selectedProjects as TreeNode[],
        this.projects,
        this.firstDate,
        this.lastDate,
        this.selectedOverhead.value
      )
      .subscribe((reports) => {
        this.timeReports = reports.map((report: UserTimeReport) => ({
          ...report,
          isEditable: report.isEditable
        }));
        this.calculateTotals(this.timeReports);
      });
  }

  calculateTotals(userTimeReports: UserTimeReport[]) {
    const totals = [];
    let totalInternal = 0;
    let totalExternal = 0;
    for (let userTimeReport of userTimeReports) {
      const existingTotal = totals.find(
        (t) =>
          t.account.id === userTimeReport.account.id &&
          t.project.id === userTimeReport.project.id
      );
      if (existingTotal) {
        existingTotal.sumInternal += userTimeReport.internalHours;
        existingTotal.sumExternal += userTimeReport.externalHours;
      } else {
        totals.push({
          account: userTimeReport.account,
          project: userTimeReport.project,
          sumInternal: userTimeReport.internalHours,
          sumExternal: userTimeReport.externalHours,
        });
      }
      totalInternal += userTimeReport.internalHours;
      totalExternal += userTimeReport.externalHours;
    }
    this.userTotals = totals;
    this.totalInternal = totalInternal;
    this.totalExternal = totalExternal;
  }

  onEditTimeReport(report: UserTimeReport) {
    this.reportingDate = Date.ParseISOString(report.reportingDate);
    const startTimeDate = report.startTime ? new Date(report.startTime) : null;
    this.startTime = startTimeDate
      ? `${startTimeDate.getHours().toString().padStart(2, "0")}:${startTimeDate
        .getMinutes()
        .toString()
        .padStart(2, "0")}`
      : "10:00";

    this.editTimeReport = { ...report, project: report.project }
    this.showDialog();
  }

  onSave() {
    this.editTimeReport.reportingDate = Date.CreateUTCDate(
      this.reportingDate
    ).toISOString();
    this.editTimeReport.startTime =
      this.editTimeReport.internalHours !== 8
        ? Date.CreateDateTime(
          this.reportingDate,
          new Date("1970/01/01 " + this.startTime)
        ).toISOString()
        : null;

    this.meService
      .editMyTimeReport({
        body: {
          ...this.editTimeReport,
          projectId: this.editTimeReport.project.id,
        },
      })
      .subscribe((userTimeReport) => {
        const editedReport = this.timeReports.find(x => x.id === this.editTimeReport.id)
        if (editedReport) {
          const i = this.timeReports.indexOf(editedReport);
          this.timeReports[i] = {
            ...this.editTimeReport,
            account: this.editTimeReport.project.account
          };
        }
        this.messageService.add({
          severity: "success",
          summary: "Done",
          detail: `${userTimeReport.reportingDate.parseISOStringToLocaleDateString()} updated`,
        });
      });
    this.hideDialog();
  }

  onDeleteTimeReport(report: UserTimeReport) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to delete reporting for ${report.reportingDate.parseISOStringToLocaleDateString()
        }?`,
      accept: () => {
        this.meService.deleteMyTimeReport({ id: report.id }).subscribe(() => {
          this.messageService.add({
            severity: "success",
            summary: "Done",
            detail: `${report.reportingDate.parseISOStringToLocaleDateString()} deleted`,
          });

          this.timeReports = this.timeReports.filter(
            (userTimeReport) => userTimeReport.id !== report.id
          );
          this.calculateTotals(this.timeReports);
        });
      },
    });
  }

  private showDialog() {
    this.dialogVisible = true;
    setTimeout(() => {
      this.dialog.center();
    }, 10);
  }

  private hideDialog() {
    this.dialogVisible = false;
  }
}

import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable, forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { MeService } from "src/app/core/api/services";
import { UserTimeReport, ProjectShort } from "src/app/core/api/models";
import { CalendarDateRange } from "src/app/shared/components/calendar/models/calendar-date-range";

export interface StatisticPreloaded {
  calendarDateRange: CalendarDateRange;
  projects: ProjectShort[];
  userTimeReports: UserTimeReport[];
  reportedProjects: ProjectShort[];
}

@Injectable()
export class StatisticResolver  {
  constructor(private meService: MeService) {}

  resolve(
    _route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot
  ): Observable<StatisticPreloaded> {
    const date = new Date(),
      year = date.getFullYear(),
      month = date.getMonth();

    const calendarDateRange = new CalendarDateRange({
      dateFrom: new Date(year, month, 1).toISOString(), // first day in month
      dateTo: new Date(year, month + 1, 0).toISOString(), // last day in month
    });

    return forkJoin([
      this.meService.getMyTimeReports({
        dateFrom: Date.CreateUTCDate(calendarDateRange.dateFrom).toISOString(),
        dateTo: Date.CreateUTCDate(calendarDateRange.dateTo).toISOString(),
      }),
      this.meService.getFilterProjects({
        dateFrom: Date.CreateUTCDate(calendarDateRange.dateFrom).toISOString(),
        dateTo: Date.CreateUTCDate(calendarDateRange.dateTo).toISOString()
      })
    ]).pipe(
      map(([userTimeReports, projects]) => {
        return {
          calendarDateRange: calendarDateRange,
          projects: projects,
          userTimeReports: userTimeReports.map((utr) => ({
            ...utr,
            project: projects.find((p: ProjectShort) => p.id == utr.project.id)
              ?? utr.project,
          })),
          reportedProjects: projects,
        };
      })
    );
  }
}

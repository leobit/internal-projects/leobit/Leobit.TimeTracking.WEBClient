import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VacationInformation, SickLeaveInformation, SickLeaveMonthData, VacationMonthData, CustomDayOff } from 'src/app/core/api/models';
import { VacationAndSickLeavePreloaded } from './vacation-and-sick-leave.resolver';
import { VacationService, TimeOffService } from 'src/app/core/api/services';

@Component({
  selector: 'app-vacation-and-sick-leave',
  templateUrl: './vacation-and-sick-leave.component.html',
  styleUrls: ['./vacation-and-sick-leave.component.css']
})
export class VacationAndSickLeaveComponent implements OnInit {
  currentYear: number = (new Date()).getFullYear();
  selectedYear: Date = new Date();
  vacationInformation: VacationInformation;
  accrualDays: number[];
  usedDays: number[];
  employmentDate: Date;
  isTMEmployee: boolean;
  terminationDate: Date | null;
  extraDaysOff: number;
  compensatedVacationLeftovers: number;
  sickLeaveInformation: SickLeaveInformation;
  sickLeaveWithoutCertificate: number[];
  sickLeavewithCertificate: number[];

  customDayOffs: CustomDayOff[];

  @Input() newEmploymentDate: Date;

  constructor(
    private route: ActivatedRoute,
    private vacationService: VacationService,
    private timeOffService: TimeOffService) { }

  ngOnInit() {
    var preloaded = <VacationAndSickLeavePreloaded>this.route.snapshot.data.preloaded;
    this.vacationInformation = preloaded.vacationInformation;
    this.accrualDays = this.yearDataToArray(this.vacationInformation.yearData, (x: VacationMonthData) => x.accrual);
    this.usedDays = this.yearDataToArray(this.vacationInformation.yearData, (x: VacationMonthData) => x.used);
    this.employmentDate = this.vacationInformation?.employmentDate ? new Date(this.vacationInformation.employmentDate) : null;
    this.isTMEmployee = this.vacationInformation?.isTMEmployee;
    this.terminationDate = !!this.vacationInformation.terminationDate ? new Date(this.vacationInformation.terminationDate) : null;
    this.sickLeaveInformation = preloaded.sickLeaveInformation;
    this.sickLeaveWithoutCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withoutCertificate);
    this.sickLeavewithCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withCertificate);
    this.extraDaysOff = this.vacationInformation.extraDaysOff;
    this.compensatedVacationLeftovers = this.vacationInformation.compensatedVacationLeftovers;

    this.updateCustomDayOffs();
  }

  previousYearClick() {
    this.selectedYear.setFullYear(this.selectedYear.getFullYear() - 1);
    this.updateVacationAndSickLeaveInformation();
    this.updateCustomDayOffs();
  }

  nextYearClick() {
    this.selectedYear.setFullYear(this.selectedYear.getFullYear() + 1);
    this.updateVacationAndSickLeaveInformation();
    this.updateCustomDayOffs();
  }

  updateInformation(event){
    this.employmentDate = this.vacationInformation?.employmentDate ? new Date(this.vacationInformation.employmentDate) : null;
    this.updateVacationAndSickLeaveInformation();
    this.updateCustomDayOffs();
  }

  private updateVacationAndSickLeaveInformation() {
    this.timeOffService.getTimeOffsInformation({ year: this.selectedYear.getFullYear() }).subscribe(res => {
      this.vacationInformation = res;
      this.employmentDate = this.vacationInformation?.employmentDate ? new Date(this.vacationInformation.employmentDate) : null;
      this.terminationDate = !!this.vacationInformation.terminationDate ? new Date(this.vacationInformation.terminationDate) : null;
      this.accrualDays = this.yearDataToArray(this.vacationInformation.yearData, (x: VacationMonthData) => x.accrual);
      this.usedDays = this.yearDataToArray(this.vacationInformation.yearData, (x: VacationMonthData) => x.used);
      this.sickLeaveInformation = {...res, yearData: res.sickLeaveYearData};;
      this.sickLeaveWithoutCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withoutCertificate);
      this.sickLeavewithCertificate = this.yearDataToArray(this.sickLeaveInformation.yearData, (x: SickLeaveMonthData) => x.withCertificate);
      this.extraDaysOff = this.vacationInformation.extraDaysOff;
      this.compensatedVacationLeftovers = this.vacationInformation.compensatedVacationLeftovers;
    });
  }

  private updateCustomDayOffs() {
    this.vacationService.getCustomDayOffs({ year: this.selectedYear.getFullYear() }).subscribe(res => {
      this.customDayOffs = res.map(cdo => ({ ...cdo, lastEditTimestamp: cdo.lastEditTimestamp + "Z" }));
    });
  }

  private yearDataToArray(yearData: {
    january?: any
    february?: any
    march?: any
    april?: any
    may?: any
    june?: any
    july?: any
    august?: any
    september?: any
    october?: any
    november?: any
    december?: any
  }, propertyAccessor: (x?: any) => number): number[] {
    return [
      propertyAccessor(yearData.january   || {}),
      propertyAccessor(yearData.february  || {}),
      propertyAccessor(yearData.march     || {}),
      propertyAccessor(yearData.april     || {}),
      propertyAccessor(yearData.may       || {}),
      propertyAccessor(yearData.june      || {}),
      propertyAccessor(yearData.july      || {}),
      propertyAccessor(yearData.august    || {}),
      propertyAccessor(yearData.september || {}),
      propertyAccessor(yearData.october   || {}),
      propertyAccessor(yearData.november  || {}),
      propertyAccessor(yearData.december  || {}),
    ];
  }

}

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TimeOffService } from 'src/app/core/api/services';
import { VacationInformation, SickLeaveInformation } from 'src/app/core/api/models';

export interface VacationAndSickLeavePreloaded {
  vacationInformation: VacationInformation;
  sickLeaveInformation: SickLeaveInformation;
}

@Injectable()
export class VacationAndSickLeaveResolver  {
  constructor(private timeOffService: TimeOffService,) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<VacationAndSickLeavePreloaded> {
    let year = route.params['year'] | new Date().getFullYear();
    return this.timeOffService.getTimeOffsInformation({ year })
      .pipe(map((timeOffsInformation) => {
        return {
          vacationInformation: timeOffsInformation,
          sickLeaveInformation: {...timeOffsInformation, yearData: timeOffsInformation.sickLeaveYearData}
        };
      }));
  }
}

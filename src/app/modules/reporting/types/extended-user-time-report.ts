import { UserTimeReport } from "src/app/core/api/models";
import { isTimeReportEditable } from "src/app/core/helpers/UserTimeReport.helper";

export interface ExtendedUserTimeReport extends UserTimeReport {
  isEditable?: boolean;
}

export function mapUserTimeReport(report: UserTimeReport): ExtendedUserTimeReport {
  return {
    ...report,
    account: report.account ?? report.project.account,
    isEditable: isTimeReportEditable(report)
  };
}
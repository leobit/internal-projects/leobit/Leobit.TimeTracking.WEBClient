import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DailyComponent } from './pages/daily/daily.component';
import { DailyResolver } from './pages/daily/daily.resolver';

import { StatisticComponent } from './pages/statistic/statistic.component';
import { StatisticResolver } from './pages/statistic/statistic.resolver';

import { MonthlyComponent } from './pages/monthly/monthly.component';
import { MonthlyResolver } from './pages/monthly/monthly.resolver';

import { VacationAndSickLeaveComponent } from './pages/vacation-and-sick-leave/vacation-and-sick-leave.component';
import { VacationAndSickLeaveResolver } from './pages/vacation-and-sick-leave/vacation-and-sick-leave.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';


const reportingRoutes: Routes = [
  {
    path: '',
    redirectTo: 'daily',
    pathMatch: 'full'
  },
  {
    path: 'daily',
    component: DailyComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: DailyResolver }
  },
  {
    path: 'statistic',
    component: StatisticComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: StatisticResolver }
  },
  {
    path: 'monthly',
    component: MonthlyComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: MonthlyResolver }
  },
  {
    path: 'vacation-and-sick-leave',
    component: VacationAndSickLeaveComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: VacationAndSickLeaveResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(reportingRoutes)],
  exports: [RouterModule]
})
export class ReportingRoutingModule { }
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { ProjectShort, UserDefaults, UserTimeReport } from 'src/app/core/api/models';
import { MeService } from 'src/app/core/api/services';
import { isTimeReportEditable } from 'src/app/core/helpers/UserTimeReport.helper';
import { ExtendedUserTimeReport } from '../../types/extended-user-time-report';

@Component({
  selector: 'app-time-reports-table-preview',
  templateUrl: './time-reports-table-preview.component.html',
  styleUrls: ['./time-reports-table-preview.component.css']
})
export class TimeReportsTablePreviewComponent implements OnInit {
  @Input() timeReports: ExtendedUserTimeReport[] = [];
  @Input() projects: ProjectShort[];
  @Input() reportingDate: Date = new Date();
  @Input() userDefaults: UserDefaults;
  @Output() addTimeReport = new EventEmitter<UserTimeReport>();
  @Output() editTimeReport = new EventEmitter<UserTimeReport>();
  @Output() deleteTimeReport = new EventEmitter<UserTimeReport>();

  userAssignedProjects: ProjectShort[];
  availableProjectsForReporting: ProjectShort[];
  isEditDialogVisible: boolean = false;
  isAddDialogVisible: boolean = false;
  selectedTimeReport: UserTimeReport = {
    id: 0,
    project: null,
    reportingDate: "",
    internalHours: 0,
    externalHours: 0,
    workDescription: "",
    isOverhead: false,
    sendEmail: false,
    startTime: null,
  };

  startTime: string;

  constructor(
    private messageService: MessageService,
    private meService: MeService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.projects = this.projects.filter(
      (project: ProjectShort) => !project.isClosed
    );

    this.meService.getAssignedUserProjects()
      .subscribe((assignedUserProjectIds) => {
        this.userAssignedProjects = this.filterAssignedProject(assignedUserProjectIds)
      })
  }

  onAddTimeReport() {
    this.setStartTime(this.selectedTimeReport);

    this.selectedTimeReport.reportingDate = Date.CreateUTCDate(this.reportingDate).toISOString();

    this.meService.createMyTimeReport({
      body: {
        ...this.selectedTimeReport,
        projectId: this.selectedTimeReport.project.id
      }
    })
      .subscribe((report) => {
        this.timeReports.push({
          ...this.selectedTimeReport,
          id: report.id,
          account: report.project.account,
          isEditable: isTimeReportEditable(report)
        });
        this.addTimeReport.emit({ ...this.selectedTimeReport, id: report.id, account: report.project.account });
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: `${report.reportingDate.parseISOStringToLocaleDateString()} reported`
        })

        this.isAddDialogVisible = false;
      })
  }

  onEditTimeReport() {
    this.selectedTimeReport.reportingDate = Date.CreateUTCDate(
      this.reportingDate
    ).toISOString();

    this.setStartTime(this.selectedTimeReport);

    this.meService
      .editMyTimeReport({
        body: {
          ...this.selectedTimeReport,
          projectId: this.selectedTimeReport.project.id,
        },
      })
      .subscribe((userTimeReport) => {
        const editedReport = this.timeReports.find(x => x.id === this.selectedTimeReport.id)
        if (editedReport) {
          const i = this.timeReports.indexOf(editedReport);
          this.timeReports[i] = {
            ...this.selectedTimeReport,
            account: userTimeReport.project.account,
          };
          this.editTimeReport.emit({ ...this.selectedTimeReport, account: userTimeReport.project.account })
        }
        this.messageService.add({
          severity: "success",
          summary: "Done",
          detail: `${userTimeReport.reportingDate.parseISOStringToLocaleDateString()} updated`,
        });
        this.isEditDialogVisible = false;
      });
  }

  onDeleteTimeReport(report: UserTimeReport) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to delete reporting for ${report.reportingDate.parseISOStringToLocaleDateString()}?`,
      accept: () => {
        this.meService.deleteMyTimeReport({ id: report.id }).subscribe(() => {
          this.messageService.add({
            severity: "success",
            summary: "Done",
            detail: `${report.reportingDate.parseISOStringToLocaleDateString()} deleted`,
          });
          this.timeReports = this.timeReports.filter(timeReport => timeReport.id !== report.id);
          this.deleteTimeReport.emit(report);
        });
      },
    });
  }

  showEditReportDialog(report: UserTimeReport) {
    this.availableProjectsForReporting = this.filterExpiredAssignmentProject();

    const startTimeDate = report.startTime ? new Date(report.startTime) : null;
    this.startTime = startTimeDate
      ? `${startTimeDate.getHours().toString().padStart(2, "0")}:${startTimeDate
        .getMinutes()
        .toString()
        .padStart(2, "0")}`
      : "10:00";

    this.selectedTimeReport = { ...report, project: report.project }
    this.isEditDialogVisible = true;
  }

  showAddReportDialog() {
    this.availableProjectsForReporting = this.filterExpiredAssignmentProject();

    this.startTime = "10:00";
    const projectDef = this.userDefaults.defaults.find(
      (ud) => ud.projectId == this.userDefaults.projectId
    );

    this.selectedTimeReport = {
      id: 0,
      project: this.availableProjectsForReporting.find((p) => p.id == this.userDefaults.projectId) ||
        this.availableProjectsForReporting[0] ||
        null,
      reportingDate: "",
      internalHours: projectDef?.internalHours || this.userDefaults.internalHours || 0,
      externalHours: projectDef?.externalHours || this.userDefaults.externalHours || 0,
      workDescription: "",
      isOverhead: false,
      sendEmail: false,
      startTime: null,
    }
    this.isAddDialogVisible = true;
  }

  isUserAssignedToProject(projectId: number): boolean {
    return this.userAssignedProjects.some(project => project.id == projectId && !this.isUserAssignmentExpired(project, this.reportingDate));
  }

  isUserAssignmentExpired(project: ProjectShort, currentDate: Date) {
    const dateRanges = project.restriction?.allowedDateRanges?.map(dateRange => ({
      startDate: Date.ParseISOString(dateRange.startDate),
      endDate: Date.ParseISOString(dateRange.endDate)
    })) || [];

    if (!dateRanges.length) {
      return true;
    }

    return !dateRanges.some(dateRange => dateRange.endDate >= currentDate);
  }

  private setStartTime(report: UserTimeReport) {
    report.startTime =
      report.internalHours !== 8
        ? Date.CreateDateTime(
          this.reportingDate,
          new Date("1970/01/01 " + this.startTime)
        ).toISOString()
        : null
  }

  private filterAssignedProject(assignedUserProjectIds: number[]): ProjectShort[] {
    return this.projects.filter(p => assignedUserProjectIds.includes(p.id));
  }

  private filterExpiredAssignmentProject(): ProjectShort[] {
    return this.projects.filter(project => this.isUserAssignedToProject(project.id));
  }

  ReloadDefaultHours(selectedProject: ProjectShort) {
    const project = this.userDefaults.defaults.find(
      (ud) => ud.projectId == selectedProject.id
    );

    if (project) {
      this.selectedTimeReport.internalHours = project.internalHours;
      this.selectedTimeReport.externalHours = project.externalHours;
    }
  }
}

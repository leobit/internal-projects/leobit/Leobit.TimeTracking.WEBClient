import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from "@angular/core";
import { SelectItem, ConfirmationService, MessageService } from "primeng/api";
import {
  AccountShort,
  ProjectShort,
  Holiday,
  ProjectReopenRequest,
  ReopenReportingApprovalAuthority
} from "src/app/core/api/models";
import { Globals } from "src/app/core/globals";
import { Restriction } from "src/app/core/api/models/restriction";
import { catchError, map, of, Subscription, tap } from "rxjs";
import { MeService, OperationalService, ProjectService } from "src/app/core/api/services";
import {getIsExternal, getIsOperational} from "src/app/core/helpers/ProjectShort.helper";
import { calculateRange } from "src/app/core/helpers/calendar.helper";
import { UserProfileService } from "src/app/core/services/user-profile.service";

type ReopenReportingAuthorityExtent =
  Omit<ReopenReportingApprovalAuthority, "reopenAllowedFromDate"> &
  Omit<ReopenReportingApprovalAuthority, "reopenAllowedEndDate"> & {
    reopenAllowedFromDate: Date,
    reopenAllowedEndDate?: Date
  }

@Component({
  selector: "app-time-report",
  templateUrl: "./time-report.component.html",
  styleUrls: ["./time-report.component.css"],
})
export class TimeReportComponent implements OnInit, OnChanges {

  @Input() projects: ProjectShort[];
  @Input() autoResize: boolean;
  @Input() selectedProject: ProjectShort;
  @Output() selectedProjectChange = new EventEmitter<ProjectShort>();

  @Input() dateRange: Date[];
  @Output() dateRangeChange = new EventEmitter<Date[]>();

  @Input() date: Date;
  @Output() dateChange = new EventEmitter<Date>();

  @Input() reportMultipleDates: boolean;

  @Input() internalHours: number;
  @Output() internalHoursChange = new EventEmitter<number>();

  @Input() externalHours: number;
  @Output() externalHoursChange = new EventEmitter<number>();

  @Input() isOverhead: boolean;
  @Output() isOverheadChange = new EventEmitter<boolean>();

  @Input() description: string;
  @Output() descriptionChange = new EventEmitter<string>();

  @Input() sendMeCopyOfResponse: boolean;
  @Output() sendMeCopyOfResponseChange = new EventEmitter<boolean>();

  @Input() startTime: string;
  @Output() startTimeChange = new EventEmitter<string>();

  @Input() submitButtonLabel: string = "Submit";
  @Input() allowMultipleDates = false;

  @Input() isDisableDatePicker: boolean;
  @Output() submitButtonClick = new EventEmitter();
  @Input() isDaily = false;
  @Input() isDisableReOpenProjectReporting: boolean;
  @Input() appendInnerDialogToBody = false;

  sub: Subscription = new Subscription();

  uiFirstAllowedDate = new Date();
  uiLastAllowedDate = new Date();
  uiDisabledDates: Date[] = [];
  batchProjectTypes = [2, 4, 5];

  uiInternalHours: SelectItem[] = Array.from({ length: 25 })
    .map((_, i) => i * 0.5)
    .map((i) => ({ label: `${i}`, value: i }));

  uiExternalHours: SelectItem[] = Array.from({ length: 25 })
    .map((_, i) => i * 0.5)
    .map((i) => ({ label: `${i}`, value: i }));

  uiStartTime = Array(24 * 4)
    .fill(0)
    .map((_, i) => {
      return ("0" + ~~(i / 4) + ":0" + 60 * ((i / 4) % 1)).replace(
        /\d(\d\d)/g,
        "$1"
      );
    })
    .map((i) => ({ label: `${i}`, value: i }));

  uiAccounts: AccountShort[];
  uiSelectedAccount: AccountShort;
  uiProjects: ProjectShort[];

  holidays: Holiday[] = [];
  isAllowedDate: boolean;
  showReopenProjectReportingDialog: boolean = false;
  reOpenDateRange: Date[];
  isReOpenDateRangeFullyAvailable: boolean;
  isReOpenDateRangeFullyConsistsOfHolidays: boolean;
  minReopenReportingDate: Date;
  maxReopenReportingDate: Date;
  reopenReportAuthorityUsers: string[];
  isReopenDateRangeSelected: boolean;

  private reopenReportingAuthorities: ReopenReportingAuthorityExtent[];

  constructor(
    public globals: Globals,
    private operationalService: OperationalService,
    private confirmationService: ConfirmationService,
    private meService: MeService,
    private userProfileService: UserProfileService,
    private messageService: MessageService,
    private projectService: ProjectService
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    // If selectedProject changed
    if (changes['selectedProject'] !== undefined) {
      const value = changes['selectedProject'].currentValue as ProjectShort;

      if (value !== null) {
        if (this.projects && this.projects.length > 0) {
          this.uiAccounts = this.projects
            .map((p) => p.account)
            .filter(
              (a, index, self) => self.findIndex((x) => x.id == a.id) == index
            )
            .sortBy((a) => a.name);

          this.uiSelectedAccount =
            this.uiAccounts.find(
              (a) => a.id == this.selectedProject.account.id
            ) || this.uiAccounts[0];

          this.uiProjects = this.projects
            .filter((p) => p.account.id == this.uiSelectedAccount.id)
            .sortBy((p) => p.name);

          // TODO: workaround
          setTimeout(() => {
            this.adjustDate(value.restriction);
            this.adjustHours(value.restriction);
          });
        }
      }
    }
  }

  ngOnInit() {
    if (this.selectedProject === undefined) {
      throw new Error("Attribute selectedProject is required");
    }
    if (this.date === undefined) {
      throw new Error("Attribute date is required");
    }
    if (this.internalHours === undefined) {
      throw new Error("Attribute internalHours is required");
    }
    if (this.externalHours === undefined) {
      throw new Error("Attribute externalHours is required");
    }
    if (this.isOverhead === undefined) {
      throw new Error("Attribute isOverhead is required");
    }
    if (this.description === undefined) {
      throw new Error("Attribute description is required");
    }

    this.sub.add(
      this.operationalService
        .getHolidaysByYear({ year: new Date().getFullYear() })
        .subscribe((r) => (this.holidays = r))
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  get isOperational(): boolean {
    return getIsOperational(this.selectedProject);
  }

  get isExternal(): boolean {
    return getIsExternal(this.selectedProject);
  }

  get isFullDay(): boolean {
    return this.internalHours == 8;
  }

  get projectSupportsBatchReport(): boolean {
    return this.batchProjectTypes.includes(this.selectedProject.projectType);
  }

  get batchReportingAllowed(): boolean {
    return this.allowMultipleDates && this.projectSupportsBatchReport;
  }

  submitClicked() {
    const verticalScrollOffset = window.scrollY;
    window.scrollTo(0, verticalScrollOffset);
    if (this.reportMultipleDates) {
      this.submitMultipleDatesReport();
    } else {
      this.submitSingleDateReport();
    }
  }

  onAccountChange(model: AccountShort) {
    this.uiProjects = this.projects
      .filter((p) => p.account.id == model.id)
      .sortBy((p) => p.name);

    this.selectedProjectChange.emit(this.uiProjects[0]);
  }

  onReportMultipleDatesChange() {
    this.reportMultipleDates
      ? (this.dateRange = [this.date])
      : (this.date = this.dateRange[0]);
  }

  onReOpenDateRangeChange() {
    const allowedDateRanges = this.selectedProject.restriction.allowedDateRanges.map(r =>
      [new Date(r.startDate), new Date(r.endDate)]
    );
    const isDateInRange = (date, ranges) => {
      return ranges.some(([start, end]) => date >= start && date <= end);
    };

    this.isReOpenDateRangeFullyAvailable =
      isDateInRange(new Date(this.reOpenDateRange[0]), allowedDateRanges) &&
      isDateInRange(new Date(this.reOpenDateRange[1]), allowedDateRanges);
    this.isReOpenDateRangeFullyConsistsOfHolidays = !this.anyBusinessDaysInReOpenDateRange();
  }

  onReopenDateRangeClosed() {
    this.reOpenDateRange = this.reOpenDateRange?.[0]
      ? [this.reOpenDateRange[0], this.reOpenDateRange?.[1] ?? this.reOpenDateRange[0]]
      : DateRangeInitialValue;

    this.isReopenDateRangeSelected = this.reOpenDateRange?.length > 1 && !this.isReOpenDateRangeFullyAvailable && !this.isReOpenDateRangeFullyConsistsOfHolidays;
    if (this.isReopenDateRangeSelected) {
      this.meService
        .getReopenRequestNotificationRecipients({
          ProjectId: this.selectedProject.id,
          StartDate: Date.CreateUTCDate(this.reOpenDateRange[0]).toUTCString(),
          EndDate: Date.CreateUTCDate(!this.reOpenDateRange[1] ? this.reOpenDateRange[0] : this.reOpenDateRange[1]).toUTCString(),
          SenderId: this.userProfileService.userProfile?.sub
        })
        .subscribe((res) => { this.reopenReportAuthorityUsers = res; });
    } else {
      this.reopenReportAuthorityUsers = [];
    }
  }

  hourMask(e, defaultTime) {
    const isDropdown = e.target.className.includes("dropdown");
    const isValidHourFormat = /^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/.test(
      e.target.value
    );

    if (isDropdown && !isValidHourFormat) {
      this.startTime = defaultTime;
      this.startTimeChange.emit(defaultTime);
    }
  }

  submitReOpenProjectReporting() {
    const userReopenProjectReporting: ProjectReopenRequest = {
      projectId: this.selectedProject.id,
      startDate: Date.CreateUTCDate(this.reOpenDateRange[0]).toUTCString(),
      endDate: Date.CreateUTCDate(!this.reOpenDateRange[1] ? this.reOpenDateRange[0] : this.reOpenDateRange[1]).toUTCString(),
      senderId: this.userProfileService.userProfile?.sub
    };
    this.meService
      .sendSlackMessageReopenProjectReporting({
        body: userReopenProjectReporting,
      })
      .subscribe((res) => {
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: `${res}`
        });
        this.hideDialog();
      });
  }

  onReopenProjectReportingClick() {
    if (this.selectedProject) {
      this.projectService.getReopenReportingAuthorities({ id: this.selectedProject.id })
        .pipe(
          map(reopenReportingAuthorities => reopenReportingAuthorities.map(authority => ({
            ...authority,
            reopenAllowedFromDate: new Date(authority.reopenAllowedFromDate),
            reopenAllowedEndDate: authority.reopenAllowedEndDate
              ? new Date(authority.reopenAllowedEndDate)
              : null
          }) as ReopenReportingAuthorityExtent)),
          tap((reopenReportingAuthorities: ReopenReportingAuthorityExtent[]) => {
            this.reopenReportingAuthorities = reopenReportingAuthorities.sort(
              (a, b) => b.reopenAllowedFromDate.getTime() - a.reopenAllowedFromDate.getTime()
            );
            this.minReopenReportingDate = this.reopenReportingAuthorities[
              this.reopenReportingAuthorities.length - 1
            ].reopenAllowedFromDate;

            this.reopenReportingAuthorities = reopenReportingAuthorities.sort(
              (a, b) => b.reopenAllowedFromDate.getTime() - a.reopenAllowedFromDate.getTime()
            );
            this.maxReopenReportingDate = this.reopenReportingAuthorities[0].reopenAllowedEndDate;
          }),
          catchError(_ => {
            this.messageService.add({ severity: 'error', summary: 'Error fetching reopen reporting authorities'});
            return of(null);
          })
        )
        .subscribe((result ) => {
          this.showReopenProjectReportingDialog = result !== null;
        });
    }
  }

  hideDialog() {
    this.showReopenProjectReportingDialog = false;
    this.isReopenDateRangeSelected = false;
    this.reopenReportingAuthorities = [];
    this.reOpenDateRange = DateRangeInitialValue;
  }

  private adjustDate(restriction: Restriction) {
    this.isAllowedDate = true;

    const dateRanges = restriction.allowedDateRanges.map(r =>
      [Date.ParseISOString(r.startDate), Date.ParseISOString(r.endDate)]);

    const firstAllowedDate = new Date(Math.min(...dateRanges.map(r => r[0].getTime())));
    const lastAllowedDate = new Date(Math.max(...dateRanges.map(r => r[1].getTime())));

    this.uiDisabledDates = this.getDisabledDates(dateRanges, firstAllowedDate, lastAllowedDate);

    if (this.uiFirstAllowedDate.getTime() != firstAllowedDate.getTime()) {
      this.uiFirstAllowedDate = firstAllowedDate;
    }

    if (this.uiLastAllowedDate.getTime() != lastAllowedDate.getTime()) {
      this.uiLastAllowedDate = lastAllowedDate;
    }

    if (this.batchReportingAllowed && this.reportMultipleDates) {
      if (this.dateRange === null) {
        this.dateRange = [this.date];
      }
      this.date = null;

      let rangeStart = this.dateRange[0];
      if (rangeStart.getTime() < firstAllowedDate.getTime()) {
        rangeStart = firstAllowedDate;
      } else if (rangeStart.getTime() > lastAllowedDate.getTime()) {
        rangeStart = lastAllowedDate;
      }

      let rangeEnd = this.dateRange[this.dateRange.length - 1];
      if (rangeEnd.getTime() < firstAllowedDate.getTime()) {
        rangeEnd = firstAllowedDate;
      } else if (rangeEnd.getTime() > lastAllowedDate.getTime()) {
        rangeEnd = lastAllowedDate;
      }

      this.dateRange = calculateRange(rangeStart, rangeEnd);
      this.dateRangeChange.emit(this.dateRange);
      this.dateChange.emit(this.date);
    } else {
      if (this.date === null) {
        this.date = this.dateRange[0];
      }
      this.dateRange = null;

      if (this.isDaily) {
        if (this.date.getTime() < firstAllowedDate.getTime()) {
          this.date = firstAllowedDate;
        } else if (this.date.getTime() > lastAllowedDate.getTime()) {
          this.date = lastAllowedDate;
        }
      } else {
        if (this.date.getTime() < firstAllowedDate.getTime() || this.date.getTime() > lastAllowedDate.getTime()) {
          this.isAllowedDate = false;
        }
      }
      this.dateRangeChange.emit(this.dateRange);
      this.dateChange.emit(this.date);
    }
  }

  private getDisabledDates(dateRanges: Date[][], firstAllowedDate: Date, lastAllowedDate: Date): Date[] {
    let disabledDates: Date[] = [];
    let currentDate = new Date(firstAllowedDate.getTime());

    while (currentDate <= lastAllowedDate) {
      if (!dateRanges.some(range => currentDate >= range[0] && currentDate <= range[1])) {
        disabledDates.push(new Date(currentDate.getTime()));
      }
      currentDate.setDate(currentDate.getDate() + 1);
    }

    return disabledDates;
  }

  private adjustHours(restriction: Restriction) {
    let { minInternal, maxInternal, minExternal, maxExternal } = restriction;

    if (
      minInternal != this.uiInternalHours[0].value ||
      maxInternal != this.uiInternalHours[this.uiInternalHours.length - 1].value
    ) {
      this.uiInternalHours = Array.from({
        length: (maxInternal - minInternal) * 2 + 1,
      })
        .map((_, i) => minInternal + i * 0.5)
        .map((i) => ({ label: `${i}`, value: i }));

      if (this.internalHours < minInternal) {
        this.internalHoursChange.emit(minInternal);
      } else if (this.internalHours > maxInternal) {
        this.internalHoursChange.emit(maxInternal);
      }
    }

    if (
      minExternal != this.uiExternalHours[0].value ||
      maxExternal != this.uiExternalHours[this.uiExternalHours.length - 1].value
    ) {
      this.uiExternalHours = Array.from({
        length: (maxExternal - minExternal) * 2 + 1,
      })
        .map((_, i) => minExternal + i * 0.5)
        .map((i) => ({ label: `${i}`, value: i }));

      if (this.externalHours < minExternal) {
        this.externalHoursChange.emit(minExternal);
      } else if (this.externalHours > maxExternal) {
        this.externalHoursChange.emit(maxExternal);
      }
    }
  }

  private submitMultipleDatesReport() {
    if (this.date !== null && this.dateRange == null) {
      throw Error("Invalid dates selected");
    }
    if (!this.batchReportingAllowed) {
      throw Error("Batch reporting is now allowed for this project.");
    }
    this.dateRangeChange.emit(this.dateRange);

    if (this.anyHolidaysInDateRange()) {
      this.confirmationService.confirm({
        message: `Selected range contains official holidays that will be automatically excluded from your report.
          Do you want to continue?`,
        key: "holidayConfirm",
        accept: () => this.submitButtonClick.emit(true),
      });
    } else {
      this.submitButtonClick.emit(true);
    }
  }

  private submitSingleDateReport() {
    if (this.isHoliday(this.date)) {
      this.confirmationService.confirm({
        message:
          "This day is an official holiday that should not be reported. Terminate?",
        key: "holidayConfirm",
        reject: () =>
          setTimeout(
            () =>
              this.confirmationService.confirm({
                message: "Are you sure you have been working on a holiday?",
                accept: () => this.submitButtonClick.emit(false),
              }),
            0
          ),
      });
    } else {
      this.submitButtonClick.emit(false);
    }
  }

  private anyHolidaysInDateRange(): boolean {
    return this.anyDateInRange(this.dateRange, d => this.isHoliday(d));
  }

  private anyBusinessDaysInReOpenDateRange(): boolean {
    return this.anyDateInRange(this.reOpenDateRange, d => !this.isHoliday(d));
  }

  private anyDateInRange(dateRange: Date[], predicate: (item: Date) => boolean): boolean {
    let anyDays = false;
    let date = new Date(dateRange[0]);
    let endDate = dateRange[1] ? new Date(dateRange[1]) : new Date(dateRange[0]);

    while (date <= endDate) {
      if (predicate(date)) {
        anyDays = true;
        break;
      }

      date.setDate(date.getDate() + 1);
    }

    return anyDays;
  }

  private isHoliday(date: Date): boolean {
    return (
      date.getDay() === 6 ||
      date.getDay() === 0 ||
      this.holidays.find((h) => {
        let tmp = new Date(h.date);
        return (
          tmp.getFullYear() === date.getFullYear() &&
          tmp.getMonth() == date.getMonth() &&
          tmp.getDate() === date.getDate()
        );
      }) != null
    );
  }


}

const DateRangeInitialValue: Date[] = null;

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {
  @Input() title: string;
  @Input() description: string;

  @Input() textField: string;
  @Input() items: any[];

  @Input() selectedItem: any;
  @Output() selectedItemChange = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onSelectedItemChange(model: any) {
    if (this.selectedItem && model.id !== this.selectedItem.id) {
      this.selectedItem = model;
      this.selectedItemChange.emit(model);
    }
  }

  private _uniqueName: string;
  get uniqueName() {
    this._uniqueName = this._uniqueName || Math.random().toString(36).substring(2);
    return this._uniqueName;
  }
}

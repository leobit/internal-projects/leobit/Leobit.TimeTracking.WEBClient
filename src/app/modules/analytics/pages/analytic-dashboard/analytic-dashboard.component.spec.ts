import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticDashboardComponent } from './analytic-dashboard.component';

describe('AnalyticDashboardComponent', () => {
  let component: AnalyticDashboardComponent;
  let fixture: ComponentFixture<AnalyticDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { delay, of, Subject, Subscription, takeUntil } from 'rxjs';
import { TimeReport, TimeReportIssue, TimeReportIssueType } from 'src/app/core/api/models';
import { AnalyticService } from 'src/app/core/api/services';

export interface IssueTypeShortcut {
  key: TimeReportIssueType;
  icon: string;
  label: string;
}

@Component({
  selector: 'app-analytic-dashboard',
  templateUrl: './analytic-dashboard.component.html',
  styleUrls: ['./analytic-dashboard.component.css']
})
export class AnalyticDashboardComponent implements OnInit {
  public issues: TimeReportIssue[] = [];
  public allIssues: TimeReportIssue[] = [];
  
  public issueTypes: IssueTypeShortcut[] = [
    { key: TimeReportIssueType.DuplicateContent, icon: 'pi pi-copy', label: 'Duplicates' },
    { key: TimeReportIssueType.ShortContent, icon: 'pi pi-ellipsis-h', label: 'Short' },
    { key: TimeReportIssueType.SickleaveAbuse, icon: 'pi pi-heart', label: 'Sick abuse' },
    { key: TimeReportIssueType.SelfEducation, icon: 'pi pi-user-plus', label: 'Self Education' },
    { key: TimeReportIssueType.Ke, icon: 'pi pi-users', label: 'KE' },
    { key: TimeReportIssueType.Holidays, icon: 'pi pi-calendar-times', label: 'Holidays' },
    { key: TimeReportIssueType.Inactive, icon: 'pi pi-user-minus', label: 'Inactive' },
  ]
  public selectedIssueTypes: string [] = this.issueTypes.map(it => it.key);
  public isLoading: boolean = false;
  public selectedMonth: Date = new Date();
  public currentYear: number = new Date().getFullYear();
  public currentMonth: number = new Date().getMonth();
  private subs = new Subscription();

  monthChange = new Subject();
  monthChangeDebounce = {
    getData: () => of({ id: 1 }).pipe(delay(500)),
  };

  constructor(
    private analyticService: AnalyticService
  ) { }

  ngOnInit() {
    this.getIssues();
  }

  private getIssues(): void {
    this.isLoading = false;
    this.subs.unsubscribe();
    const year = this.selectedMonth.getFullYear();
    const month = this.selectedMonth.getMonth() + 1;
    this.isLoading = true;
    this.subs = (this.analyticService.getAllIssues({ year: year, month: month }).subscribe(res => {
      this.allIssues = res.map((item, index) => ({ 
        ...item, 
        userFullName: `${item.user.firstName} ${item.user.lastName}`, 
        index: item.issueType + index,
        hours: parseFloat(this.getTotalHours(item.reports).split(' / ')[0])
      }));
      this.issues = this.filterIssues(this.allIssues, this.selectedIssueTypes);
      this.isLoading = false;
    }, _ => {
      this.isLoading = false;
    }));
  }

  private filterIssues(allIssues: TimeReportIssue[], selectedIssueTypes: string []): TimeReportIssue[] {
    return allIssues.filter(i => selectedIssueTypes.indexOf(i.issueType) != -1);
  }

  public isIssueTypeSelected(issueType: string): boolean {
    return this.selectedIssueTypes.indexOf(issueType) != -1;
  }

  public toggleIssueType(issueType: string): void {
    if (!this.isIssueTypeSelected(issueType)){
      this.selectedIssueTypes.push(issueType);
    } else {
      this.selectedIssueTypes = this.selectedIssueTypes.filter(i => i != issueType);
    }
    this.issues = this.filterIssues(this.allIssues, this.selectedIssueTypes);
  }

  public toggleAllIssueType(): void {
    if (this.selectedIssueTypes.length == 0 
        || this.selectedIssueTypes.length != this.issueTypes.length) {
      this.selectedIssueTypes = this.issueTypes.map(it => it.key);
    } else {
      this.selectedIssueTypes = [];
    }
    this.issues = this.filterIssues(this.allIssues, this.selectedIssueTypes);
  }

  public getTotalHours(reports: TimeReport[]): string {
    let int = 0;
    let ext = 0;
    reports.forEach(r => {
      int += r.internalHours;
      ext += r.externalHours;
    });
    return `${int} / ${ext}`;
  }

  public nextMonth(): void {
    this.selectedMonth.setMonth(this.selectedMonth.getMonth() + 1);
    this.monthChange.next(true);
    this.monthChangeDebounce.getData().pipe(
      takeUntil(this.monthChange),
    ).subscribe(_ => {
      this.getIssues();
    });
  }

  public prevMonth(): void {
    this.selectedMonth.setMonth(this.selectedMonth.getMonth() -1);
    this.monthChange.next(true);
    this.monthChangeDebounce.getData().pipe(
      takeUntil(this.monthChange),
    ).subscribe(_ => {
      this.getIssues();
    });
  }

  public calcIssueTypeCount(issueType: string): number {
    return this.allIssues.filter(i => i.issueType == issueType).length;
  }

  public getIssueTypeLabel(key: string) {
    return this.issueTypes.find(it => it.key == key)?.label || "None";
  }

}

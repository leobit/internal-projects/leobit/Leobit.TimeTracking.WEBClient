import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticDashboardComponent } from './pages/analytic-dashboard/analytic-dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { ProgressBarModule } from 'primeng/progressbar';
import { BadgeModule } from 'primeng/badge';
import { ToggleButtonModule } from 'primeng/togglebutton';

const analyticsRoutes: Routes = [
  {
    path: '',
    component: AnalyticDashboardComponent,
    canActivate: [ AuthGuard ],
    resolve: { }
  }
];

@NgModule({
  declarations: [AnalyticDashboardComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(analyticsRoutes),
    InputTextModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    KeyFilterModule,
    FieldsetModule,
    CheckboxModule,
    SliderModule,
    DialogModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    BadgeModule,
    ToggleButtonModule
  ]
})
export class AnalyticsModule { }

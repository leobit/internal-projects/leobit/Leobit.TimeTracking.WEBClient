import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Slider } from 'primeng/slider';
import { SelectItem, MessageService, SortEvent, FilterMetadata } from 'primeng/api';
import { Account, AccountType, ClosingType, UserShort } from 'src/app/core/api/models';
import { AccountService } from 'src/app/core/api/services';
import { TabPermissionsService } from 'src/app/core/services/tab-permissions-service';
import { Table } from 'primeng/table';
import { UserProfileService } from 'src/app/core/services/user-profile.service';
import { firstValueFrom } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ClosingTypeService } from 'src/app/core/services/closing-type-service';
import { userToStringWithMail } from 'src/app/core/helpers/user.helper';
import { noWhiteSpacesValidator } from 'src/app/shared/validators/no-whitespaces-validator';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit, AfterViewInit {
  @ViewChild("internalSlider", { static: true })
  internalSlider: Slider;

  @ViewChild("externalSlider", { static: true })
  externalSlider: Slider;

  @ViewChild('dt') dataTable!: Table;
  dialogVisible: boolean = false;
  showInActiveAccounts: boolean = false;
  currentSort: 'none' | 'ascending' | 'descending' = 'none';
  closingTypes: SelectItem[] ;
  accountTypes: SelectItem[] = [
    { label: "External", value: AccountType.External },
    { label: "Internal", value: AccountType.Internal }
  ];
  editAccount: Account = {};
  accounts: Account[] = [];

  eligibleBudgetOwners: UserShort[] = [];
  defaultBudgetOwnerId: string;

  budgetOwnerSelectItems: SelectItem<string>[] = [];

  formLoaded = false;

  form = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(100), noWhiteSpacesValidator()]),
    beforeClosing: new FormControl({ value: 0, disabled: true }, [Validators.required, Validators.min(0), Validators.max(366)]),
    afterClosing: new FormControl({ value: 0, disabled: true }, [Validators.required, Validators.min(0), Validators.max(366)]),
    enableClosingControls: new FormControl<boolean>(false)
  });

  get f() {
    return this.form.controls;
  }

  get hasWritePermission() {
    return this.tabPermissionsService.hasWriteClaim("Account");
  }

  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService,
    private messageService: MessageService,
    private userProfileService: UserProfileService,
    private tabPermissionsService: TabPermissionsService,
    public closingTypeService: ClosingTypeService
  ) { }

  ngOnInit() {
    this.accounts = <Account[]>this.route.snapshot.data.preloaded;
  }

  ngAfterViewInit() {
    this.dataTable.filter('true', 'isActive', 'equals');
  }

  async onAddAccount() {
    const currentUserId = this.userProfileService.userProfile.sub;
    this.editAccount = {
      id: 0,
      name: 'Account name',
      budgetOwnerId: currentUserId,
      overrideHoursSettings: false,
      minInternal: 0,
      maxInternal: 12,
      minExternal: 0,
      maxExternal: 12,
      overrideClosingSettings: false,
      closingType: ClosingType.Monthly,
      beforeClosing: 0,
      afterClosing: 0,
      accountType: AccountType.External
    };

    this.accountTypes = this.accountTypes.filter(type => type.value !== AccountType.Operational);

    this.loadClosingTypes();
    await this.initEditDialog();

    this.editAccount.budgetOwnerId = this.eligibleBudgetOwners.some(bo => bo.id === currentUserId) ? currentUserId : this.defaultBudgetOwnerId
  }

  async onEditAccount(account: Account) {
    this.editAccount = { ...account };

    this.accountTypes = this.accountTypes.filter(type => type.value !== AccountType.Operational);

    if (this.editAccount.accountType === AccountType.Operational) {
      this.accountTypes.push({ label: "Operational", value: AccountType.Operational });
    }

    this.form.patchValue({enableClosingControls: this.editAccount.overrideClosingSettings})
    this.form.get('enableClosingControls').valueChanges.subscribe(enable => {
      this.toggleClosingSettings(enable);
    });

    this.loadClosingTypes();
    await this.initEditDialog();
  }

  toggleClosingSettings(enable: boolean) {
    if (enable) {
      this.form.get('beforeClosing').enable();
      this.form.get('afterClosing').enable();
    } else {
      this.form.get('beforeClosing').disable();
      this.form.get('afterClosing').disable();
    }
  }

  loadClosingTypes(){
    this.closingTypes = this.closingTypeService.getEnabledTypes(this.editAccount.closingType);
  }

  supportsClosingType(closingType) { return this.closingTypeService.supportsWindowExtension(closingType); }

  onSaveAccount() {
    const editAccount = this.editAccount;
    if (editAccount.id == 0) {
      this.accountService.createAccount({ body: editAccount }).subscribe(account => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Account '${account.name}' created` });
        this.accounts = [account].concat(this.accounts.slice());
        this.filterGlobally();
      });
    } else {
      this.accountService.editAccount({ body: editAccount }).subscribe(account => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Account '${account.name}' updated` });
        const i = this.accounts.findIndex(x => x.id === editAccount.id);

        if (this.currentSort === "none"){
          this.accounts[i] = editAccount;
        } else {
          if (i !== -1) {
            this.accounts.splice(i, 1);
          }
          this.accounts = [editAccount].concat(this.accounts.slice());
        }
        this.filterGlobally();
      });
    }
    this.hideDialog();
  }

  filterGlobally() {
    const filterGlobal = this.dataTable.filters['global'] as FilterMetadata;
    this.dataTable.filterGlobal(filterGlobal?.value, 'contains');
  }

  onInternalHoursChange(values: number[]) {
    this.editAccount.minInternal = values[0] / 2;
    this.editAccount.maxInternal = values[1] / 2;
  }

  onExternalHoursChange(values: number[]) {
    this.editAccount.minExternal = values[0] / 2;
    this.editAccount.maxExternal = values[1] / 2;
  }

  customSort(event: SortEvent) {
    if (event.order === 1) {
      this.currentSort = "ascending";
      this.accounts.sort((a, b) => a.name.localeCompare(b.name));
    }
    if (event.order === -1) {
      this.currentSort = "descending";
      this.accounts.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase() ? -1 : 1));
    }
  }

  resetSort() {
    this.currentSort = "none";
    this.accounts.sort((a, b) => b.id - a.id);
  }

  private async initEditDialog() {
    this.showDialog();

    if (!this.formLoaded) {
      const eligibleBudgetOwnersInfo = await firstValueFrom(
        this.accountService.getEligibleBudgetOwners()
      );
      this.eligibleBudgetOwners = eligibleBudgetOwnersInfo.eligibleBudgetOwners;
      this.defaultBudgetOwnerId = eligibleBudgetOwnersInfo.defaultBudgetOwnerId;

      this.formLoaded = true;
    }

    this.setBudgetOwnerDropdownItems();
  }

  private setBudgetOwnerDropdownItems(){
    this.budgetOwnerSelectItems = this.eligibleBudgetOwners.map<SelectItem<string>>(
      (user: UserShort) => ({
        label: userToStringWithMail(user),
        value: user.id,
      }))
        .sortBy((item: SelectItem<string>) => item.label);
  }

  private showDialog() {
    this.dialogVisible = true;
    // handle sliders range mode
    if (this.editAccount.minInternal == 0 && this.editAccount.maxInternal == 0) {
      this.internalSlider.handleIndex = 1;
    }
    if (this.editAccount.minExternal == 0 && this.editAccount.maxExternal == 0) {
      this.externalSlider.handleIndex = 1;
    }
  }

  private hideDialog() {
    this.dialogVisible = false;
    this.editAccount = {};
    this.budgetOwnerSelectItems = [];
  }

  getEventValue($event: any): string {
    return $event.target.value;
  }
}



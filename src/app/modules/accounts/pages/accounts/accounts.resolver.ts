import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountService } from 'src/app/core/api/services';
import { Account } from 'src/app/core/api/models';



@Injectable()
export class AccountsResolver  {
    constructor(private accountService: AccountService) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Account[]> {
        return this.accountService.getAccounts();
    }
}

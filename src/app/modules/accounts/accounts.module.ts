import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AccountsComponent } from './pages/accounts/accounts.component';
import { AccountsResolver } from './pages/accounts/accounts.resolver';

import { AuthGuard } from 'src/app/core/guards/auth.guard';

import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { DialogModule } from 'primeng/dialog';
import { InputNumberModule } from 'primeng/inputnumber';
import { SharedModule } from 'src/app/shared/shared.module';

const accountsRoutes: Routes = [
  {
    path: '',
    component: AccountsComponent,
    canActivate: [ AuthGuard ],
    resolve: { preloaded: AccountsResolver }
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(accountsRoutes),
    InputTextModule,
    InputNumberModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    FieldsetModule,
    CheckboxModule,
    SliderModule,
    DialogModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [AccountsComponent],
  providers: [AccountsResolver]
})
export class AccountsModule { }

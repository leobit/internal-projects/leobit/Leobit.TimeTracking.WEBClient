import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd, NavigationCancel } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';
import { MenuItem, PrimeNGConfig } from 'primeng/api';
import { TabMenu } from 'primeng/tabmenu';
import { authConfig } from './auth.config';
import { UserProfileService } from './core/services/user-profile.service';
import { TabPermissionsService } from './core/services/tab-permissions-service';
import { Env,  User } from 'ngx-leobit-components/lib/models';
import { environment } from 'src/environments/environment';
import { HttpStatus } from 'src/app/core/interceptors/loader-http-interceptor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(TabMenu) menu: TabMenu;
  menuItems: MenuItem[] = [];
  lastActiveItem: MenuItem;
  loggedIn: boolean = false;
  user: User | null = {
    name: '',
    picture: 'https://leobit.com/content/themes/leobit-wp-theme/images/lion-new.svg',
  };

  env: Env = environment.production ? "prod" :"test";
  requestInProgress: boolean;

  constructor(
    private oauthService: OAuthService,
    private router: Router,
    private userProfileService: UserProfileService,
    private tabPermissionsService: TabPermissionsService,
    private primengConfig: PrimeNGConfig,
    private httpStatus: HttpStatus
  ) {
    this.configureWithNewConfigApi();
  }

  ngOnInit() {
    this.lastActiveItem = this.menuItems[0];
    this.primengConfig.ripple = true;

    this.httpStatus.getHttpStatus().subscribe((requestInProgress: boolean) => {
      this.requestInProgress = requestInProgress;
    });
  }

  private configureWithNewConfigApi() {
    var redirectPath = window.location.pathname;

    const allowedRedirectPaths = [
      '/reporting/daily',
      '/reporting/statistic',
      '/reporting/monthly',
      '/reporting/vacation-and-sick-leave',
      '/employee-vacation-and-sick-leave',
      '/reserve',
      '/division-hierarchy',
      '/accounts',
      '/projects',
      '/projectassignment',
      '/project-manager-assignment',
      'role-assignment',
      '/settings',
      '/employeereporting'
    ];

    if (!allowedRedirectPaths.includes(redirectPath) && !redirectPath.includes('/approve-reopen-reporting/')) {
      redirectPath = '/reporting/daily';
    }

    this.oauthService.configure({ ...authConfig, redirectUri: window.location.origin + redirectPath });
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin().then(_ => {
      if (this.oauthService.hasValidAccessToken()) {
        this.loggedIn = true;
        this.oauthService.events.subscribe((event) => {
          if (event.type == 'silently_refreshed') {
            this.loadUserProfileAndUpdateUI();
          }
        })
        this.loadUserProfileAndUpdateUI();
        this.oauthService.setupAutomaticSilentRefresh();
        this.router.initialNavigation();
      } else {
        this.oauthService.initCodeFlow();
      }
    });
  }

  loadUserProfileAndUpdateUI() {
    this.oauthService.loadUserProfile().then((userProfile: any) => {
      this.userProfileService.updateUserProfile(userProfile.info);
      this.userProfileService.userProfile = userProfile.info;
      this.user = {
        name: userProfile.info?.name,
        picture: userProfile.info?.picture,
      };

      this.menuItems = [
        { label: 'Daily', routerLink: '/reporting/daily', icon: 'fa fa-calendar-check-o' },
        { label: 'Statistic', routerLink: '/reporting/statistic', icon: 'fa fa-bar-chart' },
        { label: 'Monthly', routerLink: '/reporting/monthly', icon: 'fa fa-calendar' }
      ];
      const tabPermissions = this.tabPermissionsService.getPermissions(userProfile);

      if (tabPermissions.canSeeEmployeeVacationAndSickLeave) {
        this.menuItems.push({ label: 'Vacation & Sick leave', routerLink: '/employee-vacation-and-sick-leave', icon: 'fa fa-life-ring' });
      } else {
        this.menuItems.push({ label: 'Vacation & Sick leave', routerLink: '/reporting/vacation-and-sick-leave', icon: 'fa fa-life-ring' });
      }
      if (tabPermissions.canSeeReserve) {
        this.menuItems.push({ label: 'Reserve', routerLink: '/reserve', icon: 'fa fa-users' });
      }
      if (tabPermissions.canSeeDivisionHierarchy) {
        this.menuItems.push({ label: 'Division Hierarchy', routerLink: '/division-hierarchy', icon: 'fa fa-sitemap' });
      }
      if (tabPermissions.canSeeAccounts) { this.menuItems.push({ label: 'Accounts', routerLink: '/accounts', icon: 'fa fa-briefcase' }); }
      if (tabPermissions.canSeeProjects) { this.menuItems.push({ label: 'Projects', routerLink: '/projects', icon: 'fa fa-tasks' }); }
      if (tabPermissions.canSeeProjectAssignmentReport) {
        this.menuItems.push({ label: 'Project Assignment', routerLink: '/projectassignment', icon: 'fa fa-user-plus' });
        this.menuItems.push({ label: 'Project Managers', routerLink: '/project-manager-assignment', icon: 'fa fa-user-plus' });
      }
      if (tabPermissions.canSeeRoleAssignment) { this.menuItems.push({ label: 'Role Assignment', routerLink: '/role-assignment', icon: 'fa fa-user-plus' }); }
      if (tabPermissions.canSeeEmployeeReporting) { this.menuItems.push({ label: 'Employee Reporting', routerLink: '/employeereporting', icon: 'fa fa-clock-o' }); }
      if (tabPermissions.canSeeEmployeeReporting) { this.menuItems.push({ label: 'Analytics', routerLink: '/analytics', icon: 'fa fa-pie-chart' }); }
      if (tabPermissions.canSeeSettings) { this.menuItems.push({ label: 'Settings', routerLink: '/settings', icon: 'fa fa-wrench' }); }
    });
  }

  signOut() {
    this.oauthService.logOut();
  }

  themeChange(theme: string) {
    document.documentElement.setAttribute('data-theme', theme);
  }
}

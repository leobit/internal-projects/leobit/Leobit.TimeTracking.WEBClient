import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { MessageService } from 'primeng/api';
 
@Injectable()
export class AuthGuard  {

    constructor(private oauthService: OAuthService, private messageService: MessageService) {        
    }

    // dont use this.oauthService.hasValidAccessToken() because of 5 minutes skew
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        var hasValidAccessToken = !!this.oauthService.getAccessToken();

        if (hasValidAccessToken) {
            const now = new Date();
            var accessTokenExpiration = this.oauthService.getAccessTokenExpiration();
                                                              // + 5 minutes skew
            if (accessTokenExpiration && ((accessTokenExpiration + 5 * 60 * 1000) < now.getTime())) {
                hasValidAccessToken = false;
            }
        }

        if (!hasValidAccessToken) {
            this.messageService.add({ severity: 'error', summary: 'Unauthorized'});
        }

        return hasValidAccessToken;
    }
}
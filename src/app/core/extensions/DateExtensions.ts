interface DateConstructor {
  CreateUTCDate(date: Date): Date;
  CreateDateTime(date: Date, time: Date): Date;
  ParseISOString(date: string): Date;
}

Date.CreateUTCDate = function (date: Date): Date {
  return new Date(
    Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())
  );
};

Date.CreateDateTime = function (date: Date, time: Date): Date {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate(), time.getHours(), time.getMinutes(), 0, 0);
}

interface Date {
  getDayOfWeek(): number;
}

interface String {
  parseISOStringToLocaleDateString(): string;
}

Date.prototype.getDayOfWeek = function (): number {
  var mondayFisrt = true;
  var dayOfWeek = this.getDay();
  if (mondayFisrt) {
    dayOfWeek = (dayOfWeek + 6) % 7;
  }

  return dayOfWeek;
};

String.prototype.parseISOStringToLocaleDateString = function (): string {
  return Date.ParseISOString(this).toLocaleDateString();
};

Date.ParseISOString = function (date: string): Date {
  const year = Number(date.substring(0, 4));
  const month = Number(date.substring(5, 7)) - 1; // Month is zero-based
  const day = Number(date.substring(8, 10));
  const hour = Number(date.substring(11, 13));
  const minute = Number(date.substring(14, 16));
  const second = Number(date.substring(17, 19));
  return new Date(year, month, day, hour, minute, second);
}

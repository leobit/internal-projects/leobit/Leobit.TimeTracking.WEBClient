interface Array<T> {
    sortBy(fieldSelector: (item: T) => any): this;
    groupBy<TKey>(keySelector: (item: T) => TKey, keyEqualityComparer?: (a: TKey, b: TKey) => boolean): { key: TKey, value: T[] }[];
}

Array.prototype.sortBy = function(fieldSelector) {
    return this.sort((a, b) => {
        var aField = fieldSelector(a);
        var bField = fieldSelector(b);

        return aField < bField ? -1 :
               aField > bField ? 1 :
               0;
    });
};

Array.prototype.groupBy = function(keySelector, keyEqualityComparer) {
    keyEqualityComparer = keyEqualityComparer || ((a, b) => a === b);

    var result = [];

    this.forEach(item => {
        var key = keySelector(item);
        var resultItem = result.find(ri => keyEqualityComparer(ri.key, key));
        if (!resultItem) {
            resultItem = { key: key, value: [] };
            result.push(resultItem);
        }
        resultItem.value.push(item);
    });

    return result;
};
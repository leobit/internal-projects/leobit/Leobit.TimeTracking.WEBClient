import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { ClaimValue } from '../models/claim-value';
import { PermissionsModel } from '../models/permissions-model';
import { UserProfileService } from './user-profile.service';

@Injectable({
  providedIn: 'root'
})
export class TabPermissionsService {

  constructor(
    private oauthService: OAuthService, private userprofileService: UserProfileService
  ) { }

  getPermissions(userProfile: any): PermissionsModel {
    return {
      canSeeEmployeeVacationAndSickLeave: userProfile.info.EmployeeVacationAndSickLeave === 1 
      || userProfile.info.EmployeeVacationAndSickLeave === 3,
      canSeeReserve: userProfile.info.EmployeeVacationAndSickLeave === 2
      || userProfile.info.EmployeeVacationAndSickLeave === 3,
      canSeeVacationAdministration: userProfile.info.EmployeeVacationAndSickLeave === 2
      || userProfile.info.EmployeeVacationAndSickLeave === 3,
      canSeeDivisionHierarchy: userProfile.info.Division === 1 || userProfile.info.Division === 3 || userProfile.info.OrganizationUnit === 1 
      || userProfile.info.OrganizationUnit === 3 || userProfile.info.Category === 1 || userProfile.info.Category === 3,
      canSeeAccounts: userProfile.info.Account === 1 || userProfile.info.Account === 3,
      canSeeProjects: userProfile.info.Project === 1 || userProfile.info.Project === 3,
      canSeeProjectAssignmentReport: userProfile.info.ProjectAssignment === 1 || userProfile.info.ProjectAssignment === 3,
      canSeeRoleAssignment: userProfile.info.Role === 1 || userProfile.info.Role === 3,
      canSeeEmployeeReporting: userProfile.info.TimeReport === 1 || userProfile.info.TimeReport === 3
        || userProfile.info.TeamTimeReport === 1 || userProfile.info.TeamTimeReport === 3,
      canSeeSettings: userProfile.info.Settings === 1 || userProfile.info.Settings === 3
    };
  }

  hasWriteClaim(claim: string): boolean {
    const claimValue = this.userprofileService.userProfile[claim];
    return claimValue
      ? (claimValue & ClaimValue.Write) === ClaimValue.Write : false;
  }
}
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  public userProfile: any;
  public userProfile$: Observable<any>;
  private userProfileSubject: BehaviorSubject<any>;

  constructor() {
    this.userProfileSubject = new BehaviorSubject<any>(null);
    this.userProfile$ = this.userProfileSubject.asObservable();
  }

  updateUserProfile(profile: any) {
    this.userProfileSubject.next(profile);
  }

}

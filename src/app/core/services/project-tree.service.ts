import { TreeNode } from 'primeng/api';
import { ProjectShort, UserTimeReport } from 'src/app/core/api/models';
import { MeService } from 'src/app/core/api/services';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectTreeService {

  constructor(private meService: MeService) { }

  getTimeReports(
    filterProjects: TreeNode[],
    selectedProjects: TreeNode[],
    projects: ProjectShort[],
    firstDate,
    lastDate,
    isOverhead,
  ) {
    const nothingSelected = selectedProjects.length === 0 && projects.length !== 0;

    if (nothingSelected) {
      return of(<UserTimeReport[]>[]);
    }

    const projectIds = selectedProjects
        .filter(node => !node.children)
        .map(node => node.data);

    return this.meService.getMyTimeReports({
      dateFrom: Date.CreateUTCDate(firstDate).toISOString(),
      dateTo: Date.CreateUTCDate(lastDate).toISOString(),
      projectIds: projectIds,
      isOverhead: isOverhead,
    }).pipe(map(userTimeReports =>
      userTimeReports.map<UserTimeReport>((utr) => ({
        ...utr,
        project:
          projects.find((p) => p.id === utr.project.id) || utr.project,
      }))));
  }
}

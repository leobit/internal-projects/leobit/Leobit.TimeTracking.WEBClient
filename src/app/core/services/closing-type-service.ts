import { Injectable } from "@angular/core";
import { ClosingType } from "../api/models";
import { SelectItem } from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class ClosingTypeService {

  constructor(
  ) { }

  private closingTypes: SelectItem[] = [
    { label: "Monthly (legacy)", value: ClosingType.Monthly },
    { label: "Weekly (legacy)", value: ClosingType.Weekly },
    { label: "Daily (legacy)", value: ClosingType.Daily },
    { label: "Monday 12:00PM", value: ClosingType.Monday },
    { label: "Monday & Thursday 12:00PM", value: ClosingType.MondayThursday}
  ];

  private supportsWindowExtesnion: Record<ClosingType, boolean> = {
    [ClosingType.Daily]: true,
    [ClosingType.Weekly]: true,
    [ClosingType.Monthly]: true,
    [ClosingType.Monday]: false,
    [ClosingType.MondayThursday]: false
  };

  supportsWindowExtension(closingType: ClosingType) {
     return this.supportsWindowExtesnion[closingType];
    }

  getEnabledTypes(current: ClosingType): SelectItem[] {
    return this.closingTypes.filter(item =>
      !this.supportsWindowExtesnion[item.value] || item.value === current);
   }

}

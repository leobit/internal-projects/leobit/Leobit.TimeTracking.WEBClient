import {ProjectShort} from "../api/models";

export function getIsOperational(project: ProjectShort): boolean {
  return project && (
    project.projectType === 2 ||
    project.projectType === 3 ||
    project.projectType === 4 ||
    project.projectType === 5
  );
}

export function getIsExternal(project: ProjectShort): boolean {
  return project && project.projectType === 0;
}

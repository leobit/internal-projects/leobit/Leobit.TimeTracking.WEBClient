import { FormGroup } from "@angular/forms"

export function sanitizeFormInput(event: Event, formControlName: string, formGroup: FormGroup, pattern: RegExp): void {
    const inputElement = event.target as HTMLInputElement;
    const sanitizedValue = inputElement.value.replace(pattern, '');
    formGroup.get(formControlName)?.setValue(sanitizedValue);
}

export function showValidationErrors(fieldName: string, form: FormGroup): boolean {
    const control = form.controls?.[fieldName];
    const isDirty = control?.dirty;
    const hasErrors = control?.errors != null;

    return isDirty && hasErrors;
}
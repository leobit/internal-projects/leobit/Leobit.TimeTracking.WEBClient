import { UserShort } from "../api/models";

export function userToStringWithMail(user: UserShort) {
  return `${user.firstName} ${user.lastName} (${user.id}@leobit.com)`;
}

export function userToString(user: UserShort) {
  return `${user.firstName} ${user.lastName}`;
}

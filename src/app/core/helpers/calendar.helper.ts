export function calculateRange(start: Date, end: Date): Date[] {
    if (start.getTime() === end.getTime()) {
      return [start];
    }
    const range = Array.from(
      {
        length: Math.ceil(
          (end.getTime() - start.getTime()) / 1000 / 60 / 60 / 24 + 1
        ),
      },
      (e, i) => {
        let date = new Date(start);
        date.setDate(start.getDate() + i);
        return date;
      }
    );
    return range;
  }

export function getDatesBetweenRanges(dateRanges: Date[][]): Date[] {
  const dates: Date[] = [];

  if (dateRanges.length < 2) {
    return dates;
  }

  const endDate = dateRanges[0][0] >= dateRanges[1][0] ? dateRanges[0][0] : dateRanges[1][0];
  const startDate = dateRanges[0][1] <= dateRanges[1][1] ? dateRanges[0][1] : dateRanges[1][1];

  if (startDate >= endDate) {
    return dates;
  }

  let currentDate = new Date(startDate);
  currentDate.setDate(currentDate.getDate() + 1);

  while (currentDate < endDate) {
    dates.push(new Date(currentDate));
    currentDate.setDate(currentDate.getDate() + 1);
  }

  return dates;
}
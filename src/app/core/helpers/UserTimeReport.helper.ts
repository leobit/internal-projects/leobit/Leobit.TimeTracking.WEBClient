import { UserTimeReport } from "../api/models";

export function isTimeReportEditable(report: UserTimeReport): boolean {
  if (!report.project.isClosed && report.project.restriction) {
    const reportingDate = new Date(report.reportingDate);

    const allowedDateRanges = report.project.restriction.allowedDateRanges.map(r =>
      [new Date(r.startDate), new Date(r.endDate)]);

    return (
      reportingDate.getTime() >= allowedDateRanges[0][0].getTime() &&
      reportingDate.getTime() <= allowedDateRanges[0][1].getTime() ||
      (allowedDateRanges[1] &&
        (reportingDate.getTime() >= allowedDateRanges[1][0].getTime() &&
          reportingDate.getTime() <= allowedDateRanges[1][1].getTime()))
    );
  } else {
    return false;
  }
}

export class PermissionsModel {
    canSeeEmployeeVacationAndSickLeave: boolean;
    canSeeReserve: boolean;
    canSeeVacationAdministration: boolean;
    canSeeDivisionHierarchy: boolean;
    canSeeAccounts: boolean;
    canSeeProjects: boolean;
    canSeeProjectAssignmentReport: boolean;
    canSeeRoleAssignment: boolean;
    canSeeEmployeeReporting: boolean;
    canSeeSettings: boolean;
}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { ApiModule } from './api/api.module';
import { MessageService, ConfirmationService } from 'primeng/api';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHttpInterceptor } from './interceptors/error-http-interceptor';
import { HttpStatus, LoaderHttpInterceptor } from './interceptors/loader-http-interceptor';

import { AuthGuard } from './guards/auth.guard';

import { Globals } from './globals';

import './extensions/DateExtensions';
import './extensions/ArrayExtensions';
import { environment } from 'src/environments/environment';

@NgModule({
  imports: [
    CommonModule,
    ApiModule.forRoot({ rootUrl: environment.apiUrl }),
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [environment.apiUrl],
        sendAccessToken: true
      }
    })
  ],
  exports: [ OAuthModule ],
  declarations: [],
  providers: [
    MessageService,
    ConfirmationService,
    AuthGuard,
    HttpStatus,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderHttpInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHttpInterceptor,
      multi: true
    },
    {
      provide: OAuthStorage,
      useFactory: () => localStorage
    },
    Globals
  ]
})
export class CoreModule { }

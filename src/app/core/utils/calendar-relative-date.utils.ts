export abstract class CalendarRelativeDateUtils {
  static getTodayRange() {
    const nowDate = new Date();
    return [nowDate, nowDate];
  }

  static getCurrentWeekRange() {
    const nowDate = new Date();
    return [
      new Date(nowDate.setDate(nowDate.getDate() - nowDate.getDay() + 1)),
      new Date(nowDate.setDate(nowDate.getDate() - nowDate.getDay() + 7))
    ];
  }

  static getLastWeekRange() {
    const nowDate = new Date();
    const lastWeekDate = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate() - 7);
    return [
      new Date(lastWeekDate.setDate(lastWeekDate.getDate() - lastWeekDate.getDay() + 1)),
      new Date(lastWeekDate.setDate(lastWeekDate.getDate() - lastWeekDate.getDay() + 7))
    ];
  }

  static getCurrentMonthRange() {
    const nowDate = new Date();
    return [
      new Date(nowDate.getFullYear(), nowDate.getMonth(), 1),
      new Date(nowDate.getFullYear(), nowDate.getMonth() + 1, 0)
    ];
  }

  static getLastMonthRange() {
    const nowDate = new Date();
    return [
      new Date(nowDate.getFullYear(), nowDate.getMonth() - 1, 1),
      new Date(nowDate.getFullYear(), nowDate.getMonth(), 0)
    ];
  }

  static getCurrentYearRange() {
    const nowDate = new Date();
    return [
      new Date(nowDate.getFullYear(), 0, 1),
      new Date(nowDate.getFullYear() + 1, 0, 0)
    ];
  }

  static getLastYearRange() {
    const nowDate = new Date();
    return [
      new Date(nowDate.getFullYear() - 1, 0, 1),
      new Date(nowDate.getFullYear(), 0, 0)
    ];
  }
}
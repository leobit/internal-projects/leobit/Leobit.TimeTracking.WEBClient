import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MessageService } from 'primeng/api';

@Injectable()
export class ErrorHttpInterceptor implements HttpInterceptor {

    constructor(private messageService: MessageService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(catchError((error) => {
            if (error instanceof HttpErrorResponse) {
                this.handleHttpErrorResponse(error);
            }
            return throwError(error);
        }) as any);
    }

    handleHttpErrorResponse(error: HttpErrorResponse) {
      let errorMessage = error.error;

      if (error.error instanceof Blob) {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          const text = event.target.result;
          errorMessage = text;
          this.messageService.add({severity: "error", summary: "Error", detail: errorMessage});
        };
        reader.readAsText(error.error);
      } else {
        this.messageService.add({severity: "error", summary: "Error", detail: errorMessage});
      }
    }
}

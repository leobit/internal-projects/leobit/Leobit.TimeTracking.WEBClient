import { Observable, BehaviorSubject, interval } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { map, catchError, finalize, delay, debounce } from 'rxjs/operators';

@Injectable()
export class HttpStatus {

    private requests: Set<string>;
    private requestInProgress: BehaviorSubject<boolean>;
    
    constructor() {
        this.requests = new Set<string>();
        this.requestInProgress = new BehaviorSubject(false);
    }

    addRequest(url: string) {
        if(this.requests.size == 0) {
            this.requestInProgress.next(true);
        }
        this.requests.add(url);
    }

    deleteRequest(url: string) {
        if(this.requests.size == 1 && this.requests.has(url)) {
            this.requestInProgress.next(false);
        }
        this.requests.delete(url);
    }
  
    getHttpStatus(): Observable<boolean> {
        return this.requestInProgress.asObservable().pipe(debounce(value => interval(value ? 0 : 500)));
    }
}

@Injectable()
export class LoaderHttpInterceptor implements HttpInterceptor {
    constructor(private status: HttpStatus) { }
  
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            map(event => {
                this.status.addRequest(request.urlWithParams);
                return event;
            }),
            catchError(error => {
                throw new Error(error);
            }),
            finalize(() => {
                this.status.deleteRequest(request.urlWithParams);
            }));
    }
}
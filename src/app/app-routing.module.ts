import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'reporting',
    loadChildren: () => import('./modules/reporting/reporting.module').then(m => m.ReportingModule),
  },
  {
    path: 'employee-vacation-and-sick-leave',
    loadChildren: () => import('./modules/employee-vacation-and-sick-leave/employee-vacation-and-sick-leave.module').then(m => m.EmployeeVacationAndSickLeaveModule)
  },
  {
    path: 'division-hierarchy',
    loadChildren: () => import('./modules/division-hierarchy/division-hierarchy.module').then(m => m.DivisionHierarchyModule)
  },
  {
    path: 'accounts',
    loadChildren: () => import('./modules/accounts/accounts.module').then(m => m.AccountsModule)
  },
  {
    path: 'projects',
    loadChildren: () => import('./modules/projects/projects.module').then(m => m.ProjectsModule)
  },
  {
    path: 'projectassignment',
    loadChildren: () => import('./modules/project-assignment/project-assignment.module').then(m => m.ProjectAssignmentModule)
  },
  {
    path: 'project-manager-assignment',
    loadChildren: () => import('./modules/project-manager-assignment/project-manager-assignment.module').then(m => m.ProjectManagerAssignmentModule)
  },
  {
    path: 'role-assignment',
    loadChildren: () => import('./modules/role-assignment/role-assignment.module').then(m => m.RoleAssignmentModule)
  },
  {
    path: 'employeereporting',
    loadChildren: () => import('./modules/employee-reporting/employee-reporting.module').then(m => m.EmployeeReportingModule)
  },
  {
    path: 'reserve',
    loadChildren: () => import('./modules/reserve/reserve.module').then(m => m.ReserveModule)
  },
  {
    path: 'analytics',
    loadChildren: () => import('./modules/analytics/analytics.module').then(m => m.AnalyticsModule)
  },
  {
    path: 'settings',
    loadChildren:  () => import('./modules/settings/settings.module').then(m => m.SettingsModule)
  },
  {
    path: '**',
    redirectTo: '/reporting/daily'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { initialNavigation: 'disabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
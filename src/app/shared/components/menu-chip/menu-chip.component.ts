import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { MenuItem } from "primeng/api";

@Component({
  selector: 'app-menu-chip',
  templateUrl: './menu-chip.component.html',
  styleUrls: ['./menu-chip.component.css']
})
export class MenuChipComponent implements OnInit {
  @Input() label: string;
  @Input() selected: boolean;
  @Input() menuItems: MenuItem[] = [];
  @Input() menuActive: boolean;

  @Output() chipClick = new EventEmitter();

  ngOnInit() {}

  selectFiltersPreset() {
    if (!this.selected) {
      this.chipClick.emit();
    }
  }
}

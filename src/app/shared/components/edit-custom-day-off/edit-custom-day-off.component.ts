import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Globals } from 'src/app/core/globals';
import { CustomDayOffDialog } from '../vacation/models/custom-day-off-dialog';
import { sanitizeFormInput, showValidationErrors } from 'src/app/core/helpers/reactive-forms.helper';

@Component({
  selector: 'app-edit-custom-day-off',
  templateUrl: './edit-custom-day-off.component.html',
  styleUrls: ['./edit-custom-day-off.component.css']
})
export class EditCustomDayOffComponent implements OnInit {
  @Input() customDayOffDialogModel: CustomDayOffDialog;
  @Output() customDayOffDialogModelChange = new EventEmitter<CustomDayOffDialog>();

  @Input() forbiddenMonths: number[] = [];

  @Output() onConfirm = new EventEmitter();

  editDate: Date;
  public autoResize: boolean = true;
  customDayOffForm: UntypedFormGroup;

  constructor(
    public globals: Globals,
  ) { }

  ngOnInit() {
    this.customDayOffForm = new UntypedFormGroup({
      extraDayOff: new UntypedFormControl({ value: this.customDayOffDialogModel.extraDayOff }, {
        validators: [
          this.nonZeroValidate(),
          Validators.required,
          Validators.min(1),
          Validators.max(10),
          Validators.pattern("^[0-9]*$")
        ]
      }),
      date: new UntypedFormControl({ value: this.customDayOffDialogModel.date, disabled: this.customDayOffDialogModel.action === 2, }, {
        validators: [
          Validators.required,
          this.forbiddenMonthsValidator(this.forbiddenMonths),
        ]
      }),
      description: new UntypedFormControl({ value: this.customDayOffDialogModel.description }, {
        validators: [
          Validators.required,
          Validators.minLength(3),
        ]
      })
    });

    this.customDayOffForm.setValue({
      date: this.customDayOffDialogModel.date,
      extraDayOff: this.customDayOffDialogModel.extraDayOff,
      description: this.customDayOffDialogModel.description
    })
  }

  onSave() {
    this.customDayOffDialogModel = {
      ...this.customDayOffDialogModel,
      date: this.customDayOffForm.get('date').value,
      extraDayOff: this.customDayOffForm.get('extraDayOff').value,
      description: this.customDayOffForm.get('description').value
    }

    this.customDayOffDialogModelChange.emit(this.customDayOffDialogModel);
    this.onConfirm.emit();
  }

  sanitizeExtraDayOffInput(event: Event): void {
    sanitizeFormInput(event, 'extraDayOff', this.customDayOffForm, /(^0)|([.,-])+/g);
  }

  nonZeroValidate(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.value == 0 ? { forbiddenValue: { value: control.value } } : null;
    }
  }

  forbiddenMonthsValidator(months: number[]): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return months.includes(new Date(control.value).getMonth()) ?
        { forbiddenMonths: { value: control.value } } : null;
    };
  }

  showCustomDayOffFormValidationErrors(fieldName: string): boolean {
    return showValidationErrors(fieldName, this.customDayOffForm);
  }
}

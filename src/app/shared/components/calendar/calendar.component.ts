import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { Calendar, LocaleSettings } from 'primeng/calendar';
import { DateRangeType, TimeUnit, TimeUnitState } from 'src/app/core/api/models';
import { Globals } from 'src/app/core/globals';
import { CalendarDateRange, RelativeTimeUnit } from 'src/app/shared/components/calendar/models/calendar-date-range';

interface RelativeDateRangeType {
  key: RelativeTimeUnit,
  label: string,
  class: string
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, OnChanges {
  @Input() readonly selectionMode = 'range';
  @Input() readonly inputStyle = { "width": "178px" };

  @Input() dateRange: CalendarDateRange;
  @Output() dateRangeChange = new EventEmitter<CalendarDateRange>();

  @ViewChild(Calendar) calendar: Calendar;

  dates: Date[];
  calendarLocale: LocaleSettings;

  relativeDateRangeTypes: RelativeDateRangeType[] = [
    { key: [TimeUnitState.Current, TimeUnit.Day], label: 'Today', class: 'calendar-button' },
    { key: [TimeUnitState.Current, TimeUnit.Week], label: 'This Week', class: 'calendar-button-half' },
    { key: [TimeUnitState.Last, TimeUnit.Week], label: 'Last Week', class: 'calendar-button-half' },
    { key: [TimeUnitState.Current, TimeUnit.Month], label: 'This Month', class: 'calendar-button-half' },
    { key: [TimeUnitState.Last, TimeUnit.Month], label: 'Last Month', class: 'calendar-button-half' },
    { key: [TimeUnitState.Current, TimeUnit.Year], label: 'This Year', class: 'calendar-button-half' },
    { key: [TimeUnitState.Last, TimeUnit.Year], label: 'Last Year', class: 'calendar-button-half' }
  ];

  constructor(private globals: Globals) {
    this.calendarLocale = this.globals.calendarLocale;
  }

  ngOnInit() {
    this.dates = this.getSelectedDates();
  }

  ngOnChanges(changes: SimpleChanges) {
    const dateRange = changes['dateRange']?.currentValue as CalendarDateRange;
    if (dateRange) {
      this.dates = [dateRange.dateFrom, dateRange.dateTo];
    }
  }

  onSelectAbsoluteDates() {
    const [dateFrom, dateTo] = this.dates ?? [];
    if (dateFrom && dateTo) {
      this.setAbsoluteDates(dateFrom, dateTo);
    } else {
      this.refreshAbsoluteDatesNoEmit();
    }
  }

  onSelectRelativeDates(relativeTimeUnit: RelativeTimeUnit) {
    this.setRelativeDates(...relativeTimeUnit);
  }

  onBlur() {
    if (this.isAbsoluteDateSelected && !this.calendar.overlayVisible) {
      const [dateFrom, dateTo] = this.dates ?? [];
      this.tryUpdateAbsoluteDatesOrRevert(dateFrom, dateTo);
    }
  }

  onClose() {
    const [dateFrom, dateTo] = this.dates ?? [];
    const [selectedDateFrom, selectedDateTo] = this.getSelectedDates();

    const datesAreEqual = dateFrom?.getDate() === selectedDateFrom?.getDate()
      && dateTo?.getDate() === selectedDateTo?.getDate();

    if (!datesAreEqual && this.isAbsoluteDateSelected) {
      this.tryUpdateAbsoluteDatesOrRevert(dateFrom, dateTo);
    }
  }

  onInput() {
    this.refreshAbsoluteDatesNoEmit();
  }

  isRelativeDateValueSelected([timeUnitState, timeUnit]: RelativeTimeUnit): boolean {
    const [markerTimeUnitState, markerTimeUnit] = this.dateRange
      .relativeDateRangeMarker ?? [];
    return markerTimeUnitState === timeUnitState && markerTimeUnit === timeUnit;
  }

  private setAbsoluteDates(dateFrom: Date, dateTo: Date) {
    const dateRange = new CalendarDateRange({
      type: DateRangeType.Absolute,
      dateFrom: dateFrom?.toISOString(),
      dateTo: dateTo?.toISOString()
    });
    this.dateRange = dateRange;
    this.dateRangeChange.emit(dateRange);
  }

  private setRelativeDates(timeUnitState: TimeUnitState, timeUnit: TimeUnit) {
    const dateRange = new CalendarDateRange({
      type: DateRangeType.Relative,
      relativeTimeUnitState: timeUnitState,
      relativeTimeUnit: timeUnit
    });
    this.dateRange = dateRange;
    this.dateRangeChange.emit(dateRange);
  }

  /**
   * Sets absolute date range, or revert to previously selected value when dateTo in not defined
   */
  private tryUpdateAbsoluteDatesOrRevert(dateFrom: Date, dateTo?: Date) {
    if (!dateFrom && !dateTo) {
      this.dates = this.getSelectedDates();
    } else {
      this.setAbsoluteDates(dateFrom, dateTo ?? dateFrom);
    }
  }

  /**
   * Converts selected range to absolute without emitting event
   */
  private refreshAbsoluteDatesNoEmit() {
    if (this.isRelativeDateSelected) {
      const [selectedDateFrom, selectedDateTo] = this.getSelectedDates();
      this.dateRange = new CalendarDateRange({
        type: DateRangeType.Absolute,
        dateFrom: selectedDateFrom?.toISOString(),
        dateTo: selectedDateTo?.toISOString()
      });
    }
  }

  private get isAbsoluteDateSelected() {
    return !this.dateRange.relativeDateRangeMarker;
  }

  private get isRelativeDateSelected() {
    return !this.isAbsoluteDateSelected;
  }

  private getSelectedDates(): Date[] {
    return [
      this.dateRange?.dateFrom,
      this.dateRange?.dateTo
    ];
  }
}

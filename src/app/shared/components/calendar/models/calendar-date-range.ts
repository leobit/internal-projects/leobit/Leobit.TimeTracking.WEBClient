import { isDefined } from "src/app/core/helpers/types.helpers";
import { DateRange, DateRangeType, TimeUnit, TimeUnitState } from "../../../../core/api/models";
import { CalendarRelativeDateUtils } from "src/app/core/utils/calendar-relative-date.utils";

export type RelativeTimeUnit = [timeUnitState: TimeUnitState, timeUnit: TimeUnit];

export class CalendarDateRange {
  private _dateFrom: Date;
  private _dateTo: Date;
  private _relativeDateRangeMarker?: RelativeTimeUnit;

  get dateFrom(): Date {
    return this._dateFrom;
  }

  get dateTo(): Date {
    return this._dateTo;
  }

  get relativeDateRangeMarker(): RelativeTimeUnit {
    return this._relativeDateRangeMarker;
  }

  constructor(dateRangeFromApi: DateRange) {
    if (!isDefined(dateRangeFromApi)) {
      throw new Error("DateRange argument is null or undefined or has wrong type");
    }
    if (dateRangeFromApi.type === DateRangeType.Relative) {
        if (!dateRangeFromApi.relativeTimeUnitState || !dateRangeFromApi.relativeTimeUnit) {
          throw new Error("Relative date range marker is incorrect for relative date range");
        }
        this.applyRelativeDateRange(dateRangeFromApi.relativeTimeUnitState,
          dateRangeFromApi.relativeTimeUnit);
    } else {
      if (!dateRangeFromApi.dateFrom && !dateRangeFromApi.dateTo) {
        throw new Error("DateFrom and DateTo values cannot be null for absolute date range");
      }

      this._dateFrom = new Date(dateRangeFromApi.dateFrom);
      this._dateTo = new Date(dateRangeFromApi.dateTo || dateRangeFromApi.dateFrom);
      this._relativeDateRangeMarker = null;
    }
  }

  toDateRangeFromApi(): DateRange {
    if (this.relativeDateRangeMarker) {
      return {
        type: DateRangeType.Relative,
        relativeTimeUnitState: this.relativeDateRangeMarker[0],
        relativeTimeUnit: this.relativeDateRangeMarker[1]
      }
    } else {
      return {
        type: DateRangeType.Absolute,
        dateFrom: Date.CreateUTCDate(this.dateFrom).toISOString(),
        dateTo: Date.CreateUTCDate(this.dateTo || this.dateFrom).toISOString()
      }
    }
  }

  private applyRelativeDateRange(...[timeUnitState, timeUnit]: RelativeTimeUnit) {
    const dates = CalendarRelativeDatesProvider.relativeDateRangeTypes.find((pair: RelativeDateKeyValuePair) => {
      const [comparedTimeUnitState, comparedTimeUnit] = pair.key;
      return comparedTimeUnitState === timeUnitState
        && comparedTimeUnit === timeUnit
    })?.dates;

    if (!dates) {
      throw new Error("Correct relative date range type is not found");
    }

    [this._dateFrom, this._dateTo] = dates;
    this._relativeDateRangeMarker = [timeUnitState, timeUnit];
  }
}

class CalendarRelativeDatesProvider {
  public static readonly relativeDateRangeTypes: RelativeDateKeyValuePair[] = [
    { key: [TimeUnitState.Current, TimeUnit.Day], dates: CalendarRelativeDateUtils.getTodayRange() },
    { key: [TimeUnitState.Current, TimeUnit.Week], dates: CalendarRelativeDateUtils.getCurrentWeekRange() },
    { key: [TimeUnitState.Last, TimeUnit.Week], dates: CalendarRelativeDateUtils.getLastWeekRange() },
    { key: [TimeUnitState.Current, TimeUnit.Month], dates: CalendarRelativeDateUtils.getCurrentMonthRange() },
    { key: [TimeUnitState.Last, TimeUnit.Month], dates: CalendarRelativeDateUtils.getLastMonthRange() },
    { key: [TimeUnitState.Current, TimeUnit.Year], dates: CalendarRelativeDateUtils.getCurrentYearRange() },
    { key: [TimeUnitState.Last, TimeUnit.Year], dates: CalendarRelativeDateUtils.getLastYearRange() }
  ];
}

interface RelativeDateKeyValuePair {
  readonly key: RelativeTimeUnit;
  readonly dates: Date[];
}

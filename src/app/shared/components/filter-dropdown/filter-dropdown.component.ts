import { Component, EventEmitter, Input, Output, input } from '@angular/core';
import { SelectItem } from 'primeng/api';

@Component({
  selector: 'app-filter-dropdown',
  templateUrl: './filter-dropdown.component.html',
  styleUrls: ['./filter-dropdown.component.css'],
})
export class FilterDropdownComponent {
  @Input() options: SelectItem[];
  @Input() selectedItemConfigOptions: any;
  @Input() style: any;
  @Input() disabled: boolean = false;
  @Input() appendTo: string;
  @Input() placeholder: string;
  @Input() resetFilterOnHide = false;

  @Input() selectedItem: any;
  @Output() selectedItemChange = new EventEmitter<any>();

  onSelectedItemChange(model: any) {
    this.selectedItemChange.emit(model);
  }
}

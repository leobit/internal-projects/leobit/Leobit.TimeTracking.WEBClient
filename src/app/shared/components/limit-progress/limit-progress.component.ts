import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-limit-progress',
  templateUrl: './limit-progress.component.html',
  styleUrls: ['./limit-progress.component.css']
})
export class LimitProgressComponent{
  @Input() value: number;
  @Input() maxLimit: number;
  @Input() intermediateLimit: number;
  @Input() unit: string;
  @Input() label: string;
  @Input() defaultMessage: string;
  @Input() maxLimitExceededMessage: string;
  @Input() intermediateLimitExceededMessage: string;
  @Input() disableExceedingWarning: boolean = false;
  @Input() progressState: string;
}

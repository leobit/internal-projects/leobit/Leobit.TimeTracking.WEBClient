import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sick-leave',
  templateUrl: './sick-leave.component.html',
  styleUrls: ['./sick-leave.component.css']
})
export class SickLeaveComponent implements OnInit {
  @Input() providedSickLeaveDaysWithoutCertificate: number;
  @Input() providedSickLeaveDaysWithCertificate: number;
  @Input() providedSickLeaveDaysToBeCompensated100: number;
  @Input() providedSickLeaveDaysToBeCompensated75: number;
  @Input() year: number;

  @Input() withoutCertificate: number[];
  @Input() withCertificate: number[];

  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  columns = ["", ...this.months, "Total for the year"];

  get usedSickLeaveDaysWithoutCertificate() {
    return this.withoutCertificate.reduce((accumulator, currentValue) => accumulator + (currentValue || 0), 0);
  }

  get usedSickLeaveDaysWithCertificate() {
    return this.withCertificate.reduce((accumulator, currentValue) => accumulator + (currentValue || 0), 0);
  }

  get usedSickLeaveDaysToBeCompensated100() {
    return Math.min(this.usedSickLeaveDaysWithCertificate, this.providedSickLeaveDaysToBeCompensated100);
  }

  get usedSickLeaveDaysToBeCompensated75() {
    return  Math.max(this.usedSickLeaveDaysWithCertificate - this.providedSickLeaveDaysToBeCompensated100, 0);
  }

  constructor() { }

  ngOnInit() {
    
  }

  isPastMonth(monthNumber: number) {
    let date = new Date();
    return monthNumber < date.getMonth() || this.year < date.getFullYear();
  }
}

import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";
import { ConfirmationService, MenuItem, MessageService } from "primeng/api";
import { DateRange, DateRangeType, EmployeeReportingFilters, FiltersPreset, Int32SelectedItemList, StringSelectedItemList } from "src/app/core/api/models";
import { FiltersPresetService } from "src/app/core/api/services";
import { showValidationErrors } from "src/app/core/helpers/reactive-forms.helper";

@Component({
  selector: 'app-filters-presets',
  templateUrl: './filters-presets.component.html',
  styleUrls: ['./filters-presets.component.css']
})
export class FiltersPresetsComponent implements OnInit {
  filtersPresets: FiltersPreset[] = [];
  selectedFiltersPreset: FiltersPreset = {};
  addFiltersPreset: FiltersPreset = {
    id: 0,
    name: '',
    employeeReportingFilters: null
  };

  isOverrideDisabled: boolean = true;
  filtersPresetDialogHeader: string;
  filtersPresetDialogVisible: boolean = false;
  
  filterPresetForm = new FormGroup({
    name: new FormControl(this.selectedFiltersPreset.name, 
      [
        Validators.required, 
        Validators.maxLength(20), 
        this.filtersPresetNameValidator(this.selectedFiltersPreset.name)
      ]),
  });

  @Input() selectedReportingFilters: EmployeeReportingFilters;
  @Output() selectedReportingFiltersChange = new EventEmitter<EmployeeReportingFilters>();
  @Output() selectedReportingFiltersReset = new EventEmitter();

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private filtersPresetService: FiltersPresetService
  ) {}

  ngOnInit(): void {
    this.filtersPresetService.getFiltersPresets()
      .subscribe(filtersPresets => {
        this.filtersPresets = filtersPresets;
        this.sortFiltersPresetsByName();
    });

    this.resetSelectedFiltersPreset();
  }

  get menuItems(): MenuItem[]  {
    return [
      {
        label: 'Override',
        icon: 'pi pi-refresh',
        command: () => this.overrideFiltersPreset(),
        disabled: this.isOverrideDisabled
      },
      {
        label: 'Rename',
        icon: 'pi pi-pencil',
        command: () => this.showRenameFiltersPresetDialog()
      },
      {
        label: 'Delete',
        icon: 'pi pi-times',
        command: () => this.deleteFiltersPreset()
      }
    ];
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['selectedReportingFilters'] !== undefined && changes['selectedReportingFilters'] !== null) {
      const currentValue = changes['selectedReportingFilters'].currentValue as EmployeeReportingFilters;

      if (currentValue) {
        if (this.selectedFiltersPreset?.employeeReportingFilters) {
          this.isOverrideDisabled = this.dateRangesAreEqual(this.selectedFiltersPreset?.employeeReportingFilters?.dateRange, currentValue.dateRange)
            && this.itemListsAreEqual(this.selectedFiltersPreset?.employeeReportingFilters?.userIds, currentValue.userIds)
            && this.itemListsAreEqual(this.selectedFiltersPreset?.employeeReportingFilters?.projectIds, currentValue.projectIds);
        }
      }
    }
  }

  private itemListsAreEqual(
    list1: StringSelectedItemList | Int32SelectedItemList,
    list2: StringSelectedItemList | Int32SelectedItemList): boolean {
    if (!list1 || !list2) {
      return false;
    }

    if (list1.allSelected && list2.allSelected) {
      return true;
    }
    else if (list1.allSelected !== list2.allSelected) {
      return false;
    }

    return this.arraysAreEqual(list1.selectedItems as Array<number | string>, list2.selectedItems as Array<number | string>);
  }

  private arraysAreEqual<Type>(arr1: Array<Type>, arr2: Array<Type>): boolean {
    if (!arr1 || !arr2) {
      return false;
    }

    if (arr1.length !== arr2.length) {
      return false;
    }

    return arr1.every(x => arr2.includes(x));
  }

  private dateRangesAreEqual(dateRange1: DateRange, dateRange2: DateRange): boolean {
    if (!dateRange1 || !dateRange2) {
      return false;
    }

    if (dateRange1.type !== dateRange2.type) {
      return false;
    } else if (dateRange1.type === DateRangeType.Relative) {
      return dateRange1.relativeTimeUnit === dateRange2.relativeTimeUnit && dateRange1.relativeTimeUnitState === dateRange2.relativeTimeUnitState;
    } else if (dateRange1.type === DateRangeType.Absolute) {
      return dateRange1.dateFrom.parseISOStringToLocaleDateString() === dateRange2.dateFrom.parseISOStringToLocaleDateString()
        && dateRange1.dateTo.parseISOStringToLocaleDateString() === dateRange2.dateTo.parseISOStringToLocaleDateString()
    }
  }

  selectFiltersPreset(filtersPreset: FiltersPreset) {
    this.selectedFiltersPreset = filtersPreset;
    this.selectedReportingFiltersChange.emit(this.selectedFiltersPreset.employeeReportingFilters);
  }

  onFiltersPresetSubmit() {
    if (this.selectedFiltersPreset.id === 0) {
      this.saveFiltersPreset();
    } else {
      this.renameFiltersPreset();
    }
  }

  saveFiltersPreset() {
    const filtersPresetName = this.filterPresetForm.get('name').value.trim();
    this.filtersPresetService.createFiltersPreset({
      body: {
        ...this.selectedFiltersPreset,
        name: filtersPresetName,
        employeeReportingFilters: this.selectedReportingFilters
      }}).subscribe({
        next: createdFiltersPreset => {
          this.messageService.add({ severity: 'success', summary: 'Done', detail: `Filter preset saved` });
          this.filtersPresets = [...this.filtersPresets, createdFiltersPreset];

          this.sortFiltersPresetsByName();
          this.selectFiltersPreset(createdFiltersPreset);
        },
        error: _ => this.resetSelectedFiltersPreset()
    });

    this.hideFiltersPresetDialog();
  }

  showAddFiltersPresetDialog() {
    this.selectFiltersPreset(this.addFiltersPreset);

    this.filtersPresetDialogHeader = "Save filters preset";
    this.filterPresetForm.setValue({name: ''});
    this.filtersPresetDialogVisible = true;
  }

  hideFiltersPresetDialog() {
    this.filterPresetForm.reset();
    this.filtersPresetDialogVisible = false;
  }

  overrideFiltersPreset() {
    this.confirmationService.confirm({
      message:
        "Are you sure you want to override this preset?",
      accept: () => {
        this.filtersPresetService.editEmployeeReportingFilters({
          filtersPresetId: this.selectedFiltersPreset.id,
          body: this.selectedReportingFilters
        })
        .subscribe(_ => {
          this.messageService.add({
            severity: 'success',
            summary: 'Done',
            detail: "Filters Preset overriden"
          });

          const filtersPresetToUpdate = this.filtersPresets.find(x => x.id === this.selectedFiltersPreset.id);
          if (filtersPresetToUpdate) {
            const index = this.filtersPresets.indexOf(filtersPresetToUpdate);
            this.filtersPresets[index] = {...filtersPresetToUpdate, employeeReportingFilters: this.selectedReportingFilters};
            this.selectedFiltersPreset = {...this.filtersPresets[index]};
          }
        });
      }
    });
  }

  renameFiltersPreset() {
    const filtersPresetName = this.filterPresetForm.get('name').value.trim();
    this.filtersPresetService.editFiltersPresetName({
      filtersPresetId: this.selectedFiltersPreset.id,
      filtersPresetName: filtersPresetName
    })
    .subscribe({
      next: _ => {
        this.messageService.add({
          severity: 'success',
          summary: 'Done',
          detail: "Filters Preset renamed"
        });

        const filtersPresetToUpdate = this.filtersPresets.find(x => x.id === this.selectedFiltersPreset.id);
        if (filtersPresetToUpdate) {
          const index = this.filtersPresets.indexOf(filtersPresetToUpdate);
          this.filtersPresets[index] = {...filtersPresetToUpdate, name: filtersPresetName};
          this.selectedFiltersPreset = {...this.filtersPresets[index]};

          this.sortFiltersPresetsByName();
        }

        this.hideFiltersPresetDialog();
      },
      error: _ => this.hideFiltersPresetDialog()
    });
  }

  showRenameFiltersPresetDialog() {
    this.filtersPresetDialogHeader = "Rename filters preset";
    this.filterPresetForm.setValue({name: this.selectedFiltersPreset.name});
    this.filtersPresetDialogVisible = true;
  }

  deleteFiltersPreset() {
    this.confirmationService.confirm({
      message:
        "Are you sure you want to delete this preset?",
      accept: () => {
        this.filtersPresetService.deleteFiltersPreset({
          id: this.selectedFiltersPreset.id
        })
        .subscribe(_ => {
          this.messageService.add({
            severity: 'success',
            summary: 'Done',
            detail: "Filters Preset deleted"
          });

          this.filtersPresets = this.filtersPresets.filter(x => x.id !== this.selectedFiltersPreset.id);
          this.selectedReportingFiltersReset.emit();

          this.resetSelectedFiltersPreset();
        });
      }
    });
  }

  filtersPresetNameValidator(filtersPresetName: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return !control.value?.trim()
        || control.value.trim() === filtersPresetName
        || this.filtersPresets.some(x => x.name === control.value.trim())
        ? { forbiddenValue: { value: control.value } }
        : null;
    }
  }

  resetSelectedFiltersPreset() {
    this.selectedFiltersPreset = {
      id: -1,
      name: '',
      employeeReportingFilters: null
    };
  }

  sortFiltersPresetsByName() {
    this.filtersPresets = this.filtersPresets
      .sort((a, b) => a.name.localeCompare(b.name));
  }

  onHideFiltersPresetDialog() {
    if (this.selectedFiltersPreset.id === 0) {
      this.resetSelectedFiltersPreset();
    }
  }

  showFiltersPresetsValidationErrors(fieldName: string): boolean {
    return showValidationErrors(fieldName, this.filterPresetForm);
  }
}

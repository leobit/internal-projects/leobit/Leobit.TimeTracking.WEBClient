import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { VacationService } from 'src/app/core/api/services';
import { CustomDayOff, VacationMonthData, VacationYearData } from 'src/app/core/api/models';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Globals } from "src/app/core/globals";
import { CustomDayOffDialog } from './models/custom-day-off-dialog';
import { ActionType } from './models/action-type.enum';
import { CellBallance } from './models/cell-balance.enum';

export interface StandingYearsBreakpoint {
  threshold: number,
  annualVacations: number,
  message?: string
}

@Component({
  selector: 'app-vacation',
  templateUrl: './vacation.component.html',
  styleUrls: ['./vacation.component.css']
})
export class VacationComponent implements OnInit {
  @Input() currentVacationBalance: number;
  @Input() expectedEoyVacationBalance: number;
  @Input() extraDaysOff: number;
  @Input() compensatedVacationLeftovers: number;
  @Input() employmentDate: Date;
  @Input() isTMEmployee: boolean;
  @Input() terminationDate: Date | null;
  @Input() firstReportedDate: string;
  @Input() standingYears: number;
  @Input() minimumAllowedBalance: number;
  @Input() transferredBalance: number;
  @Input() reportedReserveDays: number;
  @Input() reportedReserveDaysForLastSixMonths: number;
  @Input() year: number;
  @Input() selectedUserId: string;
  @Input() yearData: VacationYearData;

  @Input() customDayOffs: CustomDayOff[];
  @Input() allCustomDayOffs: CustomDayOff[];

  @Output() onUpdateEmploymentDate = new EventEmitter<Date>();
  public employmentDateModel: Date;
  public maxDate = new Date();
  public minDate: Date;

  employeeVacationAndSickLeavePermission: number;

  customDayOffMenu: MenuItem[] = [
    { label: 'All dayoffs', icon: 'fa fa-list-ul', command: () => { this.showHistoryAllDialog(); } }
  ];

  editedCustomDayOffId: number;
  selectedCustomDayOffId: number;
  accrualDays: { excludeFromYearlyTotal: boolean, value: number }[];
  usedDays: { excludeFromYearlyTotal: boolean, value: number }[];

  historyDialogVisible: boolean;
  historyAllDialogVisible: boolean;
  showEditDate = false;

  breakpoints: StandingYearsBreakpoint[] = [
    { threshold: 0, annualVacations: 18, message: "up to 2 years of total work experience in Leobit" },
    { threshold: 2, annualVacations: 20 },
    { threshold: 3, annualVacations: 21 },
    { threshold: 4, annualVacations: 22 },
  ];

  reversedBreakpoints = [...this.breakpoints].reverse();

  currentBreakpoint: StandingYearsBreakpoint;
  nextBreakpoint: StandingYearsBreakpoint;

  customDayOffDialogModel: CustomDayOffDialog = this.getNewCustomDayOffDialogModel();

  get customDayOffWritePermission(): boolean {
    return this.employeeVacationAndSickLeavePermission >= 2 && this.selectedUserId != null;
  }

  get selectedCustomDayOff(): CustomDayOff[] {
    return this.selectedCustomDayOffId ?
      [this.customDayOffs.find(cdo => cdo.id === this.selectedCustomDayOffId)] : [];
  }

  get assignedMonths(): number[] | null {
    return this.customDayOffs ?
      this.customDayOffs.map(cdo => cdo.month - 1) : null;
  }

  get totalAccrual() {
    return this.accrualDays
      .filter((current) => !current.excludeFromYearlyTotal)
      .reduce((accumulator, current) => accumulator + (current.value || 0), 0);
  }

  get totalUsed() {
    return this.usedDays
      .filter((current) => !current.excludeFromYearlyTotal)
      .reduce((accumulator, current) => accumulator + (current.value || 0), 0);
  }

  get accrualToDate() {
    return this.accrualDays
      .filter((current, index) => this.isPastMonth(index) && !current.excludeFromYearlyTotal)
      .reduce((accumulator, current) => accumulator + (current.value || 0), 0);
  }

  get availableVacation(): number {
    return this.transferredBalance + this.accrualToDate + this.extraDaysOff + this.minimumAllowedBalance - (this.totalUsed + this.compensatedVacationLeftovers);
  }

  get reachedReserveBreakpoints(): number {
    return Math.floor(this.reportedReserveDays / 15);

  }

  get reserveDaysBreakpoint(): number {
    return (this.reachedReserveBreakpoints + 1) * 15;
  }

  get employmentDateWritePermission(): boolean {
    return this.employeeVacationAndSickLeavePermission >= 2 && this.selectedUserId != null;
  }

  columns = ["", ...this.globals.calendarLocale.monthNamesShort, "Total for the year"];

  constructor(
    private vacationService: VacationService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private oauthService: OAuthService,
    public globals: Globals
  ) { }

  ngOnInit() {
    this.employeeVacationAndSickLeavePermission = (this.oauthService.getIdentityClaims() as any).EmployeeVacationAndSickLeave;
    if(this.employeeVacationAndSickLeavePermission === undefined){
      this.oauthService.loadUserProfile().then(() => {
        this.employeeVacationAndSickLeavePermission = (this.oauthService.getIdentityClaims() as any).EmployeeVacationAndSickLeave;
      });
    }
    this.minDate = new Date(this.firstReportedDate);

    this.accrualDays = this.yearDataToArray(this.yearData, x => x.accrual);
    this.usedDays = this.yearDataToArray(this.yearData, x => x.used);

    this.setStandingYearsBreakpoints();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.standingYears && !changes.standingYears.isFirstChange()) {
      this.setStandingYearsBreakpoints();
    }

    if(changes.yearData){
      this.accrualDays = this.yearDataToArray(this.yearData, x => x.accrual);
      this.usedDays = this.yearDataToArray(this.yearData, x => x.used);
    }
  }

  getNewCustomDayOffDialogModel(): CustomDayOffDialog {
    let firstDayOfYear = new Date(this.year, 0, 1);
    let lastDayOfYear = new Date(this.year, 11, 31);

    let empDate = new Date(this.employmentDate);
    let termDate = !!this.terminationDate ? new Date(this.terminationDate) : new Date();

    let minDate = !this.employmentDate || firstDayOfYear > empDate
      ? firstDayOfYear : new Date(empDate.getFullYear(), empDate.getMonth(), 1);
    let maxDate = !this.terminationDate || lastDayOfYear < termDate
      ? lastDayOfYear : new Date(termDate.getFullYear(), termDate.getMonth() + 1, 0);
    let today = new Date();

    return {
      date: today > minDate && today < maxDate ? today : minDate,
      minDate: minDate,
      maxDate: maxDate,
      extraDayOff: 0,
      description: "",
      action: ActionType.add,
      isVisible: false
    };
  }

  isPastMonth(monthNumber: number) {
    let date = new Date();
    return monthNumber < date.getMonth() || this.year < date.getFullYear();
  }

  isMonthCustomsCountPositive(monthNumber: number) {
    if(!this.customDayOffs) return CellBallance.Default;
    const cdo = this.customDayOffs.find(c => c.month - 1 === monthNumber);
    if (!cdo) {
      return CellBallance.Default;
    }

    if (cdo.extraDayOff > 0) {
      return CellBallance.Positive;
    } else if (cdo.extraDayOff < 0) {
      return CellBallance.Negative;
    }
  }

  onCustomDayOff(id) {
    if (this.selectedCustomDayOffId === id) {
      this.selectedCustomDayOffId = null;
    } else {
      this.selectedCustomDayOffId = id;
    }
  }

  onDeleteCustomDayOff(id) {
    this.confirmationService.confirm({
      message: `Are you sure that you want to remove this day off?`,
      accept: () => {
        this.vacationService.deleteCustomDayOff({ id }).subscribe(_ => {
          const customDayOff = this.allCustomDayOffs.find(cdo => cdo.id === id);
          if (customDayOff.year === this.year && this.selectedUserId == customDayOff.userId) {
            this.accrualDays[customDayOff.month - 1].value -= customDayOff.extraDayOff;
            this.customDayOffs = this.customDayOffs.filter(cdo => cdo.id != id);
            this.extraDaysOff = this.extraDaysOff - customDayOff.extraDayOff;
          }
          this.allCustomDayOffs = this.allCustomDayOffs.filter(cdo => cdo.id != id);
          this.messageService.add({ severity: "success", summary: "Done" });
          if(this.customDayOffs.length == 0) this.historyDialogVisible = false;
          if(this.allCustomDayOffs.length == 0) this.historyAllDialogVisible = false;
        });
      }
    });
  }

  onEditCustomDayOff(id) {
    this.editedCustomDayOffId = id
      ? id
      : this.customDayOffs.find(cdo => cdo.month === this.customDayOffDialogModel.date.getMonth() + 1).id;
    const editedCustomDayOff = this.allCustomDayOffs.find(cdo => cdo.id === this.editedCustomDayOffId);
    this.customDayOffDialogModel = {
      ...this.getNewCustomDayOffDialogModel(),
      date: new Date(this.year, editedCustomDayOff.month, 0),
      extraDayOff: editedCustomDayOff.extraDayOff,
      description: editedCustomDayOff.description,
      action: ActionType.edit
    };
    this.showCustomDayOffDialog();
  }

  onAddCustomDayOff() {
    this.customDayOffDialogModel = this.getNewCustomDayOffDialogModel();
    this.showCustomDayOffDialog();
  }

  onSaveCustomDayOff() {
    if (this.customDayOffDialogModel.action === ActionType.add) {
      this.vacationService.createCustomDayOff({
        body: {
          month: this.customDayOffDialogModel.date.getMonth() + 1,
          year: this.customDayOffDialogModel.date.getFullYear(),
          extraDayOff: this.customDayOffDialogModel.extraDayOff,
          description: this.customDayOffDialogModel.description,
          userId: this.selectedUserId,
        }
      }).subscribe(customDayOff => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Day off for ${this.globals.calendarLocale.monthNames[customDayOff.month - 1]} ${customDayOff.year} created` });
        if (customDayOff.year === this.year) {
          this.customDayOffs.push(customDayOff);
          this.allCustomDayOffs.push(customDayOff);
          this.accrualDays[customDayOff.month - 1].value += customDayOff.extraDayOff;
          this.extraDaysOff += customDayOff.extraDayOff;
        }
        this.hideCustomDayOffDialog();
      });
    }
    else {
      const toEditCdo = this.allCustomDayOffs.find(x => x.id == this.editedCustomDayOffId);
      this.vacationService.editCustomDayOff({
        body: {
          id: this.editedCustomDayOffId,
          month: this.customDayOffDialogModel.date.getMonth() + 1,
          year: this.customDayOffDialogModel.date.getFullYear(),
          extraDayOff: this.customDayOffDialogModel.extraDayOff,
          description: this.customDayOffDialogModel.description,
          userId: toEditCdo.userId
        }
      }).subscribe(customDayOff => {
        this.messageService.add({ severity: "success", summary: "Done", detail: `Day off for ${this.globals.calendarLocale.monthNames[customDayOff.month - 1]} ${customDayOff.year} updated` });
        if (customDayOff.year === this.year) {
          if(customDayOff.userId == this.selectedUserId){
            var editedCustomDayOffIndex = this.customDayOffs.findIndex(cdo => cdo.id == this.editedCustomDayOffId);
            var editedCustomDayOff = this.customDayOffs[editedCustomDayOffIndex];
            this.accrualDays[editedCustomDayOff.month - 1].value -= editedCustomDayOff.extraDayOff;
            this.accrualDays[customDayOff.month - 1].value += customDayOff.extraDayOff;
            this.customDayOffs[editedCustomDayOffIndex] = customDayOff;
          }
          const editAllCustomDayOffIndex = this.allCustomDayOffs.findIndex(cdo => cdo.id == this.editedCustomDayOffId);
          this.allCustomDayOffs[editAllCustomDayOffIndex] = customDayOff;
        }
        this.hideCustomDayOffDialog();
      });
    }
  }

  showHistoryDialog() {
    this.historyDialogVisible = this.customDayOffs && !!this.customDayOffs.length;
  }

  showHistoryAllDialog() {
    this.historyAllDialogVisible = this.allCustomDayOffs && !!this.allCustomDayOffs.length;
  }

  onEditEmploymentDate() {
    this.employmentDateModel = this.employmentDate;
    this.showEditDate = true;
  }

  onNewEmploymentDateConfirm(){
    this.vacationService.editEmploymentDate({
      body: {
        userId: this.selectedUserId,
        date: this.employmentDateModel?.toDateString()
      }
    }).subscribe(() => {
      this.employmentDate = this.employmentDateModel;
      this.showEditDate = false;
      this.onUpdateEmploymentDate.emit(this.employmentDate);
    });
  }

  onCancelEmploymentDate(){
    this.showEditDate = false;
  }

  setStandingYearsBreakpoints() {
    const index = this.reversedBreakpoints.findIndex(rb => rb.threshold <= this.standingYears);

    this.currentBreakpoint = this.reversedBreakpoints[index];
    this.nextBreakpoint = this.reversedBreakpoints[index - 1] || null;
  }

  private showCustomDayOffDialog() {
    this.customDayOffDialogModel.isVisible = true;
  }

  private hideCustomDayOffDialog() {
    this.customDayOffDialogModel.isVisible = false;
  }

  private yearDataToArray(
    yearData: VacationYearData,
    propertyAccessor: (x: VacationMonthData | undefined) => number
  ): { excludeFromYearlyTotal: boolean; value: number }[] {
    return [
      {value: propertyAccessor(yearData.january || {}), excludeFromYearlyTotal: yearData.january.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.february || {}), excludeFromYearlyTotal: yearData.february.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.march || {}), excludeFromYearlyTotal: yearData.march.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.april || {}), excludeFromYearlyTotal: yearData.april.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.may || {}), excludeFromYearlyTotal: yearData.may.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.june || {}), excludeFromYearlyTotal: yearData.june.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.july || {}), excludeFromYearlyTotal: yearData.july.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.august || {}), excludeFromYearlyTotal: yearData.august.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.september || {}), excludeFromYearlyTotal: yearData.september.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.october || {}), excludeFromYearlyTotal: yearData.october.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.november || {}), excludeFromYearlyTotal: yearData.november.excludeFromYearlyTotal},
      {value: propertyAccessor(yearData.december || {}), excludeFromYearlyTotal: yearData.december.excludeFromYearlyTotal},
    ];
  }
}

export enum CellBallance {
    Default = 'Default',
    Positive = 'Positive',
    Negative = 'Negative'
}
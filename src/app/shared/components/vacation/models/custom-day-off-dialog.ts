import { ActionType } from "./action-type.enum";

export interface CustomDayOffDialog {
  description?: null | string;
  extraDayOff?: number;
  date?: Date;
  minDate?: Date;
  maxDate?: Date;
  action: ActionType;
  isVisible: boolean;
}

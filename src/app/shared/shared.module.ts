import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { ProgressBarModule } from 'primeng/progressbar';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CalendarModule } from 'primeng/calendar';
import { ChipModule } from 'primeng/chip';
import { MenuModule } from 'primeng/menu';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';

import { FooterComponent } from './components/footer/footer.component';
import { VacationComponent } from './components/vacation/vacation.component';
import { SickLeaveComponent } from './components/sick-leave/sick-leave.component';
import { EditCustomDayOffComponent } from './components/edit-custom-day-off/edit-custom-day-off.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { FiltersPresetsComponent } from './components/filters-presets/filters-presets.component';
import { MenuChipComponent } from './components/menu-chip/menu-chip.component';
import { LimitProgressComponent } from './components/limit-progress/limit-progress.component';
import { FilterDropdownComponent } from './components/filter-dropdown/filter-dropdown.component';
import { MaxLengthDirective } from './directives/max-length.directive';
import { NoWhiteSpacesDirective } from './directives/no-whitespaces.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ButtonModule,
    SplitButtonModule,
    ProgressBarModule,
    TableModule,
    DialogModule,
    InputTextModule,
    KeyFilterModule,
    InputTextareaModule,
    CalendarModule,
    ReactiveFormsModule,
    ChipModule,
    MenuModule,
    ToastModule,
    ConfirmDialogModule,
    OverlayPanelModule,
    DropdownModule
  ],
  declarations: [
    FooterComponent,
    VacationComponent,
    SickLeaveComponent,
    EditCustomDayOffComponent,
    CalendarComponent,
    MenuChipComponent,
    FiltersPresetsComponent,
    LimitProgressComponent,
    FilterDropdownComponent,
    MaxLengthDirective,
    NoWhiteSpacesDirective
  ],
  exports: [
    FooterComponent,
    VacationComponent,
    SickLeaveComponent,
    EditCustomDayOffComponent,
    CalendarComponent,
    MenuChipComponent,
    FiltersPresetsComponent,
    FilterDropdownComponent,
    MaxLengthDirective,
    NoWhiteSpacesDirective
  ]
})
export class SharedModule { }

export class ProjectConfigurationBackup {
  BeforeClosing: number;
  AfterClosing: number;
  RestoreClosingSettingsLater: boolean;
}

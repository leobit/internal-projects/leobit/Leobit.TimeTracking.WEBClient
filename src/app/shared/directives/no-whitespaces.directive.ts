import { Directive } from '@angular/core';
import { AbstractControl, ValidationErrors, NG_VALIDATORS, Validator } from '@angular/forms';
import { noWhiteSpacesValidator } from '../validators/no-whitespaces-validator';

@Directive({
  selector: '[noWhiteSpaces]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: NoWhiteSpacesDirective,
      multi: true,
    },
  ]
})
export class NoWhiteSpacesDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return noWhiteSpacesValidator()(control);
  }
}

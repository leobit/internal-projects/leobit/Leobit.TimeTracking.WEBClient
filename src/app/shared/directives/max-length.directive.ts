import { Directive, Input } from '@angular/core';
import { AbstractControl, ValidationErrors, NG_VALIDATORS, Validator } from '@angular/forms';
import { maxLengthValidator } from '../validators/max-length.validator';

@Directive({
  selector: '[maxLength]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: MaxLengthDirective,
      multi: true,
    },
  ]
})
export class MaxLengthDirective implements Validator {
  @Input() maxLength: number;

  validate(control: AbstractControl): ValidationErrors | null {
    if (this.maxLength === null || this.maxLength === undefined || this.maxLength < 1) {
      return null;
    }
    return maxLengthValidator(this.maxLength)(control);
  }
}

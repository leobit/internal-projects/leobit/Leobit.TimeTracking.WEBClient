import { DecimalPipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'signedNumber',
  standalone: true
})
export class SignedDecimalPipe extends DecimalPipe implements PipeTransform {
  transform(value: string | number, digitsInfo?: string, locale?: string): string;
  transform(value: null, digitsInfo?: string, locale?: string): null;
  transform(value: string | number, digitsInfo?: string, locale?: string): string;
  transform(value: any, digitsInfo?: any, locale?: any): string {
    if (typeof value === 'number') {
      if (value > 0) {
        return '+' + super.transform(value, digitsInfo);
      } else {
        return super.transform(value, digitsInfo);
      }
    }

    return null;
  }
}

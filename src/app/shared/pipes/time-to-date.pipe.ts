import { Pipe, PipeTransform } from '@angular/core';
import * as dayjs from 'dayjs';
import * as relativeTime from 'dayjs/plugin/relativeTime';

@Pipe({
  name: 'timeToDate',
  standalone: true
})
export class TimeToDatePipe implements PipeTransform {
  transform(value: Date): string {
    dayjs.extend(relativeTime);
    return dayjs(value).fromNow();

  }
}

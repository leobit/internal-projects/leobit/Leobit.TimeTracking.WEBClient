import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";

export const DateRangeValidator: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
  const start = group.get('startDate').value;
  const end = group.get('endDate').value;

  if (start && end) {
    const startDate = start instanceof Date ? start : new Date(start);
    const endDate = end instanceof Date ? end : new Date(end);

    if (isNaN(startDate.getTime()) || isNaN(endDate.getTime())) {
      return { invalidDateFormat: true };
    }

    if (startDate > endDate) {
      return { dateRange: true };
    }
  }

  return null;
};

import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

export function noWhiteSpacesValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if ((control.value || '').trim().length === 0) {
      return {
        canNotContainWhiteSpaces: true
      };
    }
    return null;
  };
}

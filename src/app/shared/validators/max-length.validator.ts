import { ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

export function maxLengthValidator(maxLength: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (control.value?.length <= maxLength) {
      return null;
    }
    return {
      maxLength: `Max length is ${maxLength}`
    };
  };
}

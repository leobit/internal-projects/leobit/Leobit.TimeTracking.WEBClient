import { AbstractControl } from "@angular/forms";

export function PositiveNumberValidator(control: AbstractControl): { [key: string]: any } | null {
  if (control.value !== null && control.value !== undefined && (isNaN(control.value) || control.value < 0)) {
    return { 'positiveNumber': { value: control.value } };
  }
  return null;
}

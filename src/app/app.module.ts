import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { TabMenuModule } from 'primeng/tabmenu';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

import { AppComponent } from './app.component';
import { ApiModule } from './core/api/api.module';
import { environment } from 'src/environments/environment';
import { NgxLeobitComponentsModule } from 'ngx-leobit-components';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { RippleModule } from 'primeng/ripple';
import { ProgressBarModule } from 'primeng/progressbar';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,

    TabMenuModule,
    ToastModule,
    ConfirmDialogModule,
    NgxLeobitComponentsModule,
    MessagesModule,
    MessageModule,
    RippleModule,
    ProgressBarModule,

    ApiModule.forRoot({ rootUrl: environment.apiUrl }),
    AppRoutingModule,
    SharedModule,
    CoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

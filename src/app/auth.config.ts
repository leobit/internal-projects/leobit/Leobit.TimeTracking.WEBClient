import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';
 
export const authConfig: AuthConfig = {
 
  // Url of the Identity Provider
  issuer: environment.identityServerUrl,
  requireHttps: false,
  oidc: true,
  responseType: 'code',

  // URL of the SPA to redirect the user after silent refresh
  //silentRefreshRedirectUri: window.location.origin + '/silent-refresh.html',

  // The SPA's id. The SPA is registerd with this id at the auth-server
  clientId: 'leobit.timetracking.web',
 
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile TimeTracking offline_access',
  postLogoutRedirectUri: environment.postLogoutRedirectUri
}
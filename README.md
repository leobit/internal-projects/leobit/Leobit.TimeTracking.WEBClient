# TimeTracking Web Client

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.1.

## Local setup

### Cloning repository 

Use this command to clone over https. Use `develop` branch:
```shell
git clone https://gitlab.com/leobit/internal-projects/leobit/Leobit.TimeTracking.WEBClient.git Leobit.TimeTracking.WebClient -b develop
```

### Installing packages

Run this command
```shell
npm install
```

### Setting up backend

App requires the following services in order to run properly:
1. [Leobit Identity](https://gitlab.com/leobit/internal-projects/leobit/Leobit.IdentityServer)
2. [TimeTracking backend](https://gitlab.com/leobit/internal-projects/leobit/Leobit.TimeTracking)

The first (and recommended) option is to set up them locally. To do this, just follow the instructions in those repositories.

Alternatively, (if you don't have access to repositories, or cannot run them locally for some other reason), you can use remote Test environment instead. To do so, update your `environments\environment.ts` file, and update it with the following values:
```ts
export const environment = {
  // ... other values unchanged
  identityServerUrl: 'https://test.identity.leobit.co',
  apiUrl: 'https://test.timetracking.leobit.co/v1',
};
```

### Generating API client

We use Swagger code generation to generate API client code that communicates with backend. There are multiple options here:

By default, you should use 
```
npm run openapi-gen
``` 
It generates API client based on local API definition snapshot.

Alternatively, If you're making changes to your local backend, you might want to use `npm run openapi-gen-local` command instead. It generates API client based on current version of your local backend (backend must be running in order for this command to work)

There are also other options available, check `package.json` for details.

### Running the project

Use 
```
npm run start
``` 
to start the app in development. Use `https://localhost:5002` to access the app.

Application uses SSL locally. Node will generate a development certificate for you, but recent versions of Google Chrome display alerts for self-signed certificates. To get rid of the alert, you have to enable insecure localhost:

Open this link [chrome://flags/#allow-insecure-localhost](chrome://flags/#allow-insecure-localhost) in your browser, turn on the highlighted flag and restart. More details [here](https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate).
